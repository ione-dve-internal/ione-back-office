<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::auth();
Route::get('/', 'UserLoginController@index');
Route::post('login', 'UserLoginController@login');
Route::get('logout', 'UserLoginController@logout');

Route::group(['middleware'=>['admin']], function () {

   Route::get('dashboard', 'DashboardController@index');

   //promotion route
   Route::get('promotion', 'PromotionController@index');
   Route::get('promotion/page/{page}', 'PromotionController@index');
   Route::get('promotion/create', 'PromotionController@create');
   Route::post('promotion/insert', 'PromotionController@store');
   Route::get('promotion/edit/{id}', 'PromotionController@edit');
   Route::post('promotion/update/{id}', 'PromotionController@update');
   Route::get('promotion/delete/{id}', 'PromotionController@destroy');
   Route::get('promotion/image/{id}', 'PromotionController@image');
   Route::post('promotion/uploadthumb/{id}', 'PromotionController@uploadthumb');
   Route::post('promotion/uploadbanner/{id}', 'PromotionController@uploadbanner');

   //preorder route
   Route::get('preorder', 'PreOrderController@index');
   Route::get('preorder/create', 'PreOrderController@create');
   Route::post('preorder/insert', 'PreOrderController@store');
   Route::get('preorder/edit/{id}', 'PreOrderController@edit');
   Route::post('preorder/update/{id}', 'PreOrderController@update');
   Route::get('preorder/delete/{id}', 'PreOrderController@destroy');

   //preorder detail route
   Route::post('preorderdetail/insert', 'PreOrderController@storedetail');
   Route::post('preorderdetail/update/{id}', 'PreOrderController@updatedetail');
   Route::get('preorderdetail/delete/{id}', 'PreOrderController@destroydetail');

   //product price route
   Route::post('productprice/insert', 'PreOrderController@storeprice');
   Route::post('productprice/update/{id}', 'PreOrderController@updateprice');
   Route::get('productprice/delete/{id}/{detailId}', 'PreOrderController@destroyprice');

   //product appearance route
   Route::post('productappearence/insert', 'PreOrderController@storeappearence');
   Route::post('productappearence/update/{id}', 'PreOrderController@updateappearence');
   Route::get('productappearence/delete/{id}', 'PreOrderController@destroyappearence');

   Route::get('userpreorder', 'UserPreorderController@index');
   Route::get('userpreorders/updategetproduct/{id}/{value}', 'UserPreorderController@updategetproduct');

   //shop category route
   Route::get('shopcategory', 'ShopCategoryController@index');
   Route::get('shopcategory/create', 'ShopCategoryController@create');
   Route::post('shopcategory/insert', 'ShopCategoryController@store');
   Route::get('shopcategory/edit/{id}', 'ShopCategoryController@edit');
   Route::post('shopcategory/update/{id}', 'ShopCategoryController@update');
   Route::get('shopcategory/delete/{id}', 'ShopCategoryController@destroy');

   //shop route
   Route::get('shop', 'ShopController@index');
   Route::get('shop/page/{page}', 'ShopController@index');
   Route::get('shop/create', 'ShopController@create');
   Route::post('shop/insert', 'ShopController@store');
   Route::get('shop/edit/{id}', 'ShopController@edit');
   Route::post('shop/update/{id}', 'ShopController@update');
   Route::get('shop/delete/{id}', 'ShopController@destroy');
   Route::get('shop/image/{id}', 'ShopController@image');
   Route::post('shop/uploadlogo/{id}', 'ShopController@uploadlogo');
   Route::post('shop/uploadphoto/{id}', 'ShopController@uploadphoto');
   Route::get('shop/review/{id}', 'ShopController@review');

   //user route
   Route::get('user', 'UserController@index');
   Route::get('user/create', 'UserController@create');
   Route::post('user/insert', 'UserController@store');
   Route::get('user/edit/{id}', 'UserController@edit');
   Route::post('user/update/{id}', 'UserController@update');
   Route::get('user/delete/{id}', 'UserController@destroy');
   Route::get('user/password/{id}', 'UserController@password');
   Route::post('user/updatepassword/{id}', 'UserController@updatepassword');

   //user route
   Route::get('userfacebook', 'FacebookController@index');

   //card route
   Route::get('cards', 'CardController@index');
   Route::get('cards/page/{page}', 'CardController@index');
   //Admin Create Card
   Route::get('cards/create', 'CardController@create');
   //Shop Create Card
   Route::get('cards/creation', 'CardController@creation');
   Route::post('cards/insert', 'CardController@store');
   Route::get('cards/edit/{id}', 'CardController@edit');
   Route::post('cards/update/{id}', 'CardController@update');
   Route::get('cards/delete/{id}', 'CardController@destroy');
   Route::get('cards/export', 'CardController@cardExport');

   //card type route
   Route::get('cardtype', 'CardController@cardtype');
   Route::post('cardtype/insert', 'CardController@storetype');
   Route::get('cardtype/delete/{id}', 'CardController@destroytype');

   //offer
   Route::get('offer', 'OfferController@index');
   Route::get('offer/page/{page}', 'OfferController@index');
   Route::get('offer/create', 'OfferController@create');
   Route::post('offer/insert', 'OfferController@store');
   Route::get('offer/edit/{id}', 'OfferController@edit');
   Route::post('offer/update/{id}', 'OfferController@update');
   Route::get('offer/delete/{id}', 'OfferController@destroy');
   Route::get('offer/image/{id}', 'OfferController@image');
   Route::post('offer/uploadthumb/{id}', 'OfferController@uploadthumb');
   Route::post('offer/uploadbanner/{id}', 'OfferController@uploadbanner');

   //coupon
   Route::get('coupon', 'OfferController@coupon');
   Route::get('coupon/page/{page}', 'OfferController@coupon');
   Route::get('coupon/export', 'OfferController@couponExport');

   //Redeem
   Route::get('redeem', 'OfferController@redeem');
   Route::get('redeem/page/{page}', 'OfferController@redeem');
   Route::get('redeem/export', 'OfferController@redeemExport');

   //Redeem
   Route::get('feedback', 'FeedbackController@index');

   //aboutus
   Route::get('aboutus', 'PageController@aboutus');
   Route::post('page/update/{id}', 'PageController@update');


    // card migrate csv
    Route::get('csv', 'CardController@csv');

    //order-management route
    Route::get('order-management', 'OrderController@index');
    Route::get('order-management/page/{page}', 'OrderController@index');
    Route::get('order-management/edit/{id}', 'OrderController@edit');
    Route::post('order-management/update/{id}', 'OrderController@update');
    Route::get('order-management/show/{id}', 'OrderController@show');
    Route::post('order-management/filter', 'OrderController@filter');


    //banner route
    Route::get('banner', 'BannerController@index');
    Route::get('banner/page/{page}', 'BannerController@index');
    Route::get('banner/create', 'BannerController@create');
    Route::post('banner/insert', 'BannerController@store');
    Route::get('banner/delete/{id}', 'BannerController@destroy');
    Route::get('banner/image/{id}', 'BannerController@image');
    Route::post('banner/uploadbanner/{id}', 'BannerController@uploadbanner');


    //PRODUCT-MANAGEMENT
    Route::get('product-management', 'ProductController@index');
    Route::get('product-management/page/{page}', 'ProductController@index');
    Route::get('product-management/create', 'ProductController@create');
    Route::post('product-management/insert', 'ProductController@store');
    Route::get('product-management/edit/{id}', 'ProductController@edit');
    Route::post('product-management/update/{id}', 'ProductController@update');
    Route::get('product-management/delete/{id}', 'ProductController@destroy');
    Route::get('product-management/image/{id}', 'ProductController@image');
    Route::post('product-management/uploadphoto/{id}', 'ProductController@uploadphoto');
    Route::get('product-management/{id}/related', 'ProductController@related');
    Route::get('product-management/addrelated/{id}','ProductController@addrelated');
    Route::post('product-management/saveRelated/{id}', 'ProductController@saveRelated');
    Route::get('product-management/deleteimage/{id}', 'ProductController@deleteimage');
    Route::get('product-management/show/{id}', 'ProductController@show');
    Route::get('product-management/deleterelated/{id}', 'ProductController@deleterelated');


     //product category route
   Route::get('productcategory', 'ProductCategoryController@index');
   Route::get('productcategory/create', 'ProductCategoryController@create');
   Route::post('productcategory/insert', 'ProductCategoryController@store');
   Route::get('productcategory/edit/{id}', 'ProductCategoryController@edit');
   Route::post('productcategory/update/{id}', 'ProductCategoryController@update');
   Route::get('productcategory/delete/{id}', 'ProductCategoryController@destroy');




   
   Route::get('cards/browse', 'CardController@browseFile');
   Route::post('cards/import', 'CardController@importCard');

});
    Route::get('test_redeem', 'OfferController@test_redeem');
    