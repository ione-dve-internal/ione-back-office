<?php

/*
 * This file is part of Laravel Parse.
 *
 * (c) Graham Campbell <graham@alt-three.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Parse App Id
    |--------------------------------------------------------------------------
    |
    | Here you may specify your parse app id.
    |
    */

//    'app_id' => '_8Z6tG`B{s^NzjJv',
    'app_id' => 'myAppId',

    /*
    |--------------------------------------------------------------------------
    | Parse Rest Key
    |--------------------------------------------------------------------------
    |
    | Here you may specify your parse rest key.
    |
    */

    'rest_key' => '',

    /*
    |--------------------------------------------------------------------------
    | Parse Master Key
    |--------------------------------------------------------------------------
    |
    | Here you may specify your parse master key.
    |
    */

    //'master_key' => '*BcdCwUqN}jqD@t,LF7p?8~NzYGCDfH7"y,<DL=',
    'master_key' => 'myMasterKey',

    /*
    |--------------------------------------------------------------------------
    | Parse Server URL
    |--------------------------------------------------------------------------
    |
    | Here you may specify your parse server url.
    |
    */

    //'server_url' => 'http://ione-app-api.ioneservice.com/parse',
    'server_url' => 'http://localhost:1337/parse',

    /*
    |--------------------------------------------------------------------------
    | Parse Mount Point
    |--------------------------------------------------------------------------
    |
    | Here you may specify your parse mount point.
    |
    */

    'mount_path' => '',

];
