@extends('layouts.app')
@section('title', 'User Facebook')

@section('header')
<link rel="stylesheet" type="text/css" href="{{url('libs/datatables-net/media/css/dataTables.bootstrap4.min.css')}}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{url('assets/styles/libs/datatables-net/datatables.min.css')}}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{url('libs/select2/css/select2.min.css')}}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{url('assets/styles/libs/select2/select2.min.css')}}"> <!-- Customization -->
@endsection

@section('content')
<!-- BEGIN DASHBOARD HEADER -->
<div class="ks-header">
   <section class="ks-title">
      <h3>User Facebook</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="index.html">
               <span class="fa fa-home ks-icon"></span>
            </a>
            <span class="breadcrumb-item active">Admin</span>
            <a href="user/create" class="breadcrumb-item">New Item</a>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->

<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid">
         <table id="ks-datatable" class="table table-striped table-bordered" width="100%">
            <thead>
               <tr>
                  <th>Firstname</th>
                  <th>Lastname</th>
                  <th>Gender</th>
                  <th>Email</th>
                  <th width="80">View</th>
               </tr>
            </thead>
            <tbody>
               @foreach($results as $result)
                  <tr>
                     <td>{{$result->get('firstName')?$result->get('firstName'):'--'}}</td>
                     <td>{{$result->get('lastName')}}</td>
                     <td>{{$result->get('gender')}}</td>
                     <td>{{$result->get('email')}}</td>
                     <td class="text-center">
                        <a data-toggle="modal" data-target="#{{$result->getObjectId()}}">
                           <span class="fa fa-eye"></span>
                        </a>
                     </td>
                  </tr>
               @endforeach
            </tbody>
         </table>
         <!-- Modal -->
         @foreach($results as $result)
            <div class="modal fade" id="{{$result->getObjectId()}}" role="dialog">
               <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-body">
                        <div class="row">
                           <div class="col-lg-4"><b>Fullname</b></div>
                           <div class="col-lg-8">{{$result->get('firstName').' '.$result->get('lastName')}}</div>
                           <div class="col-lg-4"><b>Gender</b></div>
                           <div class="col-lg-8">{{$result->get('gender')?$result->get('gender'):'--'}}</div>
                           <div class="col-lg-4"><b>Date of Birth</b></div>
                           <div class="col-lg-8">{{$result->get('dob')?$result->get('dob'):'--'}}</div>
                           <div class="col-lg-4"><b>Phone</b></div>
                           <div class="col-lg-8">{{$result->get('phone')?$result->get('phone'):'--'}}</div>
                           <div class="col-lg-4"><b>Country</b></div>
                           <div class="col-lg-8">{{$result->get('country')?$result->get('country'):'--'}}</div>
                           <div class="col-lg-4"><b>Logined Date</b></div>
                           <div class="col-lg-8">{{ $result->getCreatedAt()->format('d-M-Y') }}</div>
                        </div>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                     </div>
                  </div>
               </div>
            </div>
         @endforeach
      </div>
   </div>
</div>
<!-- END DASHBOARD CONTENT -->
<div class="ks-scrollable"></div>
@endsection

@section('footer')
<script src="{{url('libs/datatables-net/media/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('libs/datatables-net/media/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{url('libs/select2/js/select2.min.js')}}"></script>
<script type="application/javascript">
(function ($) {
    $(document).ready(function() {
        $('#ks-datatable').DataTable({
            "initComplete": function () {
                $('.dataTables_wrapper select').select2({
                    minimumResultsForSearch: Infinity
                });
            }
        });
    });
})(jQuery);
</script>
@endsection
