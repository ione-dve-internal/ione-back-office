@extends('layouts.app')
@section('title', 'New Banner')

@section('header')
    <!-- BEGIN CHECK IMAGE DIMENSIONS -->
    <script type="text/javascript" src="{{url('assets/scripts/checkimagedimensions.js')}}"></script>
    <!-- END CHECK IMAGE DIMENSIONS -->
    <!--AUTOCOMPLETE SEARCH-->
    <link rel="stylesheet" type="text/css" href="{{url('css/autocomplete.css')}}">
    <!--End AUTOCOMPLETE SEARCH-->
@endsection

@section('content')
    <!-- BEGIN DASHBOARD HEADER -->
    <div class="ks-header">
        <section class="ks-title">
            <h3>Create Banner</h3>
            <div class="ks-controls">
                <nav class="breadcrumb ks-default">
                    <a class="breadcrumb-item ks-breadcrumb-icon" href="{{url('dashboard')}}">
                        <span class="fa fa-home ks-icon"></span>
                    </a>
                    <span class="breadcrumb-item active">New Item</span>
                    <a href="{{url('banner')}}" class="breadcrumb-item">Back</a>
                </nav>
            </div>
        </section>
    </div>
    <!-- END DASHBOARD HEADER -->

    <!-- BEGIN DASHBOARD CONTENT -->
    <div class="ks-content">
        <div class="ks-body">
            <div class="container-fluid">
                <form role="form" method="post" enctype="multipart/form-data" action="{{url('banner/insert')}}">
                    <label>Product</label>
                    <div class="form-group">
                        <div>
                            <input type="hidden" name="productId" id="proId" class="form-control">
                        </div>
                        <div class="autocomplete" style="width: 100%">
                            <input id="myInput" type="text" name="product" placeholder="Search product" class="form-control" autocomplete="off" style="background-color: transparent">
                        </div>

                    </div>
                    <br>
                    <div class="form-group">
                        <label>Image</label>
                                <input type="file" name="image" id="file" required class="form-control">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                    </div>
                    <div class="form-group" id="status">
                        <label class="margin-right-10">Status</label>
                        <label class="custom-control custom-radio" id="radio">
                            <input id="radio1" name="status" value="1" type="radio" class="custom-control-input" checked>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">&nbsp; True</span>
                        </label>
                        <label class="custom-control custom-radio" id="radio">
                            <input id="radio1" name="status" value="0" type="radio" class="custom-control-input">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">&nbsp; False</span>
                        </label>
                    </div>

                    <div class="form-group">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button type="submit" name="save" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END DASHBOARD CONTENT -->
    <script type="text/javascript">
       function autocomplete(search, arr){
           var currentFocus;

           /*execute a function when someone writes in the text field:*/
           search.addEventListener("input"  , function(e) {
               var a, b, i, val=this.value;
               closeAllLists();
               if(!val){return false;}
               currentFocus=-1;
               a=document.createElement("DIV");
               a.setAttribute("id", this.id + "autocomplete-list");
               a.setAttribute("class", "autocomplete-items");
               this.parentNode.appendChild(a);

               for (i = 0; i < arr.length; i++){
                   if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()){
                       b = document.createElement("DIV");

                       b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                       b.innerHTML += arr[i].substr(val.length);
                       b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";

                       b.addEventListener("click", function (e) {
                           search.value = this.getElementsByTagName("input")[0].value;
                           closeAllLists();
                       });
                       a.appendChild(b);
                   }
               }
           });

           /*execute a function presses a key on the keyboard:*/
           search.addEventListener("keydown", function(e){
               var x = document.getElementById(this.id + "autocomplete-list");
               if (x) x = x.getElementsByTagName("div");

               if (e.keyCode == 40){
                   currentFocus++;
                   addActive(x);
               }else if (e.keyCode == 38){
                   currentFocus--;
                   addActive(x);
               }else if (e.keyCode == 13){
                   e.preventDefault();
                   if (currentFocus > -1){
                       if (x) x[currentFocus].click();
                   }
               }
           });



           function addActive(x){
               if (!x) return false;
               removeActive(x);

               if (currentFocus >= x.length) currentFocus = 0;
               if (currentFocus < 0) currentFocus = (x.length - 1);

               x[currentFocus].classList.add("autocomplete-active");
           }

           function removeActive(x){
               for (var i = 0; i < x.length; i++) {
                   x[i].classList.remove("autocomplete-active");
               }
           }

           function closeAllLists(elmnt){
               var x = document.getElementsByClassName("autocomplete-items");
               for (var i = 0; i < x.length; i++) {
                   if (elmnt != x[i] && elmnt != search) {
                       x[i].parentNode.removeChild(x[i]);
                   }
               }
           }


           /*execute a function when someone clicks in the document:*/
           document.addEventListener("click", function (e) {
               closeAllLists(e.target);

               var inputProName = document.getElementById("myInput");
               var inputProId = document.getElementById("proId");
               var valueProName = inputProName.value;
               var index = productNames.indexOf(valueProName);
               if (index === -1) {
                   inputProName.value = "";
               }else{
                   inputProId.value = productIds[index];
               }
           });

       }

       var productName = ' <?php
                            echo $productNames;
                        ?>
                    ';
       var productNames = productName.split(',');
       var productId = ' <?php
                            echo $productIds;
                            ?>
                        ';
       var productIds = productId.split(',');


       autocomplete(document.getElementById("myInput"), productNames);

    </script>
    <div class="ks-scrollable"></div>

@endsection

@section('footer')

@endsection



