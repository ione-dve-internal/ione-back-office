
@extends('layouts.app')
@section('title', 'Change Banner Image')

@section('header')

@endsection

@section('content')
    <!-- BEGIN DASHBOARD HEADER -->
    <div class="ks-header">
        <section class="ks-title">
            <h3>Change Banner Image</h3>
            <div class="ks-controls">
                <nav class="breadcrumb ks-default">
                    <a class="breadcrumb-item ks-breadcrumb-icon" href="index.html">
                        <span class="fa fa-home ks-icon"></span>
                    </a>
                    <span class="breadcrumb-item active">Banner</span>
                    <a href="{{url('banner')}}" class="breadcrumb-item">back</a>
                </nav>
            </div>
        </section>
    </div>
    <!-- END DASHBOARD HEADER -->

    <!-- BEGIN DASHBOARD CONTENT -->
    <div class="ks-content">
        <div class="ks-body">
            <div class="container-fluid">
                <div class="row">

                    <div class="col-lg-6">
                        <h4>Upload Banner</h4>
                        <div class="card panel panel-default ks-information ks-light">
                            <h5 class="card-header">
                                <form role="form" method="post" enctype="multipart/form-data" action="{{url('banner/uploadbanner/'.$result->getObjectId())}}">
                                    <input type="file" name="image" id="file" required >
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <button type="submit" class="btn btn-primary-outline ks-light ks-solid">Upload</button>
                                </form>
                            </h5>
                            <div class="card-block text-center">
                                <img src="{{ $result->get('image')!=NULL?$result->get('image')->getUrl():'' }}" style="width:40%;">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- END DASHBOARD CONTENT -->
    <div class="ks-scrollable"></div>
@endsection

@section('footer')

@endsection
