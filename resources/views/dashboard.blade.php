@extends('layouts.app')
@section('title', 'Dashboard')

@section('header')
<link rel="stylesheet" type="text/css" href="assets/styles/widgets/panels.min.css">
<link rel="stylesheet" type="text/css" href="assets/scripts/charts/area/area.chart.min.css">
<link rel="stylesheet" type="text/css" href="assets/scripts/charts/radial-progress/radial-progress.chart.min.css">
@endsection

@section('content')
<!-- BEGIN DASHBOARD HEADER -->
<div class="ks-header">
   <section class="ks-title">
      <h3>Dashboard</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="index.html">
            <span class="fa fa-home ks-icon"></span>
            </a>
            <span class="breadcrumb-item active" href="#">Dashboard</span>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->

<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid ks-rows-section">
         <div class="row ks-widgets-collection">

            <div class="col-lg-3">
               <div class="ks-widget ks-widget-simple-solid-amount ks-primary">
               <span class="ks-amount">{{$numberOfPromotion}}</span>
               <span class="ks-description">Total promotions on your system</span>
               </div>
            </div>
            <div class="col-lg-3">
               <div class="ks-widget ks-widget-simple-solid-amount ks-info">
               <span class="ks-amount">{{$numberOfShops}}</span>
               <span class="ks-description">Total shops on your system</span>
               </div>
            </div>
            <div class="col-lg-3">
               <div class="ks-widget ks-widget-simple-solid-amount ks-success">
               <span class="ks-amount">{{$numberOfCards}}</span>
               <span class="ks-description">Total cards on your system</span>
               </div>
            </div>
            <div class="col-lg-3">
               <div class="ks-widget ks-widget-simple-solid-amount ks-danger">
               <span class="ks-amount">{{$numberOfFeedback}}</span>
               <span class="ks-description">Total Feedback on your system</span>
               </div>
            </div>

         </div>
      </div>
   </div>
</div>
<!-- END DASHBOARD CONTENT -->
<div class="ks-scrollable"></div>
@endsection
