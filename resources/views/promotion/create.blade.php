@extends('layouts.app')
@section('title', 'New Promotion')

@section('header')
   <!-- BEGIN DATETIME PICKER -->
   <script type="text/javascript" src="{{url('assets/scripts/jquery.min.js')}}"></script>
   <script type="text/javascript" src="{{url('assets/scripts/datetimepicker/jquery.timepicker.js')}}"></script>
   <link rel="stylesheet" type="text/css" href="{{url('assets/styles/datetimepicker/jquery.timepicker.css')}}" />
   <script type="text/javascript" src="{{url('assets/scripts/datetimepicker/bootstrap-datepicker.js')}}"></script>
   <link rel="stylesheet" type="text/css" href="{{url('assets/styles/datetimepicker/bootstrap-datepicker.css')}}" />
   <!-- END DATETIME PICKER -->

   <!-- BEGIN CHECK IMAGE DIMENSIONS -->
   <script type="text/javascript" src="{{url('assets/scripts/checkimagedimensions.js')}}"></script>
   <!-- END CHECK IMAGE DIMENSIONS -->

   <link rel="stylesheet" type="text/css" href="{{url('libs/select2/css/select2.min.css')}}"> <!-- Original -->
   <link rel="stylesheet" type="text/css" href="{{url('assets/styles/libs/select2/select2.min.css')}}"> <!-- Customization -->
@endsection

@section('content')
<!-- BEGIN DASHBOARD HEADER -->
<div class="ks-header">
   <section class="ks-title">
      <h3>Create Promotion</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="{{url('dashboard')}}">
               <span class="fa fa-home ks-icon"></span>
            </a>
            <span class="breadcrumb-item active">New Item</span>
            <a href="{{url('promotion')}}" class="breadcrumb-item">Back</a>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->

<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid">
         <form role="form" method="post" enctype="multipart/form-data" action="{{url('promotion/insert')}}">
   			<div class="form-group">
   				<input class="form-control" type="number" name="order" placeholder="Order (Number)" required>
   			</div>
            <div class="form-group">
					<input class="form-control" type="text" name="title" placeholder="Title (Required)" required>
				</div>
            <div class="form-group">
               <select class="form-control ks-select" name="shop">
                  @foreach($shops as $shop)
                     <option value="{{ $shop->getObjectId() }}">
                        {{ $shop->get('name') }}
                     </option>
                  @endforeach
               </select>
            </div>
            <div class="form-group">
               <p id="datepairClaim">
                  <input type="text" name="available" class="date start datepicker-control" placeholder="Start Date (Required)" required/>
                  <input type="text" name="expired" class="date end datepicker-control" placeholder="End Date (Required)" required/>
               </p>
            </div>
            <div class="form-group">
					<input class="form-control" type="text" name="video" placeholder="Video URL (Optional)">
				</div>
            <div class="form-group">
               <label class="margin-right-10">Status</label>
               <label class="custom-control custom-radio">
                  <input id="radio1" name="status" value="1" type="radio" class="custom-control-input" checked>
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description">True</span>
               </label>
               <label class="custom-control custom-radio">
                  <input id="radio1" name="status" value="0" type="radio" class="custom-control-input">
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description">False</span>
               </label>
            </div>
            <div class="form-group">
					<input type="file" name="thumb" id="file" required >
               <label class="margin-right-10">Upload Thumbnail (750px X 240px)</label>
				</div>
            <div class="form-group">
					<input type="file" name="fullBanner" required >
               <label class="margin-right-10">Upload Banner</label>
				</div>

            <div class="form-group">
               <label><input type="checkbox" name="push"> Push Notification</label>
            </div>

				<div class="form-group">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
   				<button type="submit" name="save" class="btn btn-primary">Save</button>
				</div>
         </form>

         <!-- BEING DATATIME PICKER SCRIPT -->
         <script src="{{url('assets/scripts/datetimepicker/datepair.js')}}"></script>
         <script src="{{url('assets/scripts/datetimepicker/jquery.datepair.js')}}"></script>

         <script>
            $('#datepairClaim .time').timepicker({
            'showDuration': true,
            'timeFormat': 'g:ia'
            });

            $('#datepairClaim .date').datepicker({
            'format': 'm/d/yyyy',
            'autoclose': true
            });

            $('#datepairClaim').datepair();
         </script>
         <!-- BEING DATATIME PICKER SCRIPT -->
      </div>
   </div>
</div>
<!-- END DASHBOARD CONTENT -->
<div class="ks-scrollable"></div>
@endsection

@section('footer')
<script src="{{url('libs/select2/js/select2.min.js')}}"></script>
<script type="application/javascript">
(function ($) {
    $(document).ready(function() {
        function formatRepo (repo) {
            if (repo.loading) return repo.text;

            var markup = "<div class='ks-search-result'>" +
                "<div class='ks-avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
                "<div class='ks-meta'>" +
                "<div class='ks-title'>" + repo.full_name + "</div>";

            if (repo.description) {
                markup += "<div class='ks-description'>" + repo.description + "</div>";
            }

            markup += "<div class='ks-statistics'>" +
                "<div class='ks-forks'><i class='fa fa-flash'></i> " + repo.forks_count + " Forks</div>" +
                "<div class='ks-stargazers'><i class='fa fa-star'></i> " + repo.stargazers_count + " Stars</div>" +
                "<div class='ks-watchers'><i class='fa fa-eye'></i> " + repo.watchers_count + " Watchers</div>" +
                "</div>" +
                "</div></div>";

            return markup;
        }

        function formatRepoSelection (repo) {
            return repo.full_name || repo.text;
        }

        function formatState (state) {
            if (!state.id) {
                return state.text;
            }

            var $state = $(
                '<span class="ks-user"><img src="assets/img/avatars/avatar-1.jpg" class="ks-avatar" /> <span class="ks-text">' + state.text + '</span></span>'
            );

            return $state;
        }

        $('select.ks-select').select2();

        $('select.ks-select-placeholder-single').select2({
            placeholder: "Select a state",
            allowClear: true
        });

        $('select.ks-select-placeholder-multiple').select2({
            placeholder: "Select a state"
        });

        $(".ks-load-remote-data").select2({
            ajax: {
                url: "https://api.github.com/search/repositories",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });

        $('select.ks-select-limited-number-of-selections').select2({
            maximumSelectionLength: 2
        });

        $('select.ks-select-hiding-search-box').select2({
            minimumResultsForSearch: Infinity
        });

        $('select.ks-select-tagging-support').select2({
            tags: true
        });

        $('select.ks-select-rtl-support').select2({
            dir: 'rtl'
        });

        $('select.ks-select-templating').select2({
            templateResult: formatState
        });
    });
})(jQuery);
</script>
@endsection
