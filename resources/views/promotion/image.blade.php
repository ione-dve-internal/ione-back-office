@extends('layouts.app')
@section('title', 'Promotion Images')

@section('header')
<!-- BEGIN CHECK IMAGE DIMENSIONS -->
<script type="text/javascript" src="{{url('assets/scripts/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/scripts/checkimagedimensions.js')}}"></script>
<!-- END CHECK IMAGE DIMENSIONS -->
@endsection

@section('content')
<!-- BEGIN DASHBOARD HEADER -->
<div class="ks-header">
   <section class="ks-title">
      <h3>Promotion Images</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="index.html">
               <span class="fa fa-home ks-icon"></span>
            </a>
            <span class="breadcrumb-item active" href="#">Images</span>
            <a href="{{url('promotion')}}" class="breadcrumb-item">back</a>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->

<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid">
         <div class="row">

            <div class="col-lg-6">
               <h4>Upload Thumbnail (750px X 240px)</h4>
               <div class="card panel panel-default ks-information ks-light">
                  <h5 class="card-header">
                     <form role="form" method="post" enctype="multipart/form-data" action="{{url('promotion/uploadthumb/'.$result->getObjectId())}}">
                        <input type="file" name="thumb" id="file" required >
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button type="submit" class="btn btn-primary-outline ks-light ks-solid">Upload</button>
                     </form>
                  </h5>
                  <div class="card-block">
                     <img src="{{ $result->get('thumb')!=NULL?$result->get('thumb')->getUrl():'' }}" style="width:100%">
                  </div>
               </div>
            </div>

            <div class="col-lg-6">
               <h4>Upload Banner</h4>
               <div class="card panel panel-default ks-information ks-light">
                  <h5 class="card-header">
                     <form role="form" method="post" enctype="multipart/form-data" action="{{url('promotion/uploadbanner/'.$result->getObjectId())}}">
                        <input type="file" name="fullBanner" required >
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button type="submit" class="btn btn-primary-outline ks-light ks-solid">Upload</button>
                     </form>
                  </h5>
                  <div class="card-block">
                     <img src="{{ $result->get('fullBanner')!=NULL?$result->get('fullBanner')->getUrl():'' }}" style="width:100%">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- END DASHBOARD CONTENT -->
<div class="ks-scrollable"></div>
@endsection

@section('footer')

@endsection
