@extends('layouts.app')
@section('title', 'Promotion')

@section('header')

@endsection

@section('content')
<!-- BEGIN DASHBOARD HEADER -->
<div class="ks-header">
   <section class="ks-title">
      <h3>Promotion Management</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="{{url('dashboard')}}">
               <span class="fa fa-home ks-icon"></span>
            </a>
            <span class="breadcrumb-item active">Promotion</span>
            <a href="{{url('promotion/create')}}" class="breadcrumb-item">New Item</a>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->

<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid">
         <table class="table table-striped table-bordered" width="100%">
            <thead>
               <tr>
                  <th colspan="8">
                     <form class="form-inline" action="{{url('promotion')}}" style="float:right;">
                        <div class="form-group">
                           <input class="form-control" style="width:200px;" type="text" name="search" placeholder="Search..." value="{{$search}}" required>
                        </div>
                     </form>
                  </th>
               </tr>
               <tr>
                  <th>Order</th>
                  <th>Name</th>
                  <th>Shop</th>
                  <th>Avaiable</th>
                  <th>Expired</th>
                  <th>Status</th>
                  <th>Action</th>
               </tr>
            </thead>
            <tbody>
               @foreach($results as $result)
                  <tr>
                     <td>{{ $result->get('order') }}</td>
                     <td>{{ $result->get('title') }}</td>
                     <td>{{ $result->get("shop")?$result->get("shop")->get("name"):'--' }}</td>
                     <td>{{ $result->get('available')->format('d-M-Y') }}</td>
                     <td>{{ $result->get('expired')->format('d-M-Y') }}</td>
                     <td>
                        @if($result->get('status')==1)
                        <span class="badge ks-circle badge-success">Enabled</span>
                        @else
                        <span class="badge ks-circle badge-danger">Disabled</span>
                        @endif
                     </td>
                     <td class="table-actions">
                        <div class="dropdown padding-top-10">
                           <a class="btn btn-link" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <span class="fa fa-ellipsis-h"></span>
                           </a>
                           <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                              <a class="dropdown-item" href="{{url('promotion/image/'.$result->getObjectId())}}">
                                 <span class="fa fa-image icon text-primary-on-hover"></span> Change image
                              </a>
                              <a class="dropdown-item" href="{{url('promotion/edit/'.$result->getObjectId())}}">
                                 <span class="fa fa-pencil icon text-primary-on-hover"></span> Edit info
                              </a>
                              <a class="dropdown-item" href="{{url('promotion/delete/'.$result->getObjectId())}}" onclick="return confirm('Do you want to delete {{ $result->get('title') }} promotion?')";>
                                 <span class="fa fa-trash icon text-danger-on-hover"></span> Delete
                              </a>
                           </div>
                        </div>
                     </td>
                  </tr>
               @endforeach
            </tbody>
         </table>
         <!-- being pagination -->
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
               <nav aria-label="Page navigation example">
                  <ul class="pagination">
                     @if($totalPage<=10)
                        <li class="page-item"><a class="page-link" href="{{url('promotion/page/'.$previous)}}">Previous</a></li>
                        @for($i=1; $i <= $totalPage; $i++)
                           <li class="page-item {{$i == $currentPage?'active':''}}">
                              <a class="page-link" href="{{url('promotion/page/'.$i)}}">{{ $i }}</a>
                           </li>
                        @endfor
                        <li class="page-item"><a class="page-link" href="{{url('promotion/page/'.$next)}}">Next</a></li>
                     @else
                        @if($currentPage<$totalPage-10)
                           @if($currentPage>3)
                              <li class="page-item"><a class="page-link" href="{{url('promotion/page/'.$previous)}}">Previous</a></li>
                              @for($i=$currentPage-3; $i < $currentPage+5; $i++)
                                 <li class="page-item {{$i == $currentPage?'active':''}}">
                                    <a class="page-link" href="{{url('promotion/page/'.$i)}}">{{ $i }}</a>
                                 </li>
                              @endfor
                           @else
                              <li class="page-item"><a class="page-link" href="{{url('promotion/page/'.$previous)}}">Previous</a></li>
                              @for($i=1; $i <= 9; $i++)
                                 <li class="page-item {{$i == $currentPage?'active':''}}">
                                    <a class="page-link" href="{{url('promotion/page/'.$i)}}">{{ $i }}</a>
                                 </li>
                              @endfor
                           @endif

                           <li class="page-item"><span class="page-link">...</span></li>
                           @for($i=$totalPage-2; $i < $totalPage; $i++)
                              <li class="page-item {{$i == $currentPage?'active':''}}">
                                 <a class="page-link" href="{{url('promotion/page/'.$i)}}">{{ $i }}</a>
                              </li>
                           @endfor
                           <li class="page-item"><a class="page-link" href="{{url('promotion/page/'.$next)}}">Next</a></li>
                        @else
                           <li class="page-item"><a class="page-link" href="{{url('promotion/page/'.$previous)}}">Previous</a></li>
                           @for($i=8; $i <= 10; $i++)
                              <li class="page-item {{$i == $currentPage?'active':''}}">
                                 <a class="page-link" href="{{url('promotion/page/'.$i)}}">{{ $i }}</a>
                              </li>
                           @endfor

                           <li class="page-item"><span class="page-link">...</span></li>
                           @for($i=$totalPage-10; $i < $totalPage; $i++)
                              <li class="page-item {{$i == $currentPage?'active':''}}">
                                 <a class="page-link" href="{{url('promotion/page/'.$i)}}">{{ $i }}</a>
                              </li>
                           @endfor
                           <li class="page-item"><a class="page-link" href="{{url('promotion/page/'.$next)}}">Next</a></li>
                        @endif
                     @endif
                  </ul>
               </nav>
            </div>
         </div>
         <!-- end pagination -->
      </div>
   </div>
</div>
<!-- END DASHBOARD CONTENT -->
<div class="ks-scrollable"></div>
@endsection

@section('footer')

@endsection
