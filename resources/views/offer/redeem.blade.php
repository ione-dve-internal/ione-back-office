@extends('layouts.app')
@section('title', 'User Redeem')

@section('header')
<!-- BEGIN DATETIME PICKER -->
<script type="text/javascript" src="{{url('assets/scripts/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/scripts/datetimepicker/jquery.timepicker.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{url('assets/styles/datetimepicker/jquery.timepicker.css')}}" />
<script type="text/javascript" src="{{url('assets/scripts/datetimepicker/bootstrap-datepicker.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{url('assets/styles/datetimepicker/bootstrap-datepicker.css')}}" />
<!-- END DATETIME PICKER -->
@endsection

@section('content')
<!-- BEGIN DASHBOARD HEADER -->
<div class="ks-header">
   <section class="ks-title">
      <h3>User Redeem</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="{{url('dashboard')}}">
               <span class="fa fa-home ks-icon"></span>
            </a>
            <span class="breadcrumb-item active">Redeem</span>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->


<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid">
         <table class="table table-striped table-bordered" width="100%">
            <thead>
               <tr>
                  <th colspan="3">
                     <form class="form-inline" action="{{url('redeem/export')}}" style="float:left;">
                        <div class="form-group">
                           <div id="datepairClaim">
                              <input type="text" name="start" class="date start form-control" placeholder="Start Date (Required)" required/>
                              <input type="text" name="end" class="date end form-control" placeholder="End Date (Required)" required/>
                              <input type="submit" class="btn btn-default" value="Export">
                           </div>
                        </div>
                     </form>
                     <form class="form-inline" action="{{url('redeem')}}" style="float:right;">
                        <div class="form-group">
                           <input class="form-control" style="width:200px;" type="text" name="search" placeholder="Search..." value="{{$search}}" required>
                        </div>
                     </form>

                     <!-- BEING DATATIME PICKER SCRIPT -->
                     <script src="{{url('assets/scripts/datetimepicker/datepair.js')}}"></script>
                     <script src="{{url('assets/scripts/datetimepicker/jquery.datepair.js')}}"></script>

                     <script>
                        $('#datepairClaim .time').timepicker({
                        'showDuration': true,
                        'timeFormat': 'g:ia'
                        });

                        $('#datepairClaim .date').datepicker({
                        'format': 'm/d/yyyy',
                        'autoclose': true
                        });

                        $('#datepairClaim').datepair();
                     </script>
                     <!-- BEING DATATIME PICKER SCRIPT -->
                  </th>
               </tr>
               <tr>
                  <th>User Name</th>
                  <th>Offer Name</th>
                  <th>Created Date</th>
               </tr>
            </thead>
            <tbody>
               @foreach($results as $result)
                  <tr>
                     <td>{{ $result->get('user')?$result->get('user')->get('firstName').' '.$result->get('user')->get('lastName'):'--' }}</td>
                     <td>{{ $result->get('offer')->get('title') }}</td>
                     <td>{{ $result->getCreatedAt()->format('d-M-Y') }}</td>
                  </tr>
               @endforeach
            </tbody>
         </table>
         <!-- being pagination -->
         @if($totalPage>0)
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12">
                  <nav aria-label="Page navigation example">
                     <ul class="pagination">
                        @if($totalPage<=10)
                           <li class="page-item"><a class="page-link" href="{{url('redeem/page/'.$previous)}}">Previous</a></li>
                           @for($i=1; $i <= $totalPage; $i++)
                              <li class="page-item {{$i == $currentPage?'active':''}}">
                                 <a class="page-link" href="{{url('redeem/page/'.$i)}}">{{ $i }}</a>
                              </li>
                           @endfor
                           <li class="page-item"><a class="page-link" href="{{url('redeem/page/'.$next)}}">Next</a></li>
                        @else
                           @if($currentPage<$totalPage-10)
                              @if($currentPage>3)
                                 <li class="page-item"><a class="page-link" href="{{url('redeem/page/'.$previous)}}">Previous</a></li>
                                 @for($i=$currentPage-3; $i < $currentPage+5; $i++)
                                    <li class="page-item {{$i == $currentPage?'active':''}}">
                                       <a class="page-link" href="{{url('redeem/page/'.$i)}}">{{ $i }}</a>
                                    </li>
                                 @endfor
                              @else
                                 <li class="page-item"><a class="page-link" href="{{url('redeem/page/'.$previous)}}">Previous</a></li>
                                 @for($i=1; $i <= 9; $i++)
                                    <li class="page-item {{$i == $currentPage?'active':''}}">
                                       <a class="page-link" href="{{url('redeem/page/'.$i)}}">{{ $i }}</a>
                                    </li>
                                 @endfor
                              @endif

                              <li class="page-item"><span class="page-link">...</span></li>
                              @for($i=$totalPage-2; $i < $totalPage; $i++)
                                 <li class="page-item {{$i == $currentPage?'active':''}}">
                                    <a class="page-link" href="{{url('redeem/page/'.$i)}}">{{ $i }}</a>
                                 </li>
                              @endfor
                              <li class="page-item"><a class="page-link" href="{{url('redeem/page/'.$next)}}">Next</a></li>
                           @else
                              <li class="page-item"><a class="page-link" href="{{url('redeem/page/'.$previous)}}">Previous</a></li>
                              @for($i=8; $i <= 10; $i++)
                                 <li class="page-item {{$i == $currentPage?'active':''}}">
                                    <a class="page-link" href="{{url('redeem/page/'.$i)}}">{{ $i }}</a>
                                 </li>
                              @endfor

                              <li class="page-item"><span class="page-link">...</span></li>
                              @for($i=$totalPage-10; $i < $totalPage; $i++)
                                 <li class="page-item {{$i == $currentPage?'active':''}}">
                                    <a class="page-link" href="{{url('redeem/page/'.$i)}}">{{ $i }}</a>
                                 </li>
                              @endfor
                              <li class="page-item"><a class="page-link" href="{{url('redeem/page/'.$next)}}">Next</a></li>
                           @endif
                        @endif
                     </ul>
                  </nav>
               </div>
            </div>
         @endif
         <!-- end pagination -->
      </div>
   </div>
</div>
<!-- END DASHBOARD CONTENT -->
<div class="ks-scrollable"></div>
@endsection

@section('footer')

@endsection
