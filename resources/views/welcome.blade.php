<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <title>iOne - Administrator</title>

      <meta http-equiv="X-UA-Compatible" content=="IE=edge"/>
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <link rel="stylesheet" type="text/css" href="{{url('libs/bootstrap/css/bootstrap.min.css')}}">
      <link rel="stylesheet" type="text/css" href="{{url('libs/font-awesome/css/font-awesome.min.css')}}">
      <link rel="stylesheet" type="text/css" href="{{url('libs/tether/css/tether.min.css')}}">
      <link rel="stylesheet" type="text/css" href="{{url('assets/styles/common.min.css')}}">
      <link rel="stylesheet" type="text/css" href="{{url('assets/styles/pages/auth.min.css')}}">
      <link rel="stylesheet" type="text/css" href="{{url('assets/styles/custome.min.css')}}">
   </head>
   <body>
      <div class="ks-page">
         <div class="ks-body">
            <div class="ks-logo">iOne</div>
            <div class="card panel panel-default ks-light ks-panel">
               <div class="card-block">
                  <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                     <h4 class="ks-header">All Please sign in account</h4>
                     <div class="form-group">
                        <div class="input-icon icon-left icon-lg icon-color-primary">
                        <input type="text" name="username" class="form-control" placeholder="Username" required="required">
                        <span class="icon-addon">
                        <span class="fa fa-user"></span>
                        </span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="input-icon icon-left icon-lg icon-color-primary">
                           <input type="password" name="password" class="form-control" placeholder="Password" required="required">
                           <span class="icon-addon">
                              <span class="fa fa-key"></span>
                           </span>
                        </div>
                     </div>
                     @if(Session::has('message'))
                     <div class="form-group help-block">
                        {!! Session::get('message') !!}
                     </div>
                     @endif
                     <div class="form-group">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
         <div class="ks-footer">
            <span class="ks-copyright">&copy; 2017 iOne, All Rights Reserved</span>
         </div>
      </div>

      <script src="{{url('libs/jquery/jquery.min.js')}}"></script>
      <script src="{{url('libs/tether/js/tether.min.js')}}"></script>
      <script src="{{url('libs/bootstrap/js/bootstrap.min.js')}}"></script>
   </body>
</html>
