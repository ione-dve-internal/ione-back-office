@extends('layouts.app')
@section('title', 'About Us')

@section('header')

   <!-- BEGIN CK EDITOR SCRIPT -->
   <script type="text/javascript" src="{{url('assets/scripts/jquery.min.js')}}"></script>
   <script type="text/javascript" src="{{url('assets/ckeditor/ckeditor.js')}}"></script>
   <!-- END CK EDITOR SCRIPT -->

@endsection

@section('content')
<!-- BEGIN DASHBOARD HEADER -->
<div class="ks-header">
   <section class="ks-title">
      <h3>About Us</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="{{url('dashboard')}}">
               <span class="fa fa-home ks-icon"></span>
            </a>
            <span class="breadcrumb-item active" href="#">About Us</span>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->

<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid">
         <form role="form" method="post" enctype="multipart/form-data" action="{{url('page/update/'.$result->getObjectId())}}">
   			<div class="form-group">
   				<textarea id="aboutus" name="content">{!! $result->get('content') !!}</textarea>
   			</div>
            <div class="form-group">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
   				<button type="submit" name="save" class="btn btn-primary">Update</button>
				</div>
         </form>
      </div>
   </div>
</div>
<!-- END DASHBOARD CONTENT -->
<div class="ks-scrollable"></div>
@endsection

@section('footer')
   <script>
		CKEDITOR.replace( 'aboutus' );
	</script>

@endsection
