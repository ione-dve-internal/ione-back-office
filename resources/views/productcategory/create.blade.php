@extends('layouts.app')
@section('title', 'New Product Category')

@section('header')
   <!-- BEGIN CHECK IMAGE DIMENSIONS -->
   <script type="text/javascript" src="{{url('assets/scripts/checkimagedimensions.js')}}"></script>
   <!-- END CHECK IMAGE DIMENSIONS -->
@endsection

@section('content')
<!-- BEGIN DASHBOARD HEADER -->
<div class="ks-header">
   <section class="ks-title">
      <h3>Create Shop Category</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="{{url('dashboard')}}">
               <span class="fa fa-home ks-icon"></span>
            </a>
            <span class="breadcrumb-item active">New Item</span>
            <a href="{{url('productcategory')}}" class="breadcrumb-item">Back</a>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->

<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid">
         <form role="form" method="post" enctype="multipart/form-data" action="{{url('productcategory/insert')}}">
            <div class="form-group">
					<input class="form-control" type="text" name="title" placeholder="Product Category (Required)" required>
				</div>
            <div class="form-group">
					<input type="file" name="icon" required >
               <label class="margin-right-10">Upload Icon</label>
				</div>
				<div class="form-group">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
   				<button type="submit" name="save" class="btn btn-primary">Save</button>
				</div>
         </form>
      </div>
   </div>
</div>
<!-- END DASHBOARD CONTENT -->
<div class="ks-scrollable"></div>
@endsection

@section('footer')

@endsection
