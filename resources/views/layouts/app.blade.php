<!DOCTYPE html>
<html lang="en">
   <!-- BEGIN HEAD -->
   <head>
      <meta charset="UTF-8">
      <title>iOne - @yield('title')</title>

      <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">

      <!-- BEGIN GLOBAL MANDATORY STYLES -->
      <link rel="stylesheet" type="text/css" href="{{url('libs/bootstrap/css/bootstrap.min.css')}}">
      <link rel="stylesheet" type="text/css" href="{{url('libs/font-awesome/css/font-awesome.min.css')}}">
      <link rel="stylesheet" type="text/css" href="{{url('assets/fonts/open-sans/styles.css')}}">
      <link rel="stylesheet" type="text/css" href="{{url('libs/tether/css/tether.min.css')}}">
      <link rel="stylesheet" type="text/css" href="{{url('libs/jscrollpane/jquery.jscrollpane.css')}}">
      <link rel="stylesheet" type="text/css" href="{{url('libs/flag-icon-css/css/flag-icon.min.css')}}">
      <link rel="stylesheet" type="text/css" href="{{url('assets/styles/common.min.css')}}">
      <link rel="stylesheet" type="text/css" href="{{url('assets/styles/custome.min.css')}}">
      <link rel="stylesheet" type="text/css" href="{{url('assets/styles/basic.min.css')}}">
      <!-- END GLOBAL MANDATORY STYLES -->

      <!-- BEGIN THEME STYLES -->
      <link rel="stylesheet" type="text/css" href="{{url('assets/styles/themes/primary.min.css')}}">
      <!-- END THEME STYLES -->
      @yield('header')
   </head>
   <!-- END HEAD -->

   <body class="ks-navbar-fixed ks-sidebar-default ks-sidebar-fixed ks-theme-primary ks-page-loading">
      <!-- BEGIN HEADER -->
      <nav class="navbar ks-navbar">
         <!-- BEGIN LOGO -->
         <div href="dashboard" class="navbar-brand">
            <!-- BEGIN RESPONSIVE SIDEBAR TOGGLER -->
            <a href="#" class="ks-sidebar-toggle"><i class="fa fa-bars" aria-hidden="true"></i></a>
            <a href="#" class="ks-sidebar-mobile-toggle"><i class="fa fa-bars" aria-hidden="true"></i></a>
            <!-- END RESPONSIVE SIDEBAR TOGGLER -->
            <a href="dashboard" class="ks-logo">iOne Administrator</a>
         </div>
         <!-- END LOGO -->

         <!-- BEGIN MENUS -->
         <div class="ks-wrapper">
            <nav class="nav navbar-nav">
               <!-- BEGIN NAVBAR MENU -->
               <div class="ks-navbar-menu"></div>
               <!-- END NAVBAR MENU -->

               <!-- BEGIN NAVBAR ACTIONS -->
               <div class="ks-navbar-actions">
                  <!-- BEGIN NAVBAR USER -->
                  <div class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="ks-info">
                           <span class="fa fa-user-circle-o fa-2x"></span>
                        </span>
                     </a>
                     <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Preview">
                        <a class="dropdown-item" href="#">
                           <span class="fa fa-user ks-icon"></span>
                           <span>{{session()->get('currentUser')->get('firstName').' '.session()->get('currentUser')->get('lastName')}}</span>
                        </a>
                        <a class="dropdown-item" href="{{url('logout')}}">
                           <span class="fa fa-sign-out ks-icon" aria-hidden="true"></span>
                           <span>Logout</span>
                        </a>
                     </div>
                  </div>
                  <!-- END NAVBAR USER -->
               </div>
               <!-- END NAVBAR ACTIONS -->
            </nav>

            <!-- BEGIN NAVBAR ACTIONS TOGGLER -->
            <nav class="nav navbar-nav ks-navbar-actions-toggle">
               <a class="nav-item nav-link" href="#">
                  <span class="fa fa-ellipsis-h ks-icon ks-open"></span>
                  <span class="fa fa-close ks-icon ks-close"></span>
               </a>
            </nav>
            <!-- END NAVBAR ACTIONS TOGGLER -->
         </div>
         <!-- END MENUS -->
      </nav>
      <!-- END HEADER -->

      <div class="ks-container">
         <!-- BEGIN DEFAULT SIDEBAR -->
         <div class="ks-column ks-sidebar ks-info">
            <div class="ks-wrapper">
               <ul class="nav nav-pills nav-stacked">
                  <li class="nav-item">
                     <a class="nav-link" href="{{url('dashboard')}}">
                        <span class="ks-icon fa fa-dashboard"></span>
                        <span>Dashboard</span>
                     </a>
                  </li>
                  @foreach($menus as $menu)
                     @if($menu->get('menu')->get('link'))
                     <li class="nav-item">
                        <a class="nav-link" href="{{$menu->get('menu')->get('link')?url($menu->get('menu')->get('link')):'#'}}">
                           <span class="ks-icon fa {{$menu->get('menu')->get('faIcon')}}"></span>
                           <span>{{$menu->get('menu')->get('name')}}</span>
                        </a>
                     </li>
                     @else
                     <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
                           <span class="ks-icon fa {{$menu->get('menu')->get('faIcon')}}"></span>
                           <span>{{$menu->get('menu')->get('name')}}</span>
                        </a>
                        <div class="dropdown-menu">
                           @foreach($childmenus as $childmenu)
                              @if($childmenu->get('parent')->getObjectId() == $menu->get('menu')->getObjectId())
                                 <a class="dropdown-item" href="{{$childmenu->get('link')?url($childmenu->get('link')):'#'}}">
                                    {{$childmenu->get('name')}}
                                 </a>
                              @endif
                           @endforeach
                        </div>
                     </li>
                     @endif
                  @endforeach
               </ul>
            </div>
         </div>
         <!-- END DEFAULT SIDEBAR -->


         <div class="ks-column ks-page">
            @yield('content')
         </div>
      </div>

      <!-- BEGIN PAGE LEVEL PLUGINS -->
      <script src="{{url('libs/jquery/jquery.min.js')}}"></script>
      <script src="{{url('libs/responsejs/response.min.js')}}"></script>
      <script src="{{url('libs/loading-overlay/loadingoverlay.min.js')}}"></script>
      <script src="{{url('libs/tether/js/tether.min.js')}}"></script>
      <script src="{{url('libs/bootstrap/js/bootstrap.min.js')}}"></script>
      <script src="{{url('libs/jscrollpane/jquery.jscrollpane.min.js')}}"></script>
      <script src="{{url('libs/jscrollpane/jquery.mousewheel.js')}}"></script>
      <script src="{{url('libs/flexibility/flexibility.js')}}"></script>
      <!-- END PAGE LEVEL PLUGINS -->

      <script src="libs/jquery-form-validator/jquery.form-validator.min.js"></script>

      <!-- BEGIN THEME LAYOUT SCRIPTS -->
      <script src="{{url('assets/scripts/common.min.js')}}"></script>
      <!-- END THEME LAYOUT SCRIPTS -->
      @yield('footer')

      <div class="ks-mobile-overlay"></div>

   </body>
</html>
