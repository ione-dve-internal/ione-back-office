@extends('layouts.app')
@section('title', 'Import Card')
@section('content')
<!-- BEGIN DASHBOARD HEADER -->
<div class="ks-header">
   <section class="ks-title">
      <h3>Import User Card</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="{{url('dashboard')}}">
               <span class="fa fa-home ks-icon"></span>
            </a>
            <a href="{{url('cards')}}" class="breadcrumb-item">Card List</a>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->

<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid">
         @if (Session::has('message'))
         <div class="alert alert-info alert-block">
         	<button type="button" class="close" data-dismiss="alert">×</button>
         	<p class="color-red">{!! Session::get('message') !!}</p>
         </div>
         @endif
         <form role="form" method="post" enctype="multipart/form-data" action="{{url('cards/import')}}">
            <div class="col-md-6 mx-auto">
               <div class="card" style="height:300px;">
                  <div class="card-header">
                     <p>Browse csv file</p>
                  </div>
                  <br>
                  <div class="card-body">
                     <div class="form-group col-md-12">
                        <input class="form-control" type="file" name="file" required>
                     </div>
                     <div class="form-group col-md-6">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button type="submit" name="save" class="btn btn-primary btn-block">Import</button>
                     </div>
                  </div>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>
<!-- END DASHBOARD CONTENT -->
<div class="ks-scrollable"></div>
@endsection

@section('footer')

@endsection
