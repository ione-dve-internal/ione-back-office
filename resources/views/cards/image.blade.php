@extends('layouts.app')
@section('title', 'Shop Images')

@section('header')

@endsection

@section('content')
<!-- BEGIN DASHBOARD HEADER -->
<div class="ks-header">
   <section class="ks-title">
      <h3>Shop Images</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="index.html">
               <span class="fa fa-home ks-icon"></span>
            </a>
            <span class="breadcrumb-item active">Images</span>
            <a href="{{url('shop')}}" class="breadcrumb-item">back</a>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->

<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid">
         <div class="row">

            <div class="col-lg-6">
               <h4>Upload Logo</h4>
               <div class="card panel panel-default ks-information ks-light">
                  <h5 class="card-header">
                     <form role="form" method="post" enctype="multipart/form-data" action="{{url('shop/uploadlogo/'.$result->getObjectId())}}">
                        <input type="file" name="logo" id="file" required >
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button type="submit" class="btn btn-primary-outline ks-light ks-solid">Upload</button>
                     </form>
                  </h5>
                  <div class="card-block text-center">
                     <img src="{{ $result->get('logo')!=NULL?$result->get('logo')->getUrl():'' }}" style="width:40%;">
                  </div>
               </div>
            </div>

            <div class="col-lg-6">
               <h4>Upload Shop Photo</h4>
               <div class="card panel panel-default ks-information ks-light">
                  <h5 class="card-header">
                     <form role="form" method="post" enctype="multipart/form-data" action="{{url('shop/uploadphoto/'.$result->getObjectId())}}">
                        <input type="file" name="photo" required >
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button type="submit" class="btn btn-primary-outline ks-light ks-solid">Upload</button>
                     </form>
                  </h5>
                  <div class="card-block">
                     <img src="{{ $result->get('photo')!=NULL?$result->get('photo')->getUrl():'' }}" style="width:100%">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- END DASHBOARD CONTENT -->
<div class="ks-scrollable"></div>
@endsection

@section('footer')

@endsection
