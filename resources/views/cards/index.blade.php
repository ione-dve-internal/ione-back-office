@extends('layouts.app')
@section('title', 'Card')

@section('header')
    <!-- BEGIN DATETIME PICKER -->
    <script type="text/javascript" src="{{url('assets/scripts/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/scripts/datetimepicker/jquery.timepicker.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{url('assets/styles/datetimepicker/jquery.timepicker.css')}}" />
    <script type="text/javascript" src="{{url('assets/scripts/datetimepicker/bootstrap-datepicker.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{url('assets/styles/datetimepicker/bootstrap-datepicker.css')}}" />
    <!-- END DATETIME PICKER -->

    <!-- MULTIPLE SELECT -->
    <link rel="stylesheet" type="text/css" href="{{url('assets/Drop-Down-Combo-Tree/style.css')}}"/>
    <!-- END MULTIPLE SELECT -->

@endsection

@section('content')
    <!-- BEGIN DASHBOARD HEADER -->
    <div class="ks-header">
        <section class="ks-title">
            <h3>Card Management</h3>
            <div class="ks-controls">
                <nav class="breadcrumb ks-default">
                    <a class="breadcrumb-item ks-breadcrumb-icon" href="{{url('dashboard')}}">
                        <span class="fa fa-home ks-icon"></span>
                    </a>
                    <span class="breadcrumb-item active">Card</span>
                    <a href="{{url('cards/create')}}" class="breadcrumb-item">New Item</a>
                </nav>
            </div>
        </section>
    </div>
    <!-- END DASHBOARD HEADER -->


    <!-- BEGIN DASHBOARD CONTENT -->
    <div class="ks-content">
        <div class="ks-body">
            <div class="container-fluid">
                <table class="table table-striped table-bordered" width="100%">
                    <thead>
                    <tr>
                        <th colspan="7">
                            <form class="form-inline" action="{{url('cards')}}" style="float:left;">
                                <div class="form-group">
                                    <div id="datepairClaim">
                                        <select class="form-control" name="filter">
                                            <option value="all" {{$filter=='all'?'selected':''}}>All</option>
                                            <option value="activated" {{$filter=='activated'?'selected':''}}>Card activated</option>
                                            <option value="notactivate" {{$filter=='notactivate'?'selected':''}}>Card not activate</option>
                                        </select>
                                        <input type="text" name="start" class="date start form-control" placeholder="Start Date" value="{{$start ==''?'':date_format($start,'m/d/Y')}}"/>
                                        <input type="text" name="end" class="date end form-control" placeholder="End Date" value="{{$end ==''?'':date_format($end,'m/d/Y')}}">
                                        <input type="text" id="justAnInputBox" placeholder="Select column" name="cols" autocomplete="off" value="{{$cols}}"/>
                                        <input type="submit" class="btn btn-default" value="Filter">
                                        <a href="cards/export?filter={{$filter}}&start={{$startDate}}&end={{$endDate}}&cols={{$cols}}">
                                            <input type="button" class="btn btn-default" value="Export">
                                        </a>
                                    </div>

                                </div>
                            </form>
                            <form class="form-inline" action="{{url('cards')}}" style="float:right;">
                                <div class="form-group">
                                    <input class="form-control" style="width:200px;" type="text" name="search" placeholder="Search..." value="{{$search}}" required>
                                </div>
                            </form>

                            <!-- MULTIPLE SELECT -->
                            <script src="{{url('assets/Drop-Down-Combo-Tree/comboTreePlugin.js')}}"></script>
                            <script src="{{url('assets/Drop-Down-Combo-Tree/icontains.js')}}"></script>
                            <script>
                                var SampleJSONData = [
                                    {
                                        id: 11,
                                        title: 'Card Number'
                                    },
                                    {
                                        id: 22,
                                        title: 'Customer Name'
                                    },
                                    {
                                        id: 33,
                                        title: 'Holder Name'
                                    },
                                    {
                                        id: 44,
                                        title: 'Status'
                                    },
                                    {
                                        id: 55,
                                        title: 'Created Date'
                                    },
                                    {
                                        id: 0,
                                        title: 'First Name'
                                    },
                                    {
                                        id: 1,
                                        title: 'Last Name'
                                    },
                                    {
                                        id: 65,
                                        title: 'DOB'
                                    },
                                    {
                                      id: 21,
                                      title: 'Gender'
                                    },
                                    {
                                        id: 2,
                                        title: 'Customer Type'
                                    },
                                    {
                                        id: 3,
                                        title: 'Children'
                                    },
                                    {
                                        id: 4,
                                        title: 'Address'
                                    },
                                    {
                                        id: 5,
                                        title: 'Phone'
                                    },
                                    {
                                        id:66,
                                        title: 'Email'
                                    },
                                    {
                                        id:666,
                                        title: 'Sangkat'
                                    },
                                    {
                                        id: 6,
                                        title: 'Khan'
                                    },
                                    {
                                        id: 7,
                                        title: 'City'
                                    },
                                    {
                                        id:900,
                                        title: 'Country'
                                    },
                                    {
                                        id: 8,
                                        title: 'Income'
                                    },
                                    {
                                        id: 9,
                                        title: 'Register'
                                    },
                                    {
                                        id: 10,
                                        title: 'Wechat'
                                    },
                                    {
                                        id: 11,
                                        title: 'Facebook'
                                    },
                                    {
                                        id: 12,
                                        title: 'Married'
                                    },
                                    {
                                        id: 13,
                                        title: 'Line'
                                    },
                                    {
                                        id: 123,
                                        title: 'Source Retail'
                                    },
                                    {
                                        id: 43,
                                        title: 'Keyin'
                                    }
                                ];
                                var comboTree;
                                comboTree = $('#justAnInputBox').comboTree({
                                    source : SampleJSONData,
                                    isMultiple: true
                                });
                            </script>


                            <!-- END MULTIPLE SELECT -->






                            <!-- BEING DATATIME PICKER SCRIPT -->
                            <script src="{{url('assets/scripts/datetimepicker/datepair.js')}}"></script>
                            <script src="{{url('assets/scripts/datetimepicker/jquery.datepair.js')}}"></script>

                            <script>
                                $('#datepairClaim .time').timepicker({
                                    'showDuration': true,
                                    'timeFormat': 'g:ia'
                                });

                                $('#datepairClaim .date').datepicker({
                                    'format': 'm/d/yyyy',
                                    'autoclose': true
                                });

                                $('#datepairClaim').datepair();
                            </script>
                            <!-- BEING DATATIME PICKER SCRIPT -->
                        </th>
                    </tr>
                    <tr>
                        <th>Card Number</th>
                        <th>Customer Name</th>
                        <th>Card Holder</th>
                        <th>Card Type</th>
                        <th>Create Date</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($results as $result)
                        <tr>
                            <td>{{ implode("  ", str_split($result->get('cardNumber'), 4) ) }}</td>
                            <td>{{ $result->get('user')? $result->get('user')->get('firstName').' '.$result->get('user')->get('lastName') : "--"}}</td>
                            <td>{{ $result->get('holderName') }}</td>
                            <td>{{ $result->get('cardType')->get('title') ? $result->get('cardType')->get('title') : '' }}</td>
                            <td>{{ $result->getCreatedAt()->format('d-M-Y') }}</td>
                            <td>@if($result->get('user')) {{ $result->get('user')->get('opt')==''?'Not Activate':'Activated' }} @endif</td>
                            <td class="table-actions">
                                <div class="dropdown padding-top-10">
                                    <a class="btn btn-link" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="fa fa-ellipsis-h"></span>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                        <a class="dropdown-item" href="{{url('cards/edit/'.$result->getObjectId())}}">
                                            <span class="fa fa-pencil icon text-primary-on-hover"></span> Edit Info
                                        </a>
                                        <a class="dropdown-item" href="{{url('cards/delete/'.$result->getObjectId())}}" onclick="return confirm('Do you want to delete {{ $result->get('holderName') }} card user?')";>
                                            <span class="fa fa-trash icon text-danger-on-hover"></span> Delete
                                        </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <!-- being pagination -->
                @if($totalPage>0)
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    @if($totalPage<=10)
                                        @if($search)
                                            <li class="page-item"><a class="page-link" href="{{url('cards/page/'.$previous.'?search='.$search)}}">Previous</a></li>
                                            @for($i=1; $i <= $totalPage; $i++)
                                                <li class="page-item {{$i == $currentPage?'active':''}}">
                                                    <a class="page-link" href="{{url('cards/page/'.$i.'?search='.$search)}}">{{ $i }}</a>
                                                </li>
                                            @endfor
                                            <li class="page-item"><a class="page-link" href="{{url('cards/page/'.$next.'?search='.$search)}}">Next</a></li>
                                        @elseif($filter)
                                            <li class="page-item"><a class="page-link" href="{{url('cards/page/'.$previous.'?filter='.$filter.'&start='.$startDate.'&end='.$endDate)}}">Previous</a></li>
                                            @for($i=1; $i <= $totalPage; $i++)
                                                <li class="page-item {{$i == $currentPage?'active':''}}">
                                                    <a class="page-link" href="{{url('cards/page/'.$i.'?filter='.$filter.'&start='.$startDate.'&end='.$endDate.'&cols='.$cols)}}">{{ $i }}</a>
                                                </li>
                                            @endfor
                                            <li class="page-item"><a class="page-link" href="{{url('cards/page/'.$next.'?filter='.$filter.'&start='.$startDate.'&end='.$endDate)}}">Next</a></li>
                                        @else
                                            <li class="page-item"><a class="page-link" href="{{url('cards/page/'.$previous.'?filter='.$filter.'&start='.$startDate.'&end='.$endDate)}}">Previous</a></li>
                                            @for($i=1; $i <= $totalPage; $i++)
                                                <li class="page-item {{$i == $currentPage?'active':''}}">
                                                    <a class="page-link" href="{{url('cards/page/'.$i.'?filter='.$filter.'&start='.$startDate.'&end='.$endDate.'&cols='.$cols)}}">{{ $i }}</a>
                                                </li>
                                            @endfor
                                            <li class="page-item"><a class="page-link" href="{{url('cards/page/'.$next.'?filter='.$filter.'&start='.$startDate.'&end='.$endDate)}}">Next</a></li>
                                        @endif
                                    @else
                                        @if($currentPage<$totalPage-10)
                                            @if($currentPage>3)
                                                <li class="page-item"><a class="page-link" href="{{url('cards/page/'.$previous.'?filter='.$filter.'&start='.$startDate.'&end='.$endDate)}}">Previous</a></li>
                                                @for($i=$currentPage-3; $i < $currentPage+5; $i++)
                                                    <li class="page-item {{$i == $currentPage?'active':''}}">
                                                        <a class="page-link" href="{{url('cards/page/'.$i.'?filter='.$filter.'&start='.$startDate.'&end='.$endDate)}}">{{ $i }}</a>
                                                    </li>
                                                @endfor
                                            @else
                                                <li class="page-item"><a class="page-link" href="{{url('cards/page/'.$previous.'?filter='.$filter.'&start='.$startDate.'&end='.$endDate)}}">Previous</a></li>
                                                @for($i=1; $i <= 9; $i++)
                                                    <li class="page-item {{$i == $currentPage?'active':''}}">
                                                        <a class="page-link" href="{{url('cards/page/'.$i.'?filter='.$filter.'&start='.$startDate.'&end='.$endDate)}}">{{ $i }}</a>
                                                    </li>
                                                @endfor
                                            @endif

                                            <li class="page-item"><span class="page-link">...</span></li>
                                            @for($i=$totalPage-2; $i < $totalPage; $i++)
                                                <li class="page-item {{$i == $currentPage?'active':''}}">
                                                    <a class="page-link" href="{{url('cards/page/'.$i.'?filter='.$filter.'&start='.$startDate.'&end='.$endDate)}}">{{ $i }}</a>
                                                </li>
                                            @endfor
                                            <li class="page-item"><a class="page-link" href="{{url('cards/page/'.$next.'?filter='.$filter.'&start='.$startDate.'&end='.$endDate)}}">Next</a></li>
                                        @else
                                            <li class="page-item"><a class="page-link" href="{{url('cards/page/'.$previous.'?filter='.$filter.'&start='.$startDate.'&end='.$endDate)}}">Previous</a></li>
                                            @for($i=8; $i <= 10; $i++)
                                                <li class="page-item {{$i == $currentPage?'active':''}}">
                                                    <a class="page-link" href="{{url('cards/page/'.$i.'?filter='.$filter.'&start='.$startDate.'&end='.$endDate)}}">{{ $i }}</a>
                                                </li>
                                            @endfor

                                            <li class="page-item"><span class="page-link">...</span></li>
                                            @for($i=$totalPage-10; $i < $totalPage; $i++)
                                                <li class="page-item {{$i == $currentPage?'active':''}}">
                                                    <a class="page-link" href="{{url('cards/page/'.$i.'?filter='.$filter.'&start='.$startDate.'&end='.$endDate)}}">{{ $i }}</a>
                                                </li>
                                            @endfor
                                            <li class="page-item"><a class="page-link" href="{{url('cards/page/'.$next.'?filter='.$filter.'&start='.$startDate.'&end='.$endDate)}}">Next</a></li>
                                        @endif
                                    @endif
                                </ul>
                            </nav>
                        </div>
                    </div>
            @endif
            <!-- end pagination -->
            </div>
        </div>
    </div>
    <!-- END DASHBOARD CONTENT -->
    <div class="ks-scrollable"></div>
@endsection

@section('footer')

@endsection
