@extends('layouts.app')
@section('title', 'New Card')

@section('header')

   <!-- BEGIN DATETIME PICKER -->
   <script type="text/javascript" src="{{url('assets/scripts/jquery.min.js')}}"></script>
   <script type="text/javascript" src="{{url('assets/scripts/datetimepicker/jquery.timepicker.js')}}"></script>
   <link rel="stylesheet" type="text/css" href="{{url('assets/styles/datetimepicker/jquery.timepicker.css')}}" />
   <script type="text/javascript" src="{{url('assets/scripts/datetimepicker/bootstrap-datepicker.js')}}"></script>
   <link rel="stylesheet" type="text/css" href="{{url('assets/styles/datetimepicker/bootstrap-datepicker.css')}}" />
   <!-- END DATETIME PICKER -->

   <!-- Phone Formating -->
   <script type="text/javascript" src="{{url('assets/scripts/bootstrap-formhelpers-phone.js')}}"></script>

@endsection

@section('content')
<!-- BEGIN DASHBOARD HEADER -->
<div class="ks-header">
   <section class="ks-title">
      <h3>Create User Card</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="{{url('dashboard')}}">
               <span class="fa fa-home ks-icon"></span>
            </a>
            <span class="breadcrumb-item active">New User</span>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->

<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid">
         @if (Session::has('message'))
         <div class="alert alert-info alert-block">
         	<button type="button" class="close" data-dismiss="alert">×</button>
         	<p class="color-red">{!! Session::get('message') !!}</p>
         </div>
         @endif
         <form role="form" method="post" enctype="multipart/form-data" action="{{url('cards/insert')}}">
            <div class="form-group">
   				<input class="form-control" type="text" name="firstName" placeholder="Firstname (Required)" required>
   			</div>
            <div class="form-group">
					<input class="form-control" type="text" name="lastName" placeholder="Lastname (Required)" required>
				</div>
            <div class="form-group">
					<input class="input-medium bfh-phone form-control" type="text" name="phone" pattern=".{15,}" placeholder="phone (Required)" data-format="+855 dd ddd dddd" required>
				</div>
            <div class="form-group">
               <label class="margin-right-10">Gender</label>
               <label class="custom-control custom-radio">
                  <input id="radio1" name="gender" value="male" type="radio" class="custom-control-input" checked>
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description">Male</span>
               </label>
               <label class="custom-control custom-radio">
                  <input id="radio1" name="gender" value="female" type="radio" class="custom-control-input">
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description">Female</span>
               </label>
            </div>
            <div class="form-group">
               <p id="datepairClaim">
                  <input type="text" name="dob" class="date start datepicker-control full-width" placeholder="Date of Birth (Required)" required/>
               </p>
            </div>

            <div class="form-group">
               <label class="margin-right-10">Married</label>
               <label class="custom-control custom-radio">
                  <input id="radio1" name="married" value="1" type="radio" class="custom-control-input" checked>
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description">Yes</span>
               </label>
               <label class="custom-control custom-radio">
                  <input id="radio1" name="married" value="0" type="radio" class="custom-control-input">
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description">No</span>
               </label>
            </div>
            <div class="form-group">
					<input class="form-control" type="text" name="children" placeholder="Children (Number)">
				</div>
            <div class="form-group">
					<input class="form-control" type="text" name="address" placeholder="Address (Optional)">
				</div>
            <div class="form-group">
					<input class="form-control" type="text" name="sangkat" placeholder="Sangkat (Optional)">
				</div>
            <div class="form-group">
					<input class="form-control" type="text" name="khan" placeholder="Khan (Optional)">
				</div>
            <div class="form-group">
					<input class="form-control" type="text" name="city" placeholder="City (Optional)">
				</div>
            <div class="form-group">
					<input class="form-control" type="text" name="country" placeholder="Country (Optional)">
				</div>
            <div class="form-group">
					<input class="form-control" type="text" name="keyin" placeholder="Keyin (Optional)">
				</div>
            <div class="form-group">
					<input class="form-control" type="text" name="facebook" placeholder="Facebook (Optional)">
				</div>
            <div class="form-group">
					<input class="form-control" type="text" name="line" placeholder="Line (Optional)">
				</div>
            <div class="form-group">
					<input class="form-control" type="text" name="wechat" placeholder="Wechat (Optional)">
				</div>
            <div class="form-group">
					<input class="form-control" type="text" name="holderName" placeholder="Holder Name (Required)" required>
				</div>
            <div class="form-group">
					<input class="form-control" type="text" name="cardNumber" placeholder="Card Number (Required)" required>
				</div>
            <div class="form-group">
               <select class="form-control" name="cardType">
                  @foreach($cardtypes as $cardtype)
                     <option value="{{ $cardtype->getObjectId() }}">
                        {{ $cardtype->get('title') }}
                     </option>
                  @endforeach
               </select>
            </div>

				<div class="form-group">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
   				<button type="submit" name="save" class="btn btn-primary">Save</button>
				</div>
         </form>

         <!-- BEING DATATIME PICKER SCRIPT -->
         <script src="{{url('assets/scripts/datetimepicker/datepair.js')}}"></script>
         <script src="{{url('assets/scripts/datetimepicker/jquery.datepair.js')}}"></script>

         <script>
            $('#datepairClaim .date').datepicker({
            'format': 'm/d/yyyy',
            'autoclose': true
            });

            $('#datepairClaim').datepair();
         </script>
         <!-- BEING DATATIME PICKER SCRIPT -->

      </div>
   </div>
</div>
<!-- END DASHBOARD CONTENT -->
<div class="ks-scrollable"></div>
@endsection

@section('footer')

@endsection
