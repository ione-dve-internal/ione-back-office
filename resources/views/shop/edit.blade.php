@extends('layouts.app')
@section('title', 'Edit Shop')

@section('header')
   <!-- MAP PICKER -->
   <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
   <script type="text/javascript" src='http://maps.google.com/maps/api/js?key=AIzaSyDvYzz9Qhl2NLOQrSfYqyJboUmGog1e1Ow&sensor=false&libraries=places'></script>
   <script src="{{url('assets/mappicker/locationpicker.jquery.min.js')}}"></script>
   <!-- Phone Formating -->
   <script type="text/javascript" src="{{url('assets/scripts/bootstrap-formhelpers-phone.js')}}"></script>
@endsection

@section('content')
<!-- BEGIN DASHBOARD HEADER -->
<div class="ks-header">
   <section class="ks-title">
      <h3>Update Shop</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="{{url('dashboard')}}">
               <span class="fa fa-home ks-icon"></span>
            </a>
            <span class="breadcrumb-item active">Update Item</span>
            <a href="{{url('shop')}}" class="breadcrumb-item">Back</a>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->

<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid">
         <form role="form" method="post" enctype="multipart/form-data" action="{{url('shop/update/'.$result->getObjectId())}}">
            <div class="form-group">
   				<input class="form-control" type="text" name="code" placeholder="Code (Required)" value="{{ $result->get('code') }}" required>
   			</div>
            <div class="form-group">
					<input class="form-control" type="text" name="name" placeholder="Name (Required)" value="{{ $result->get('name') }}" required>
				</div>
            <div class="form-group">
               <input class="input-medium bfh-phone form-control" type="text" name="phone" pattern=".{15,}" placeholder="phone (Required)" data-format="+855 dd ddd dddd" value="{{ $result->get('phone')?$result->get('phone'):'' }}">
            </div>
            <div class="form-group">
					<input class="form-control" type="email" name="email" placeholder="Email (Optional)" value="{{ $result->get('email')?$result->get('email'):'' }}">
				</div>
            <div class="form-group">
					<input class="form-control" type="text" name="website" placeholder="Website (Optional)" value="{{ $result->get('website')?$result->get('website'):'' }}">
				</div>
            <div class="form-group">
					<textarea class="form-control" rows="5" id="about" name="about" placeholder="Please write about shop">{{ $result->get('about')?$result->get('about'):'' }}</textarea>
				</div>
            <div class="form-group">
					<input class="form-control" type="text" name="location" placeholder="Location (Optional) eg: 40.0, -30.0" value="{{ $result->get('location')?$result->get('location')->getLatitude().','.$result->get('location')->getLongitude():'' }}">
				</div>
            <div class="form-group">
               <select class="form-control" name="category">
                  <option value="{{ $result->get('category')->getObjectId() }}">{{ $result->get('category')->get('name') }}</option>
                  @foreach($shopcategories as $shopcategory)
                     @if($shopcategory->getObjectId() != $result->get('category')->getObjectId())
                        <option value="{{ $shopcategory->getObjectId() }}">
                           {{ $shopcategory->get('name') }}
                        </option>
                     @endif
                  @endforeach
               </select>
            </div>

            <label>Shop Location</label>
            <div class="form-group">
               <input type="text" class="form-control" id="us3-address" />
            </div>
            <div class="form-group">
               <div id="us3" style="width: 100%; height: 400px;"></div>
               <input type="hidden" class="form-control" name="latitude" id="us3-lat" />
               <input type="hidden" class="form-control" name="longitude" id="us3-lon" />
            </div>

            <script>
               $('#us3').locationpicker({
                  location: {
                     latitude: {{$result->get('location')->getLatitude()}},
                     longitude: {{$result->get('location')->getLongitude()}}
                  },
                  radius: 50,
                  inputBinding: {
                     latitudeInput: $('#us3-lat'),
                     longitudeInput: $('#us3-lon'),
                     radiusInput: $('#us3-radius'),
                     locationNameInput: $('#us3-address')
                  },
                  enableAutocomplete: true,
                  onchanged: function (currentLocation, radius, isMarkerDropped) {
                     // Uncomment line below to show alert on each Location Changed event
                     //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                  }
               });
            </script>
            <div class="form-group">
               <label class="margin-right-10">Popular</label>
                  <label class="custom-control custom-radio">
                     <input id="radio1" name="popular" value="1" type="radio" class="custom-control-input" checked>
                     <span class="custom-control-indicator"></span>
                     <span class="custom-control-description">True</span>
                  </label>
                  <label class="custom-control custom-radio">
                     <input id="radio1" name="popular" value="0" type="radio" class="custom-control-input" {{ $result->get('popular')==False?'checked':'' }} >
                     <span class="custom-control-indicator"></span>
                     <span class="custom-control-description">False</span>
                  </label>

            </div>
             <div class="form-group">
               <label class="margin-right-10">Status</label>
                  <label class="custom-control custom-radio">
                     <input id="radio1" name="status" value="1" type="radio" class="custom-control-input" checked>
                     <span class="custom-control-indicator"></span>
                     <span class="custom-control-description">True</span>
                  </label>
                  <label class="custom-control custom-radio">
                     <input id="radio1" name="status" value="0" type="radio" class="custom-control-input" {{ $result->get('status')==False?'checked':'' }} >
                     <span class="custom-control-indicator"></span>
                     <span class="custom-control-description">False</span>
                  </label>

            </div>
				<div class="form-group">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
   				<button type="submit" name="save" class="btn btn-primary">Update</button>
				</div>
         </form>
      </div>
   </div>
</div>
<!-- END DASHBOARD CONTENT -->
<div class="ks-scrollable"></div>
@endsection

@section('footer')

@endsection
