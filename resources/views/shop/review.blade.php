@extends('layouts.app')
@section('title', 'Shop Review')

@section('header')
<link rel="stylesheet" type="text/css" href="{{url('libs/datatables-net/media/css/dataTables.bootstrap4.min.css')}}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{url('assets/styles/libs/datatables-net/datatables.min.css')}}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{url('libs/select2/css/select2.min.css')}}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{url('assets/styles/libs/select2/select2.min.css')}}"> <!-- Customization -->
@endsection

@section('content')
<!-- BEGIN DASHBOARD HEADER -->

<div class="ks-header">
   <section class="ks-title">
      <h3>{{$shop->get('name')}} Review</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="index.html">
               <span class="fa fa-home ks-icon"></span>
            </a>
            <span class="breadcrumb-item active">Shop Review</span>
            <a href="{{url('shop')}}" class="breadcrumb-item">Back</a>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->

<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid">
         <table id="ks-datatable" class="table table-striped table-bordered" width="100%">
            <thead>
               <tr>
                  <th>User</th>
                  <th>Review</th>
                  <th>Review Date</th>
               </tr>
            </thead>
            <tbody>
               @foreach($results as $result)
                  <tr>
                     <td>{{$result->get('user')->getObjectId()?$result->get('user')->get('firstName').' '.$result->get('user')->get('lastName'):''}}</td>
                     <td>
                        <form class="rating">
                           <input type="radio" value="5"   {{$result->get('star') >= 5?'checked':''}} />  <label class = "full" for="star5"></label>
                           <input type="radio" value="4.5" {{$result->get('star') >= 4.5?'checked':''}} /><label class="half" for="star4half"></label>
                           <input type="radio" value="4"   {{$result->get('star') >= 4?'checked':''}} />  <label class = "full" for="star4"></label>
                           <input type="radio" value="3.5" {{$result->get('star') >= 3.5?'checked':''}} /><label class="half" for="star3half"></label>
                           <input type="radio" value="3"   {{$result->get('star') >= 3?'checked':''}} />  <label class = "full" for="star3"></label>
                           <input type="radio" value="2.5" {{$result->get('star') >= 2.5?'checked':''}} /><label class="half" for="star2half"></label>
                           <input type="radio" value="2"   {{$result->get('star') >= 2?'checked':''}} />  <label class = "full" for="star2"></label>
                           <input type="radio" value="1.5" {{$result->get('star') >= 1.5?'checked':''}}/> <label class="half" for="star1half"></label>
                           <input type="radio" value="1"   {{$result->get('star') >= 1?'checked':''}} />  <label class = "full" for="star1"></label>
                           <input type="radio" value="0.5" {{$result->get('star') >= 0.5?'checked':''}}/> <label class="half" for="starhalf"></label>
                        </form>
                     </td>
                     <td>{{ $result->getCreatedAt()->format('d-M-Y') }}</td>
                  </tr>
               @endforeach
            </tbody>
         </table>
      </div>
   </div>
</div>
<!-- END DASHBOARD CONTENT -->
<div class="ks-scrollable"></div>
@endsection

@section('footer')
<script src="{{url('libs/datatables-net/media/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('libs/datatables-net/media/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{url('libs/select2/js/select2.min.js')}}"></script>
<script type="application/javascript">
(function ($) {
    $(document).ready(function() {
        $('#ks-datatable').DataTable({
            "initComplete": function () {
                $('.dataTables_wrapper select').select2({
                    minimumResultsForSearch: Infinity
                });
            }
        });
    });
})(jQuery);
</script>
@endsection
