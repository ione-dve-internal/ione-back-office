@extends('layouts.app')
@section('title', 'New Pre-Order')

@section('header')
   <!-- BEGIN DATETIME PICKER -->
   <script type="text/javascript" src="{{url('assets/scripts/jquery.min.js')}}"></script>
   <script type="text/javascript" src="{{url('assets/scripts/datetimepicker/jquery.timepicker.js')}}"></script>
   <link rel="stylesheet" type="text/css" href="{{url('assets/styles/datetimepicker/jquery.timepicker.css')}}" />
   <script type="text/javascript" src="{{url('assets/scripts/datetimepicker/bootstrap-datepicker.js')}}"></script>
   <link rel="stylesheet" type="text/css" href="{{url('assets/styles/datetimepicker/bootstrap-datepicker.css')}}" />
   <!-- END DATETIME PICKER -->

   <!-- BEGIN CHECK IMAGE DIMENSIONS -->
   <script type="text/javascript" src="{{url('assets/scripts/checkimagedimensions.js')}}"></script>
   <!-- END CHECK IMAGE DIMENSIONS -->
@endsection

@section('content')
<!-- BEGIN DASHBOARD HEADER -->
<div class="ks-header">
   <section class="ks-title">
      <h3>Create Pre-Order</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="{{url('dashboard')}}">
               <span class="fa fa-home ks-icon"></span>
            </a>
            <span class="breadcrumb-item active" href="#">New Item</span>
            <a href="{{url('preorder')}}" class="breadcrumb-item">Back</a>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->

<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid">
         <form role="form" method="post" enctype="multipart/form-data" action="{{url('preorder/insert')}}">
   			<div class="form-group">
   				<input class="form-control" type="text" name="redeemCode" placeholder="Redeem Code (Required)" required>
   			</div>
            <div class="form-group">
					<input class="form-control" type="text" name="name" placeholder="Title (Required)" required>
				</div>
            <div class="form-group">
               <p id="datepairClaim">
                  <input type="text" name="available" class="date start datepicker-control" placeholder="Available Date (Required)" required/>
               </p>
            </div>
            <div class="form-group">
               <label class="margin-right-10">Status</label>
               <label class="custom-control custom-radio">
                  <input id="radio1" name="status" value="1" type="radio" class="custom-control-input" checked>
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description">True</span>
               </label>
               <label class="custom-control custom-radio">
                  <input id="radio1" name="status" value="0" type="radio" class="custom-control-input">
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description">False</span>
               </label>
            </div>
            <div class="form-group">
					<input type="file" name="thumb" id="file" required >
               <label class="margin-right-10">Upload Thumbnail (750px X 240px)</label>
				</div>
				<div class="form-group">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
   				<button type="submit" name="save" class="btn btn-primary">Save</button>
				</div>
         </form>

         <!-- BEING DATATIME PICKER SCRIPT -->
         <script src="{{url('assets/scripts/datetimepicker/datepair.js')}}"></script>
         <script src="{{url('assets/scripts/datetimepicker/jquery.datepair.js')}}"></script>

         <script>
            $('#datepairClaim .date').datepicker({
            'format': 'm/d/yyyy',
            'autoclose': true
            });

            $('#datepairClaim').datepair();
         </script>
         <!-- BEING DATATIME PICKER SCRIPT -->
      </div>
   </div>
</div>
<div class="ks-scrollable"></div>
<!-- END DASHBOARD CONTENT -->
@endsection

@section('footer')

@endsection
