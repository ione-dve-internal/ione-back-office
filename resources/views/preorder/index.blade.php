@extends('layouts.app')
@section('title', 'Products')

@section('header')
<link rel="stylesheet" type="text/css" href="{{url('libs/datatables-net/media/css/dataTables.bootstrap4.min.css')}}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{url('assets/styles/libs/datatables-net/datatables.min.css')}}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{url('libs/select2/css/select2.min.css')}}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{url('assets/styles/libs/select2/select2.min.css')}}"> <!-- Customization -->
@endsection

@section('content')
<!-- BEGIN DASHBOARD HEADER -->
<div class="ks-header">
   <section class="ks-title">
      <h3>Pre-Order List</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="index.html">
               <span class="fa fa-home ks-icon"></span>
            </a>
            <span class="breadcrumb-item active">Products</span>
            <a href="preorder/create" class="breadcrumb-item">New Item</a>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->

<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid">
         <table id="ks-datatable" class="table table-striped table-bordered" width="100%">
            <thead>
               <tr>
                  <th>Redeem Code</th>
                  <th>Name</th>
                  <th>Avaiable</th>
                  <th>Status</th>
                  <th>Action</th>
               </tr>
            </thead>
            <tbody>
               @foreach($results as $result)
                  <tr>
                     <td>{{ $result->get('redeemCode') }}</td>
                     <td>{{ $result->get('name') }}</td>
                     <td>{{ $result->get('available')->format('d-M-Y') }}</td>
                     <td>
                        @if($result->get('status')==1)
                           <span class="badge ks-circle badge-success">Enabled</span>
                        @else
                           <span class="badge ks-circle badge-danger">Disabled</span>
                        @endif
                     </td>
                     <td class="table-actions">
                        <div class="dropdown padding-top-10">
                           <a class="btn btn-link" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <span class="fa fa-ellipsis-h"></span>
                           </a>
                           <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                              <!--a class="dropdown-item" href="{{url('promotion/image/'.$result->getObjectId())}}">
                                 <span class="fa fa-image icon text-primary-on-hover"></span> Change image
                              </a-->
                              <a class="dropdown-item" href="{{url('preorder/edit/'.$result->getObjectId())}}">
                                 <span class="fa fa-pencil icon text-primary-on-hover"></span> Edit info
                              </a>
                              <a class="dropdown-item" href="{{url('preorder/delete/'.$result->getObjectId())}}" onclick="return confirm('Do you want to delete {{ $result->get('title') }} preorder?')";>
                                 <span class="fa fa-trash icon text-danger-on-hover"></span> Delete
                              </a>
                           </div>
                        </div>
                     </td>
                  </tr>
               @endforeach
            </tbody>
         </table>
      </div>
   </div>
</div>
<!-- END DASHBOARD CONTENT -->
<div class="ks-scrollable"></div>
@endsection

@section('footer')
<script src="{{url('libs/datatables-net/media/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('libs/datatables-net/media/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{url('libs/select2/js/select2.min.js')}}"></script>
<script type="application/javascript">
(function ($) {
    $(document).ready(function() {
        $('#ks-datatable').DataTable({
            "initComplete": function () {
                $('.dataTables_wrapper select').select2({
                    minimumResultsForSearch: Infinity
                });
            }
        });
    });
})(jQuery);
</script>
@endsection
