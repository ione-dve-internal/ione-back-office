@extends('layouts.app')
@section('title', 'Update Pre-Order')

@section('header')
   <!-- BEGIN DATETIME PICKER -->
   <script type="text/javascript" src="{{url('assets/scripts/jquery.min.js')}}"></script>
   <script type="text/javascript" src="{{url('assets/scripts/datetimepicker/jquery.timepicker.js')}}"></script>
   <link rel="stylesheet" type="text/css" href="{{url('assets/styles/datetimepicker/jquery.timepicker.css')}}" />
   <script type="text/javascript" src="{{url('assets/scripts/datetimepicker/bootstrap-datepicker.js')}}"></script>
   <link rel="stylesheet" type="text/css" href="{{url('assets/styles/datetimepicker/bootstrap-datepicker.css')}}" />
   <!-- END DATETIME PICKER -->

   <!-- BEGIN CHECK IMAGE DIMENSIONS -->
   <script type="text/javascript" src="{{url('assets/scripts/checkimagedimensions.js')}}"></script>
   <!-- END CHECK IMAGE DIMENSIONS -->

   <!-- BEGIN CK EDITOR SCRIPT -->
   <script type="text/javascript" src="{{url('assets/ckeditor/ckeditor.js')}}"></script>
   <!-- END CK EDITOR SCRIPT -->


@endsection

@section('content')
<!-- BEGIN DASHBOARD HEADER -->
<div class="ks-header">
   <section class="ks-title">
      <h3>Update Pre-Order</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="{{url('dashboard')}}">
               <span class="fa fa-home ks-icon"></span>
            </a>
            <span class="breadcrumb-item active" href="#">New Item</span>
            <a href="{{url('preorder')}}" class="breadcrumb-item">Back</a>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->

<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid">
         <form role="form" method="post" enctype="multipart/form-data" action="{{url('preorder/update/'.$preOrder->getObjectId())}}">
   			<div class="form-group">
   				<input class="form-control" type="text" name="redeemCode" placeholder="Redeem Code (Required)" value="{{ $preOrder->get('redeemCode') }}" required>
   			</div>
            <div class="form-group">
					<input class="form-control" type="text" name="name" placeholder="Name (Required)" value="{{ $preOrder->get('name') }}" required>
				</div>
            <div class="form-group">
               <p id="datepairClaim">
                  <input type="text" name="available" class="date start datepicker-control" placeholder="Available Date (Required)" value="{{ $preOrder->get('available')->format('n/d/Y') }}" required/>
               </p>
            </div>
            <div class="form-group">
               <label class="margin-right-10">Status</label>
               <label class="custom-control custom-radio">
                  <input id="radio1" name="status" value="1" type="radio" class="custom-control-input" checked>
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description">True</span>
               </label>
               <label class="custom-control custom-radio">
                  <input id="radio1" name="status" value="0" type="radio" class="custom-control-input" {{ $preOrder->get('status')==False?'checked':'' }}>
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description">False</span>
               </label>
            </div>
            <div class="form-group">
					<input type="file" name="thumb" id="thumb" >
               <label class="margin-right-10">Upload Thumbnail (750px X 240px)</label>
               <div class="row">
                  <div class="col-lg-6">
                     <img src="{{ $preOrder->get('thumb')!=NULL?$preOrder->get('thumb')->getUrl():'' }}" style="width:100%">
                  </div>
               </div>
				</div>

				<div class="form-group">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
   				<button type="submit" name="save" class="btn btn-primary">Update</button>
				</div>
         </form>

         <div class="row">
            <div class="col-lg-12" style="overflow-x: auto">
               <table class="table table-bordered" style="width:auto">
                  <tbody>
                     <tr>
                        @foreach($preOrderDetails as $preOrderDetail)
                        <td width="500">
                           <!-- main form of preorder detail -->
                           <form id="{!! $preOrderDetail->getObjectId() !!}" name="mainForm" role="form" method="post" enctype="multipart/form-data" action="{{url('preorderdetail/update/'.$preOrderDetail->getObjectId())}}">
                              <input type="text" class="preorder-control" name="productName" placeholder="Product Name (Required)" value="{{$preOrderDetail->get('productName')}}" required>
                              <input type="hidden" name="preorderId" value="{{$preOrder->getObjectId()}}">
                              <label>Specification</label>
                              <textarea class="form-control" id="{!! $preOrderDetail->get('productName') !!}" name="spec">
                              	{!! $preOrderDetail->get('spec') !!}
                              </textarea>
                              <input type="hidden" name="_token" value="{{csrf_token()}}">
                           </form>

                           <!-- get all product price by preOrderDetailId-->
                           <div class="margin-top-15">Product Price</div>
                           @foreach($productPrices as $productPrice)
                              @if($productPrice->get('detail')->getObjectId() == $preOrderDetail->getObjectId())
                                 <form class="form-inline margin-top-10" method="post" action="{{url('productprice/update/'.$productPrice->getObjectId())}}">
                                    <div class="float-left">
                                       <a href="{{url('productprice/delete/'.$productPrice->getObjectId().'/'.$preOrderDetail->getObjectId())}}" onclick="return confirm('Do you want to delete {!! $productPrice->get('capacity') !!}?')";>
                                          <span class="fa fa-trash-o margin-right-10"></span>
                                       </a>
                                    </div>
                                    <div class="form-group">
                                       <input type="text" class="form-control" name="capacity" placeholder="Capacity (Required)" value="{!! $productPrice->get('capacity') !!}" required>
                                    </div>
                                    <div class="form-group">
                                       <input type="number" class="form-control" name="price" placeholder="Price (Required)" value="{!! $productPrice->get('price') !!}" required>
                                    </div>
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <button type="submit" class="btn btn-default">
                                       <span class="fa fa-save"></span>
                                    </button>
                                 </form>
                              @endif
                           @endforeach

                           <!-- create new product price-->
                           <form id="{{'price'.$preOrderDetail->getObjectId()}}" class="form-inline margin-top-15 margin-left-20" method="post" action="{{url('productprice/insert')}}">
                              <div class="form-group">
                                 <input type="text" class="form-control" name="capacity" placeholder="Capacity (Required)" required>
                              </div>
                              <div class="form-group">
                                 <input type="number" class="form-control" name="price" placeholder="Price (Required)" required>
                              </div>
                              <input type="hidden" name="detailId" value="{{$preOrderDetail->getObjectId()}}">
                              <input type="hidden" name="preorderId" value="{{$preOrder->getObjectId()}}">
                              <input type="hidden" name="_token" value="{{csrf_token()}}">
                              <button type="submit" class="btn btn-default">
                                 <span class="fa fa-save"></span>
                              </button>
                           </form>
                           <button id="{{'addPrice'.$preOrderDetail->getObjectId()}}" class="margin-top-15 margin-left-20">+</button>

                           <div class="margin-top-15">Product Appearence</div>
                           @foreach($productAppearences as $productAppearence)
                              @if($productAppearence->get('detail')->getObjectId() == $preOrderDetail->getObjectId())
                                 <form class="form-inline margin-top-15" method="post" enctype="multipart/form-data" action="{{url('productappearence/update/'.$productAppearence->getObjectId())}}">
                                    <div class="float-left">
                                       <a href="{{url('productappearence/delete/'.$productAppearence->getObjectId())}}" onclick="return confirm('Do you want to delete {!! $productAppearence->get('title') !!}?')";>
                                          <span class="fa fa-trash-o margin-right-10"></span>
                                       </a>
                                    </div>
                                    <div class="form-group">
                                       <input type="text" class="form-control" name="title" placeholder="Color (Required)" value="{{$productAppearence->get('title')}}" required>
                                    </div>
                                    <div>
                                       <input type="file" class="file-control" name="photo">
                                    </div>
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <button type="submit" class="btn btn-default">
                                       <span class="fa fa-save"></span>
                                    </button>
                                 </form>
                              @endif
                           @endforeach

                           <!-- create new product appearence-->
                           <form id="{{'appearance'.$preOrderDetail->getObjectId()}}" class="form-inline margin-top-15 margin-left-20" method="post" enctype="multipart/form-data" action="{{url('productappearence/insert')}}">
                              <div class="form-group">
                                 <input type="text" class="form-control" name="title" placeholder="Color (Required)" required>
                              </div>
                              <div>
                                 <input type="file" class="file-control" name="photo" required>
                              </div>
                              <input type="hidden" name="detailId" value="{{$preOrderDetail->getObjectId()}}">
                              <input type="hidden" name="preOrderId" value="{{$preOrder->getObjectId()}}">
                              <input type="hidden" name="_token" value="{{csrf_token()}}">
                              <button type="submit" class="btn btn-default">
                                 <span class="fa fa-save"></span>
                              </button>
                           </form>
                           <button id="{{'addAppearence'.$preOrderDetail->getObjectId()}}" class="margin-top-15 margin-left-20">+</button>

                           <!--main form buttons-->
                           <div class="row">
                              <div class="col-lg-12">
                                 <div class="float-left">
                        				<button type="submit" name="save" form="{!! $preOrderDetail->getObjectId() !!}" class="btn btn-primary margin-top-15">Update</button>
                                 </div>
                                 <div class="float-right">
                                    <a href="{{url('preorderdetail/delete/'.$preOrderDetail->getObjectId())}}" class="btn btn-danger margin-top-15" role="button" onclick="return confirm('Do you want to delete {{ $preOrderDetail->get('productName') }}?')";>
                                       Delete
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </td>
                        @endforeach

                        <td width="500">
                           <form role="form" id="newDetail" method="post" enctype="multipart/form-data" action="{{url('preorderdetail/insert')}}">
                              <input type="text" class="preorder-control" name="productName" placeholder="Product Name (Required)" required>
                              <input type="hidden" name="preorderId" value="{{$preOrder->getObjectId()}}">
                              <label>Specification</label>
                              <textarea id="newTextArea" name="spec"></textarea>
                              <input type="hidden" name="_token" value="{{csrf_token()}}">
                  				<button type="submit" name="save1" class="btn btn-primary margin-top-15">Save</button>
                           </form>
                        </td>
                     </tr>
                  <tbody>
               </table>
            </div>
         </div>

         <!-- BEING DATATIME PICKER SCRIPT -->
         <script src="{{url('assets/scripts/datetimepicker/datepair.js')}}"></script>
         <script src="{{url('assets/scripts/datetimepicker/jquery.datepair.js')}}"></script>

         <script>
            $('#datepairClaim .date').datepicker({
               'format': 'm/d/yyyy',
               'autoclose': true
            });

            $('#datepairClaim').datepair();
         </script>
         <!-- BEING DATATIME PICKER SCRIPT -->
      </div>
   </div>
</div>

<div class="modal" style="display: none; background-color:#fff; opacity:0.8; margin: auto;" >
    <div style="margin: auto; position: absolute;top: 50%; left: 50%;">
        <img alt="" src="{{url('assets/img/loaders/page-loader-ring-lg-primary.gif')}}">
    </div>
</div>
<!-- END DASHBOARD CONTENT -->
<div class="ks-scrollable"></div>
@endsection

@section('footer')
   @foreach($preOrderDetails as $preOrderDetail)
   <script>
      CKEDITOR.replace( '{!! $preOrderDetail->get('productName') !!}' );

      $(document).ready(function(){
         $("#{{ 'price'.$preOrderDetail->getObjectId() }}").hide();
         $("#{{ 'appearance'.$preOrderDetail->getObjectId() }}").hide();

         $("#{{ 'addPrice'.$preOrderDetail->getObjectId() }}").click(function(){
            $("#{{ 'price'.$preOrderDetail->getObjectId() }}").show();
            $("#{{ 'addPrice'.$preOrderDetail->getObjectId() }}").hide();
         });

         $("#{{ 'addAppearence'.$preOrderDetail->getObjectId() }}").click(function(){
            $("#{{ 'appearance'.$preOrderDetail->getObjectId() }}").show();
            $("#{{ 'addAppearence'.$preOrderDetail->getObjectId() }}").hide();
         });
      });

      //update preorderdetail with ajax
      $(function () {
         $('#{!! $preOrderDetail->getObjectId() !!}').on('submit', function (e) {
            $(".modal").show();
            e.preventDefault();

            // Get all form values
            var values = {};
            var fields = {};

            for(var instanceName in CKEDITOR.instances){
               CKEDITOR.instances[instanceName].updateElement();
            }

            $.each($("#{!! $preOrderDetail->getObjectId() !!}").serializeArray(), function(i, field){
               values[field.name] = field.value;
            });

            $.ajax({
               type: 'post',
               url: "{{url('preorderdetail/update/'.$preOrderDetail->getObjectId())}}",
               data: values,
               success: function (response) {
                  $(".modal").hide();
                  // you will get response from your php page (what you echo or print)
                  console.log(response);
                  //alert('success');
               },
               error: function(jqXHR, textStatus, errorThrown) {
                  $(".modal").hide();
                  alert('fail');
                  console.log(textStatus, errorThrown);
               }
            });
         });
      });
	</script>
   @endforeach
   <script>
		CKEDITOR.replace( 'newTextArea' );
	</script>

@endsection
