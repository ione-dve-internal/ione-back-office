@extends('layouts.app')
@section('title', 'Change Password')

@section('header')
   <!-- BEGIN CHECK IMAGE DIMENSIONS -->
   <script type="text/javascript" src="{{url('assets/scripts/checkimagedimensions.js')}}"></script>
   <!-- END CHECK IMAGE DIMENSIONS -->
@endsection

@section('content')
<!-- BEGIN DASHBOARD HEADER -->
<div class="ks-header">
   <section class="ks-title">
      <h3>Change Password</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="{{url('dashboard')}}">
               <span class="fa fa-home ks-icon"></span>
            </a>
            <span class="breadcrumb-item active">Change Password</span>
            <a href="{{url('user')}}" class="breadcrumb-item">Back</a>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->

<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid">
         <form role="form" method="post" enctype="multipart/form-data" action="{{url('user/updatepassword/'.$result->getObjectId())}}">
            @if(Session::has('message'))
            <div class="form-group text-success">
               {!! Session::get('message') !!}
            </div>
            @endif
            <div class="form-group">
					<input class="form-control" pattern=".{8,}" name="password" type="password" placeholder="New Password (minimum is 8 characters)" required>
				</div>

				<div class="form-group">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
   				<button type="submit" name="save" class="btn btn-primary">Change Password</button>
				</div>
         </form>
      </div>
   </div>
</div>
<!-- END DASHBOARD CONTENT -->
<div class="ks-scrollable"></div>
@endsection

@section('footer')

@endsection
