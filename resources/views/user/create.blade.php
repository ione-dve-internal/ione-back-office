@extends('layouts.app')
@section('title', 'New User')

@section('header')
   <!-- BEGIN CHECK IMAGE DIMENSIONS -->
   <script type="text/javascript" src="{{url('assets/scripts/checkimagedimensions.js')}}"></script>
   <!-- END CHECK IMAGE DIMENSIONS -->
@endsection

@section('content')
<!-- BEGIN DASHBOARD HEADER -->
<div class="ks-header">
   <section class="ks-title">
      <h3>Create User</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="{{url('dashboard')}}">
               <span class="fa fa-home ks-icon"></span>
            </a>
            <span class="breadcrumb-item active">New User</span>
            <a href="{{url('user')}}" class="breadcrumb-item">Back</a>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->

<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid">
         <form role="form" method="post" enctype="multipart/form-data" action="{{url('user/insert')}}">
   			<div class="form-group">
   				<input class="form-control" type="text" name="firstName" placeholder="Firstname (Required)" required>
   			</div>
            <div class="form-group">
					<input class="form-control" type="text" name="lastName" placeholder="Lastname (Required)" required>
				</div>
            <div class="form-group">
					<input class="form-control" type="phone" name="phone" placeholder="phone (Optional)">
				</div>
            <div class="form-group">
					<input class="form-control" type="text" name="username" placeholder="Username (Required)" required>
				</div>
            <div class="form-group">
					<input class="form-control" pattern=".{8,}" name="password" type="password" placeholder="Password (minimum is 8 characters)" required>
				</div>
            <div class="form-group">
               <label class="margin-right-10">Gender</label>
               <label class="custom-control custom-radio">
                  <input id="radio1" name="gender" value="male" type="radio" class="custom-control-input" checked>
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description">Male</span>
               </label>
               <label class="custom-control custom-radio">
                  <input id="radio1" name="gender" value="female" type="radio" class="custom-control-input">
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description">Female</span>
               </label>
            </div>
				<div class="form-group">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
   				<button type="submit" name="save" class="btn btn-primary">Save</button>
				</div>
         </form>
      </div>
   </div>
</div>
<!-- END DASHBOARD CONTENT -->
<div class="ks-scrollable"></div>
@endsection

@section('footer')

@endsection
