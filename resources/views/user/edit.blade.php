@extends('layouts.app')
@section('title', 'Update User')

@section('header')

@endsection

@section('content')
<!-- BEGIN DASHBOARD HEADER -->
<div class="ks-header">
   <section class="ks-title">
      <h3>Update User</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="{{url('dashboard')}}">
               <span class="fa fa-home ks-icon"></span>
            </a>
            <span class="breadcrumb-item active">Update User</span>
            <a href="{{url('user')}}" class="breadcrumb-item">Back</a>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->

<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid">
         <form role="form" method="post" enctype="multipart/form-data" action="{{url('user/update/'.$result->getObjectId())}}">
   			<div class="form-group">
   				<input class="form-control" type="text" name="firstName" placeholder="Firstname (Required)" value="{{ $result->get('firstName') }}" required>
   			</div>
            <div class="form-group">
					<input class="form-control" type="text" name="lastName" placeholder="Lastname (Required)" value="{{ $result->get('lastName') }}" required>
				</div>
            <div class="form-group">
					<input class="form-control" type="phone" name="phone" placeholder="phone (Optional)" value="{{ $result->get('phone') }}">
				</div>
            <div class="form-group">
					<input class="form-control" type="text" name="username" placeholder="Username (Required)" value="{{ $result->get('username') }}" required>
				</div>
            <div class="form-group">
               <label class="margin-right-10">Gender</label>
               <label class="custom-control custom-radio">
                  <input id="radio1" name="gender" value="male" type="radio" class="custom-control-input" checked>
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description">Male</span>
               </label>
               <label class="custom-control custom-radio">
                  <input id="radio1" name="gender" value="female" type="radio" class="custom-control-input" {{ $result->get('gender')=='female'?'checked':'' }}>
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description">Female</span>
               </label>
            </div>

            <div class="user-role-box">
               <table class="table table-striped">
                  <thead>
                     <tr>
                        <th>User Permissions</th>
                        <!--th>Read Only</th-->
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($userroles as $userrole)
                     <tr>
                        <td>
                           <input type="checkbox" name="menus[]" value="{{$userrole->get('menu')->getObjectId()}}" checked>
                           &nbsp;{{$userrole->get('menu')->get('name')}}
                        </td>
                        <!--td><input type="checkbox" name="readOnly[]" value="1" {{$userrole->get('readOnly')==True?'checked':''}}></td-->
                     </tr>
                     @endforeach
                     @foreach($menusNotAssiged as $menuNotAssiged)
                     <tr>
                        <td>
                           <input type="checkbox" name="menus[]" value="{{$menuNotAssiged->getObjectId()}}">
                           &nbsp;{{$menuNotAssiged->get('name')}}
                        </td>
                        <!--td><input type="checkbox" name="readOnly[]" value="$menuNotAssiged->getObjectId()"></td-->
                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
				<div class="form-group text-right">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
   				<button type="submit" name="save" class="btn btn-primary">Update</button>
				</div>
         </form>
      </div>
   </div>
</div>
<!-- END DASHBOARD CONTENT -->
<div class="ks-scrollable"></div>
@endsection

@section('footer')

@endsection
