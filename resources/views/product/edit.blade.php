@extends('layouts.app')
@section('title', 'Edit Product')

@section('header')

   <!--AUTOCOMPLETE SEARCH-->
   <link rel="stylesheet" type="text/css" href="{{url('css/autocomplete.css')}}">
   <!--End AUTOCOMPLETE SEARCH-->

@endsection

@section('content')
<!-- BEGIN DASHBOARD HEADER -->
<div class="ks-header">
   <section class="ks-title">
      <h3>Update Product</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="{{url('dashboard')}}">
               <span class="fa fa-home ks-icon"></span>
            </a>
            <span class="breadcrumb-item active">Update Item</span>
            <a href="{{url('product-management')}}" class="breadcrumb-item">Back</a>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->

<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid">
         <form role="form" method="post" enctype="multipart/form-data" action="{{url('product-management/update/'.$result->getObjectId())}}">
            <div class="form-group">
               <select class="form-control" name="category">
                  <option value="{{ $result->get('category')->getObjectId() }}">{{ $result->get('category')->get('title') }}</option>
                  @foreach($productcategories as $productcategory)
                     @if($productcategory->getObjectId() != $result->get('category')->getObjectId())
                        <option value="{{ $productcategory->getObjectId() }}">
                           {{ $productcategory->get('title') }}
                        </option>
                     @endif
                  @endforeach
               </select>
            </div>
            <div class="form-group">
               <input class="form-control" type="text" name="code" placeholder="Code (Required)" value="{{ $result->get('code') }}" style="background-color:transparent " required>
            </div>
            <div class="form-group">
               <input class="form-control" type="text" name="name" placeholder="Name (Required)" value="{{ $result->get('name') }}" style="background-color:transparent " required>
            </div>
            <div class="form-group">
               <input class="form-control" type="text" name="price" placeholder="Price (Required)" value="{{ $result->get('price') }}" style="background-color:transparent "  required>
            </div>
            <div class="form-group">
               <textarea class="form-control" type="text" name="desc" placeholder="Description (Required)"  style="background-color:transparent " required> value="{{ $result->get('desc') }}" </textarea>
            </div>
              {{-- EDIT RELATED --}}
               <div class="form-group">
                  <div>
                     <input type="hidden" name="productId" id="proId" class="form-control" style="background-color:transparent " >
                  </div>
                  <div class="autocomplete" style="width: 100%">

                      <input type="hidden" id="proId" style="background-color: transparent;" class="form-control">
                      <input type="hidden" name="related" id="resultId" style="background-color: transparent;" class="form-control">

                      <div class="row">
                          <div class="col-md-6">
                              <input id="myInput" type="text" class="form-control" placeholder="Search product" autocomplete="off"  style="background-color: transparent;" >
                          </div>
                          <div class="col-md-1">
                              <input type="button" id="btnAdd" onclick="relatedFunction()" value="Add Related" style="border: 1px solid  #b1b7ba;"  class="form-control btn-default">
                          </div>
                          <div class="col-md-5">
                              <input type="text" id="relatedResult" style="border: 1px solid #b1b7ba;" class="form-control" autocomplete="off">
                          </div>
                      </div>

                      <script>
                         var arr = [];
                         document.getElementById("relatedResult").value=arr;

                         var arrId=[];
                         document.getElementById("resultId").value=arrId;

                         function relatedFunction() {
                             var x = document.getElementById("myInput").value;
                             arr.push(x);
                             arr.toString();
                             document.getElementById("relatedResult").value=arr;

                             //result of array product id
                             var result =document.getElementById("proId").value;
                             arrId.push(result);
                             arrId.toString();
                             document.getElementById("resultId").value=arrId;
                         }
                      </script>

                   </div>
                </div>
                {{-- END EDIT SIZE --}}
                 <div class="row">
                     <div class="col-md-6">
                         <input class="form-control" type="text" autocomplete="off" id="size" placeholder="Size (Optional)"  value="{{implode(', ', (array)$result->get('size'))}}" style="background-color:transparent ">
                         <input type="button" class="btn btn-danger" onclick="deleteSize()" value="Set No Size">

                     </div>
                     <div class="col-md-1">
                         <input type="button" onclick="mysizeFunction()" value="Add Size" class="form-control btn-default">
                     </div>
                     <div class="col-md-5">
                         <input type="text" name="size" id="result" class="form-control" autocomplete="off">
                     </div>
                 </div>

                 <script>
                    var arrsize = [];
                    document.getElementById("result").value=arrsize;
                    function mysizeFunction() {
                        var x = document.getElementById("size").value;
                        arrsize.push(x);
                        arrsize.toString();
                        document.getElementById("result").value=arrsize;
                    }
                    //EMPTY SIZE
                    function deleteSize(){
                        var inputColor=document.getElementById("size");
                        var textColor=document.getElementById("result");
                        inputColor.value="";
                        textColor.value="No Size";
                    }

                 </script>

                 <div class="row">
                     <div class="col-md-6">
                         <input class="form-control" type="text" autocomplete="off" id="color" placeholder="Color (Optional)"  value="{{implode(', ', (array)$result->get('color'))}}" style="background-color:transparent"  >
                         <input type="button" class="btn btn-danger" onclick="deleteColor()" value="Set No Color">
                     </div>
                     <div class="col-md-1">
                         <input type="button" onclick="myFunction()" value="Add Color" class="form-control btn-default">
                     </div>
                     <div class="col-md-5">
                         <input type="text" name="color" id="rcolor" class="form-control" autocomplete="off">
                     </div>
                 </div>

                 <script>
                    var arrcolor = [];
                    document.getElementById("rcolor").value=arrcolor;
                    function myFunction() {
                        var x = document.getElementById("color").value;
                        arrcolor.push(x);
                        document.getElementById("rcolor").value=arrcolor;
                    }

                    //EMPTY COLOR
                    function deleteColor(){
                        var inputColor=document.getElementById("color");
                        var textColor=document.getElementById("rcolor");
                        inputColor.value="";
                        textColor.value="No Color";
                    }

                 </script>
                    {{-- END INPUT MULTIPLE SIZE & COLOR --}}

                    {{-- EDIT STATUS --}}
                 <div class="form-group">
                       <label class="margin-right-10">Hot</label>
                       <label class="custom-control custom-radio">
                          <input id="radio1" name="hot" value="1" type="radio" class="custom-control-input" checked>
                          <span class="custom-control-indicator"></span>
                          <span class="custom-control-description">True</span>
                       </label>
                       <label class="custom-control custom-radio">
                          <input id="radio1" name="hot" value="0" type="radio" class="custom-control-input">
                          <span class="custom-control-indicator"></span>
                          <span class="custom-control-description">False</span>
                       </label>
                 </div>
                 <div class="form-group">
                       <label class="margin-right-10">Recommend</label>
                       <label class="custom-control custom-radio">
                          <input id="radio1" name="recommend" value="1" type="radio" class="custom-control-input" checked>
                          <span class="custom-control-indicator"></span>
                          <span class="custom-control-description">True</span>
                       </label>
                       <label class="custom-control custom-radio">
                          <input id="radio1" name="recommend" value="0" type="radio" class="custom-control-input">
                          <span class="custom-control-indicator"></span>
                          <span class="custom-control-description">False</span>
                       </label>
                 </div>
                 <div class="form-group">
                       <label class="margin-right-10">Status</label>
                       <label class="custom-control custom-radio">
                          <input id="radio1" name="status" value="1" type="radio" class="custom-control-input" checked>
                          <span class="custom-control-indicator"></span>
                          <span class="custom-control-description">Enable</span>
                       </label>
                       <label class="custom-control custom-radio">
                          <input id="radio1" name="status" value="0" type="radio" class="custom-control-input">
                          <span class="custom-control-indicator"></span>
                          <span class="custom-control-description">Disable</span>
                       </label>
                 </div>
                 <div class="form-group">
                       <input type="hidden" name="_token" value="{{csrf_token()}}">
                       <button type="submit" name="save" class="btn btn-primary">Save</button>
                 </div>
                {{-- END EDIT STATUS --}}
         </form>
      </div>
   </div>
</div>

{{-- SCRIPT EDIT RELATED --}}
<script type="text/javascript">
    function autocomplete(search, arr)
    {

        var currentFocus;

        /*execute a function when someone writes in the text field:*/
        search.addEventListener("input"  , function(e)
        {

            var a, b, i, val=this.value;
            closeAllLists();
            if(!val){return false;}
            currentFocus=-1;
            a=document.createElement("DIV");
            a.setAttribute("id", this.id + "autocomplete-list");
            a.setAttribute("class", "autocomplete-items");
            this.parentNode.appendChild(a);

            for (i = 0; i < arr.length; i++){
                if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()){
                    b = document.createElement("DIV");

                    b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                    b.innerHTML += arr[i].substr(val.length);
                    b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";

                    b.addEventListener("click", function (e) {
                        search.value = this.getElementsByTagName("input")[0].value;
                        closeAllLists();
                    });
                    a.appendChild(b);
                }
            }

        });

        /*execute a function presses a key on the keyboard:*/
        search.addEventListener("keydown", function(e)
        {

            var x = document.getElementById(this.id + "autocomplete-list");
            if (x) x = x.getElementsByTagName("div");

            if (e.keyCode == 40){
                currentFocus++;
                addActive(x);
            }else if (e.keyCode == 38){
                currentFocus--;
                addActive(x);
            }else if (e.keyCode == 13){
                e.preventDefault();
                if (currentFocus > -1){
                    if (x) x[currentFocus].click();
                }
            }

        });

        function addActive(x)
        {

            if (!x) return false;
            removeActive(x);

            if (currentFocus >= x.length) currentFocus = 0;
            if (currentFocus < 0) currentFocus = (x.length - 1);

            x[currentFocus].classList.add("autocomplete-active");

        }

        function removeActive(x)
        {

            for (var i = 0; i < x.length; i++) {
                x[i].classList.remove("autocomplete-active");
            }

        }

        function closeAllLists(elmnt)
        {

            var x = document.getElementsByClassName("autocomplete-items");
            for (var i = 0; i < x.length; i++) {
                if (elmnt != x[i] && elmnt != search) {
                    x[i].parentNode.removeChild(x[i]);
                }
            }

        }


        /*execute a function when someone clicks in the document:*/
        document.addEventListener("click", function (e)
        {

            closeAllLists(e.target);

            var inputProName = document.getElementById("myInput");
            var inputProId = document.getElementById("proId");
            var valueProName = inputProName.value;
            var index = productNames.indexOf(valueProName);
            if (index === -1) {
                inputProName.value = "";
            }else{
                inputProId.value = productIds[index];
            }

        });

    }

    var productName = ' <?php
        echo $productNames;
        ?>
        ';
    var productNames = productName.split(',');
    var productId = ' <?php
        echo $productIds;
        ?>
        ';
    var productIds = productId.split(',');

    autocomplete(document.getElementById("myInput"), productNames);

</script>
{{-- END SCRIPT RELATED --}}

<!-- END DASHBOARD CONTENT -->
<div class="ks-scrollable"></div>
@endsection

@section('footer')

@endsection
