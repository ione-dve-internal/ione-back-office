@extends('layouts.app')
@section('title', 'Product Images')

@section('header')

@endsection

@section('content')

   <!-- BEGIN DASHBOARD HEADER -->
   <div class="ks-header">
      <section class="ks-title">
         <h3>Add Product Images</h3>
         <div class="ks-controls">
            <nav class="breadcrumb ks-default">
               <a class="breadcrumb-item ks-breadcrumb-icon" href="index.html">
                  <span class="fa fa-home ks-icon"></span>
               </a>
               <span class="breadcrumb-item active">Images</span>
               <a href="{{url('product-management')}}" class="breadcrumb-item">back</a>
            </nav>
         </div>
      </section>
   </div>
   <!-- END DASHBOARD HEADER -->

   <div class="ks-content">
      <div class="ks-body">
         <div class="container-fluid">
            <!-- SHOW MAIN PRODUCT -->
               <div class="form-group">
                  <h4>Product</h4>
                  <table id="ks-datatable">
                     <tbody>
                         <tr style="font-size: 15px;">
                            <td style="padding:0px 0px 15px 0px;"> Name :</td>
                            <td style="padding:0px 0px 15px 50px;">{{$result->get('name')}}</td>
                         </tr>
                         <tr style="font-size: 15px;">
                            <td style="padding:0px 0px 15px 0px;"> Code :</td>
                            <td style="padding:0px 0px 15px 50px;">{{$result->get('code')}}</td>
                         </tr>
                     </tbody>
                  </table>
               </div>
               <hr>
            {{-- END MAIN PRODUCT --}}

            <!-- CHOOSE AND UPLOAD IMAGE -->
            <div class="col-lg-6 ">
               <h4>Upload Product Photo</h4>
               <div class="card panel panel-default ks-information ks-light" >
                  <h5 class="card-header">
                     <form role="form" method="post" enctype="multipart/form-data" action="{{url('product-management/uploadphoto/'  . $imageId)}}">
                        <input type="file" name="photo" required >
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button type="submit" class="btn btn-primary-outline ks-light ks-solid">Upload</button>
                     </form>
                  </h5>
                  {{-- SHOW IMG --}}
                  <div class="card-block text-center">
                     @foreach( $results as $result)
                        <img  src="{{ $result->get('image')!=NULL?$result->get('image')->getUrl():'' }}" style="width:50%;">
                        <a class="dropdown-item" href="{{url('product-management/deleteimage/'.$result->getObjectId())}}" onclick="return confirm('Do you want to delete {{ $result->get('name') }}?')">
                           <span class="btn btn-danger">Delete</span>
                        </a>
                     @endforeach
                  </div>
                  {{-- END SHOW IMG --}}
               </div>
            </div>
            {{-- END CHOOSE AND UPLOAD IMAGE --}}
         </div>
      </div>
   </div>
   <!-- END DASHBOARD CONTENT -->
   <div class="ks-scrollable"></div>
@endsection

@section('footer')

@endsection
