@extends('layouts.app')
@section('title', 'Add Related')

@section('header')
    <!--AUTOCOMPLETE SEARCH-->
    <link rel="stylesheet" type="text/css" href="{{url('css/autocomplete.css')}}">
    <!--End AUTOCOMPLETE SEARCH-->
@endsection

@section('content')
    <!-- BEGIN DASHBOARD HEADER -->
    <div class="ks-header">
        <section class="ks-title">
            <h3>Add Related Products</h3>
            <div class="ks-controls">
                <nav class="breadcrumb ks-default">
                    <a class="breadcrumb-item ks-breadcrumb-icon" href="index.html">
                        <span class="fa fa-home ks-icon"></span>
                    </a>
                    <span class="breadcrumb-item active">Images</span>
                    <a href="{{url('product-management')}}" class="breadcrumb-item">back</a>
                </nav>
            </div>
        </section>
    </div>
    <div class="ks-content">
        <div class="ks-body">
            <div class="container-fluid">
                <div class="form-group">
                    <h4>Main Product</h4>
                    <table id="ks-datatable">
                        <tbody>
                        <tr style="font-size: 15px;">
                            <td style="padding:0px 0px 15px 0px;">Product Name :</td>
                            <td style="padding:0px 0px 15px 50px;">{{$product->get('name')}}</td>
                        </tr>
                        <tr style="font-size: 15px;">
                            <td style="padding:0px 0px 15px 0px;">Product Name :</td>
                            <td style="padding:0px 0px 15px 50px;">{{$product->get('code')}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <hr>
                {{-- AUTOCOMPLETE --}}
                <form role="form" method="post" enctype="multipart/form-data" action="{{url('product-management/saveRelated/'.$productId)}}">
                    <label>Related Product</label>
                    <div class="form-group">
                        <div>
                            <input type="hidden" name="productId" id="proId" class="form-control">
                        </div>
                        <div class="form-group">
                            <div>
                                <input type="hidden" name="productId" id="proId" class="form-control" style="background-color:transparent " >
                            </div>
                            <div class="autocomplete" style="width: 100%">

                                <input type="hidden" id="proId" style="background-color: transparent;" class="form-control">
                                <input type="hidden" name="related" id="resultId" style="background-color: transparent;" class="form-control">

                                <div class="row">
                                    <div class="col-md-6">
                                        <input id="myInput" type="text" class="form-control" placeholder="Search product" autocomplete="off" style="background-color: transparent;">
                                    </div>
                                    <div class="col-md-1">
                                        <input type="button" id="btnAdd" onclick="relatedFunction()" value="Add" style="border: 1px solid  #b1b7ba;"  class="form-control btn-default">
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text"  id="relatedResult" style="border: 1px solid #b1b7ba;" class="form-control" autocomplete="off">
                                       
                                    </div>
                                </div>

                                <script>

                                    var relatedId = '<? if(isset($relatedProductIds)) echo $relatedProductIds ?>';
                                    var relatedIds = relatedId.split(',');

                                    var arr = [];
                                    document.getElementById("relatedResult").value=arr;

                                    var arrId=[];
                                    arrId=relatedIds;

                                    document.getElementById("resultId").value=arrId;

                                    function relatedFunction() {
                                        var x = document.getElementById("myInput").value;
                                        arr.push(x);
                                        arr.toString();
                                        document.getElementById("relatedResult").value=arr;

                                        //result of array product id
                                        var result =document.getElementById("proId").value;
                                        arrId.push(result);
                                        arrId.toString();
                                        document.getElementById("resultId").value=arrId;
                                    }
                                </script>

                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button type="submit" class="btn btn-primary">Add Related</button>
                    </div>
                </form>
             </div>
        </div>
    </div>
    <!-- END DASHBOARD CONTENT -->

    <script type="text/javascript">
        function autocomplete(search, arr)
        {

            var currentFocus;

            /*execute a function when someone writes in the text field:*/
            search.addEventListener("input"  , function(e)
            {

                var a, b, i, val=this.value;
                closeAllLists();
                if(!val){return false;}
                currentFocus=-1;
                a=document.createElement("DIV");
                a.setAttribute("id", this.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items");
                this.parentNode.appendChild(a);

                for (i = 0; i < arr.length; i++){
                    if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()){
                        b = document.createElement("DIV");

                        b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                        b.innerHTML += arr[i].substr(val.length);
                        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";

                        b.addEventListener("click", function (e) {
                            search.value = this.getElementsByTagName("input")[0].value;
                            closeAllLists();
                        });
                        a.appendChild(b);
                    }
                }

            });

            /*execute a function presses a key on the keyboard:*/
            search.addEventListener("keydown", function(e)
            {

                var x = document.getElementById(this.id + "autocomplete-list");
                if (x) x = x.getElementsByTagName("div");

                if (e.keyCode == 40){
                    currentFocus++;
                    addActive(x);
                }else if (e.keyCode == 38){
                    currentFocus--;
                    addActive(x);
                }else if (e.keyCode == 13){
                    e.preventDefault();
                    if (currentFocus > -1){
                        if (x) x[currentFocus].click();
                    }
                }

            });

            function addActive(x)
            {

                if (!x) return false;
                removeActive(x);

                if (currentFocus >= x.length) currentFocus = 0;
                if (currentFocus < 0) currentFocus = (x.length - 1);

                x[currentFocus].classList.add("autocomplete-active");

            }

            function removeActive(x)
            {

                for (var i = 0; i < x.length; i++) {
                    x[i].classList.remove("autocomplete-active");
                }

            }

            function closeAllLists(elmnt)
            {

                var x = document.getElementsByClassName("autocomplete-items");
                for (var i = 0; i < x.length; i++) {
                    if (elmnt != x[i] && elmnt != search) {
                        x[i].parentNode.removeChild(x[i]);
                    }
                }

            }


            /*execute a function when someone clicks in the document:*/
            document.addEventListener("click", function (e)
            {

                closeAllLists(e.target);

                var inputProName = document.getElementById("myInput");
                var inputProId = document.getElementById("proId");
                var valueProName = inputProName.value;
                var index = productNames.indexOf(valueProName);
                if (index === -1) {
                    inputProName.value = "";
                }else{
                    inputProId.value = productIds[index];
                }

            });

        }

        var productName = ' <?php
            echo $productNames;
            ?>
            ';
        var productNames = productName.split(',');
        var productId = ' <?php
            echo $productIds;
            ?>
            ';
        var productIds = productId.split(',');

        autocomplete(document.getElementById("myInput"), productNames);

    </script>

    <div class="ks-scrollable"></div>
@endsection

@section('footer')
@endsection
