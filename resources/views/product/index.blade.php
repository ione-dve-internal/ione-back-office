@extends('layouts.app')
@section('title', 'Product')

@section('header')

@endsection

@section('content')
<!-- BEGIN DASHBOARD HEADER -->
<div class="ks-header">
   <section class="ks-title">
      <h3>Products</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="index.html">
               <span class="fa fa-home ks-icon"></span>
            </a>
            <span class="breadcrumb-item active">Products</span>
            <a href="product-management/create" class="breadcrumb-item">New Item</a>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->

<!-- BEGIN DASHBOARD CONTENT -->

<!-- SHOW DATA ON BROWSER -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid">
         <table id="ks-datatable" class="table table-striped table-bordered" width="100%">
            <thead>
               <tr>
                  <th colspan="12">
                     <form class="form-inline" action="{{url('product-management')}}" style="float:right;">
                        <div class="form-group">
                           <input class="form-control" style="width:200px;" type="text" name="search" placeholder="Search By Name And Code..." value="{{$search}}" required>
                        </div>
                     </form>
                  </th>
               </tr>
               <tr>
                  <th>ProductCategory</th>
                  <th>Code</th>
                  <th>Name</th>
                  <th>Price</th>
                  <th>Description</th>
                  <th>Related</th>
                  <th>Size</th>
                  <th>Color</th>
                  <th>Hot</th>
                  <th>Recommend</th>
                  <th>Status</th>
                  <th>Action</th>
               </tr>
            </thead>
             <tbody>
                  @foreach($results as $result)
                      <tr>
                         @if(!empty($result->get('category')))
                            <td>{{$result->get('category')->get('title')}}</td>
                         @endif
                         <td>{{$result->get('code')}}</td>
                         <td>{{$result->get('name')}}</td>
                         <td>{{$result->get('price')}}</td>
                         <td>
                             <a href="#" data-toggle="popover" title="Description" id="view" data-content="{{$result->get('desc')}}" style="text-decoration: none;">
                               <span class="fa fa-eye"> View...</span>
                             </a>
                         </td>
                         <td><a href="{{url('product-management/' . $result->getObjectId() . '/related')}}">
                                  <span class="fa fa-eye"> View...</span>
                             </a>
                         </td>
                         <td>{{implode(', ', (array)$result->get('size'))}}</td>
                         <td>{{implode(', ', (array)$result->get('color'))}}</td>
                         <td>
                            @if($result->get('hot')==1)
                               <span class="badge ks-circle badge-success">True</span>
                            @else
                               <span class="badge ks-circle badge-danger">False</span>
                            @endif
                         </td>
                         <td>
                            @if($result->get('recommend')==1)
                               <span class="badge ks-circle badge-success">True</span>
                            @else
                               <span class="badge ks-circle badge-danger">False</span>
                            @endif
                         </td>
                         <td>
                            @if($result->get('status')==1)
                            <span class="badge ks-circle badge-success">Enabled</span>
                            @else
                            <span class="badge ks-circle badge-danger">Disabled</span>
                            @endif
                         </td>
                             {{-- DROPDOWN --}}
                         <td class="table-actions">
                            <div class="dropdown padding-top-10">
                               <a class="btn btn-link" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <span class="fa fa-ellipsis-h"></span>
                               </a>
                               <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                  <a class="dropdown-item" href="{{url('product-management/addrelated/'.$result->getObjectId())}}">
                                     <span class="fa fa-book icon text-primary-on-hover"></span> Add related
                                  </a>
                                  <a class="dropdown-item" href="{{url('product-management/image/'.$result->getObjectId())}}">
                                     <span class="fa fa-image icon text-primary-on-hover"></span> Add image
                                  </a>
                                  <a class="dropdown-item" href="{{url('product-management/edit/'.$result->getObjectId())}}">
                                     <span class="fa fa-pencil icon text-primary-on-hover"></span> Edit info
                                  </a>
                                  <a class="dropdown-item" href="{{url('product-management/delete/'.$result->getObjectId())}}" onclick="return confirm('Do you want to delete {{ $result->get('name') }}?')">
                                     <span class="fa fa-trash icon text-danger-on-hover"></span> Delete
                                  </a>
                               </div>
                            </div>
                         </td>
                          {{-- END DROPDOWN --}}
                      </tr>
                  @endforeach
            </tbody>
         </table>
         <!-- being pagination -->
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
               <nav aria-label="Page navigation example">
                  <ul class="pagination">
                     @if($totalPage<=10)
                        <li class="page-item"><a class="page-link" href="{{url('product-management/page/'.$previous)}}">Previous</a></li>
                        @for($i=1; $i <= $totalPage; $i++)
                           <li class="page-item {{$i == $currentPage?'active':''}}">
                              <a class="page-link" href="{{url('product-management/page/'.$i)}}">{{ $i }}</a>
                           </li>
                        @endfor
                        <li class="page-item"><a class="page-link" href="{{url('product-management/page/'.$next)}}">Next</a></li>
                     @else
                        @if($currentPage<$totalPage-10)
                           @if($currentPage>3)
                              <li class="page-item"><a class="page-link" href="{{url('product-management/page/'.$previous)}}">Previous</a></li>
                              @for($i=$currentPage-3; $i < $currentPage+5; $i++)
                                 <li class="page-item {{$i == $currentPage?'active':''}}">
                                    <a class="page-link" href="{{url('product-management/page/'.$i)}}">{{ $i }}</a>
                                 </li>
                              @endfor
                           @else
                              <li class="page-item"><a class="page-link" href="{{url('product-management/page/'.$previous)}}">Previous</a></li>
                              @for($i=1; $i <= 9; $i++)
                                 <li class="page-item {{$i == $currentPage?'active':''}}">
                                    <a class="page-link" href="{{url('product-management/page/'.$i)}}">{{ $i }}</a>
                                 </li>
                              @endfor
                           @endif
                           <li class="page-item"><span class="page-link">...</span></li>
                           @for($i=$totalPage-2; $i < $totalPage; $i++)
                              <li class="page-item {{$i == $currentPage?'active':''}}">
                                 <a class="page-link" href="{{url('product-management/page/'.$i)}}">{{ $i }}</a>
                              </li>
                           @endfor
                           <li class="page-item"><a class="page-link" href="{{url('product-management/page/'.$next)}}">Next</a></li>
                        @else
                           <li class="page-item"><a class="page-link" href="{{url('product-management/page/'.$previous)}}">Previous</a></li>
                           @for($i=8; $i <= 10; $i++)
                              <li class="page-item {{$i == $currentPage?'active':''}}">
                                 <a class="page-link" href="{{url('product-management/page/'.$i)}}">{{ $i }}</a>
                              </li>
                           @endfor
                           <li class="page-item"><span class="page-link">...</span></li>
                           @for($i=$totalPage-10; $i < $totalPage; $i++)
                              <li class="page-item {{$i == $currentPage?'active':''}}">
                                 <a class="page-link" href="{{url('product-management/page/'.$i)}}">{{ $i }}</a>
                              </li>
                           @endfor
                           <li class="page-item"><a class="page-link" href="{{url('product-management/page/'.$next)}}">Next</a></li>
                        @endif
                     @endif
                  </ul>
               </nav>
            </div>
         </div>
         <!-- end pagination -->
      </div>
   </div>
</div>
<!-- END DASHBOARD CONTENT -->
<div class="ks-scrollable"></div>
@endsection

@section('footer')
    {{-- POPOVER SELECT --}}
    <script>
        $(document).ready(function(){
            $('[data-toggle="popover"]').popover({
                trigger: 'focus'
            });
        });

    </script>
@endsection
