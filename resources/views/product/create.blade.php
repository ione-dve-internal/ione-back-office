@extends('layouts.app')
@section('title', 'Create Product')

@section('header')

@endsection

@section('content')
<!-- BEGIN DASHBOARD HEADER -->
<div class="ks-header">
   <section class="ks-title">
      <h3>Create Product</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="{{url('dashboard')}}">
               <span class="fa fa-home ks-icon"></span>
            </a>
            <span class="breadcrumb-item active">New Item</span>
            <a href="{{url('product-management')}}" class="breadcrumb-item">Back</a>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->

<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid">
         <form role="form" method="post" enctype="multipart/form-data" action="{{url('product-management/insert')}}">
            <div class="form-group">
               <select class="form-control "  name="category"  >
                  @foreach($productcategories as $productcategory)
                     <option value="{{ $productcategory->getObjectId() }}">
                        {{ $productcategory->get('title') }}
                     </option>
                  @endforeach
               </select>
            </div>
   			<div class="form-group">
   				<input class="form-control" type="text" name="code" placeholder="Code (Required)" required>
   			</div>
            <div class="form-group">
					<input class="form-control" type="text" name="name" placeholder="Name (Required)" required>
            </div>
            <div class="form-group">
					<input class="form-control" type="text" name="price" placeholder="Price (Required)" required>
            </div>
            <div class="form-group">
                <textarea class="form-control" name="desc" placeholder="Description (Required)" ></textarea>
            </div>

            {{-- IMPUT MULTIPLE SIZE & COLOR--}}
            <div class="row">
               <div class="col-md-6">
                  <input class="form-control" type="text" autocomplete="off" id="size" placeholder="Size (Optional)">
               </div>
               <div class="col-md-1">
                  <input type="button" onclick="mysizeFunction()" value="Add" class="form-control btn-default">
               </div>
               <div class="col-md-5">
                  <input type="text" name="size" id="result" class="form-control">
               </div>
            </div>

            <script>
                var arrsize = [];
                document.getElementById("result").value=arrsize;
                function mysizeFunction() {
                    var x = document.getElementById("size").value;
                    arrsize.push(x);
                    arrsize.toString();
                    document.getElementById("result").value=arrsize;
                }
                document.addEventListener("click", function (e) {

                    document.getElementById("size").value="";
                });
            </script>

            <div class="row">
               <div class="col-md-6">
                  <input class="form-control" type="text" autocomplete="off" id="color" placeholder="Color (Optional)">
               </div>
               <div class="col-md-1">
                  <input type="button" onclick="myFunction()" value="Add" class="form-control btn-default">
               </div>
               <div class="col-md-5">
                  <input type="text" name="color" id="rcolor" class="form-control">
               </div>
            </div>


            <script>
                var arrcolor = [];
                document.getElementById("rcolor").value=arrcolor;
                function myFunction() {
                    var x = document.getElementById("color").value;
                    arrcolor.push(x);
                    document.getElementById("rcolor").value=arrcolor;
                }
                document.addEventListener("click", function (e) {

                    document.getElementById("color").value="";
                });
            </script>
            {{-- END INPUT MULTIPLE SIZE & COLOR --}}

            {{-- INPUT STATUS --}}
            <div class="form-group">
               <label class="margin-right-10">Hot</label>
               <label class="custom-control custom-radio">
                  <input id="radio1" name="hot" value="1" type="radio" class="custom-control-input" checked>
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description">True</span>
               </label>
               <label class="custom-control custom-radio">
                  <input id="radio1" name="hot" value="0" type="radio" class="custom-control-input">
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description">False</span>
               </label>
            </div>
            <div class="form-group">
               <label class="margin-right-10">Recommend</label>
               <label class="custom-control custom-radio">
                  <input id="radio1" name="recommend" value="1" type="radio" class="custom-control-input" checked>
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description">True</span>
               </label>
               <label class="custom-control custom-radio">
                  <input id="radio1" name="recommend" value="0" type="radio" class="custom-control-input">
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description">False</span>
               </label>
            </div>
            <div class="form-group">
               <label class="margin-right-10">Status</label>
               <label class="custom-control custom-radio">
                  <input id="radio1" name="status" value="1" type="radio" class="custom-control-input" checked>
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description">Enable</span>
               </label>
               <label class="custom-control custom-radio">
                  <input id="radio1" name="status" value="0" type="radio" class="custom-control-input">
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description">Disable</span>
               </label>
            </div>
            {{-- END INPUT STATUS --}}

            <div class="form-group">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
   				<button type="submit" name="save" class="btn btn-primary">Save</button>
            </div>
         </form>
      </div>
   </div>
</div>
<!-- END DASHBOARD CONTENT -->
<div class="ks-scrollable"></div>
@endsection

@section('footer')

@endsection
