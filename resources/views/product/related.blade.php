@extends('layouts.app')
@section('title', 'Product Related')

@section('header')

@endsection

@section('content')
    <!-- BEGIN DASHBOARD HEADER -->
    <div class="ks-header">
        <section class="ks-title">
            <h3>Related Products</h3>
            <div class="ks-controls">
                <nav class="breadcrumb ks-default">
                    <a class="breadcrumb-item ks-breadcrumb-icon" href="index.html">
                        <span class="fa fa-home ks-icon"></span>
                    </a>
                    <span class="breadcrumb-item active">Images</span>
                    <a href="{{url('product-management')}}" class="breadcrumb-item">back</a>
                </nav>
            </div>
        </section>
    </div>

    <!-- END DASHBOARD HEADER -->
    <div class="ks-content">
        <div class="ks-body">
            <div class="container-fluid">
                <!-- SHOW MAIN RELATED PRODUCT -->
                <div class="form-group">
                   <h4>Main Product</h4>
                   <table id="ks-datatable">
                       <tbody>
                           <tr style="font-size: 15px;">
                               <td style="padding:0px 0px 15px 0px;">Product Name :</td>
                               <td style="padding:0px 0px 15px 50px;">{{$product->get('name')}}</td>
                           </tr>
                           <tr style="font-size: 15px;">
                               <td style="padding:0px 0px 15px 0px;">Product Code :</td>
                               <td style="padding:0px 0px 15px 50px;">{{$product->get('code')}}</td>
                           </tr>
                       </tbody>
                   </table>
               </div>
                <hr>
                {{-- END MAIN RELATED PRO --}}

                <!-- SHOW PRODUCT RELATED-->
                <table id="ks-datatable" class="table table-striped table-bordered" width="100%">
                    <thead>
                    <h4>Related Product</h4>
                    <tr>
                        <th>Code</th>
                        <th>Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($results as $relatedProduct )
                        <tr>
                            <td>{{$relatedProduct->get('code')}}</td>
                            <td>{{$relatedProduct->get('name')}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{-- END PRO RELATED --}}
            </div>
        </div>
    </div>
    <!-- END DASHBOARD CONTENT -->
    <div class="ks-scrollable"></div>
@endsection

@section('footer')
    <script>
        $(document).ready(function(){
            $('[data-toggle="popover"]').popover();
        });
    </script>
@endsection
