@extends('layouts.app')
@section('title', 'Product')

@section('header')

@endsection

@section('content')
    <!-- BEGIN DASHBOARD HEADER -->
    <div class="ks-header">
        <section class="ks-title">
            <h3>Products</h3>
            <div class="ks-controls">
                <nav class="breadcrumb ks-default">
                    <a class="breadcrumb-item ks-breadcrumb-icon" href="index.html">
                        <span class="fa fa-home ks-icon"></span>
                    </a>
                    <span class="breadcrumb-item active">Products</span>
                    <a href="product-management/create" class="breadcrumb-item">New Item</a>
                </nav>
            </div>
        </section>
    </div>
    <!-- END DASHBOARD HEADER -->

    <!-- BEGIN DASHBOARD CONTENT -->

    <!-- SHOW DATA ON BROWER -->
    <div class="ks-content">
        <div class="ks-body">
            <div class="container-fluid">
                <table id="ks-datatable" class="table table-striped table-bordered" width="100%">
                    <thead>
                    <tr>
                        <th>ProductCategory</th>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Description</th>
                        <th>Related</th>
                        <th>Size</th>
                        <th>Color</th>
                        <th>Hot</th>
                        <th>Recommend</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$result->get('category')->get('title')}}</td>
                            <td>{{$result->get('code')}}</td>
                            <td>{{$result->get('name')}}</td>
                            <td>{{$result->get('price')}}</td>
                            <td>
                                <a href="#" data-toggle="popover" title="Description" id="view" data-content="{{$result->get('desc')}}" style="text-decoration: none;">
                                    <span class="fa fa-eye"> View...</span>
                                </a>
                            </td>
                            <td>
                                <a href="{{url('product-management/' . $result->getObjectId() . '/related')}}">
                                    <span class="fa fa-eye">View...</span>
                                </a>
                            </td>
                            <td>{{implode(', ', (array)$result->get('size'))}}</td>
                            <td>{{implode(', ', (array)$result->get('color'))}}</td>
                                <td>
                                    @if($result->get('hot')==1)
                                        <span class="badge ks-circle badge-success">True</span>
                                    @else
                                        <span class="badge ks-circle badge-danger">Fale</span>
                                    @endif
                                </td>
                                <td>
                                    @if($result->get('recommend')==1)
                                        <span class="badge ks-circle badge-success">True</span>
                                    @else
                                        <span class="badge ks-circle badge-danger">Fale</span>
                                    @endif
                                </td>
                            <td>
                                @if($result->get('status')==1)
                                    <span class="badge ks-circle badge-success">Enabled</span>
                                @else
                                    <span class="badge ks-circle badge-danger">Disabled</span>
                                @endif
                            </td>
                            <td class="table-actions">
                                <div class="dropdown padding-top-10">
                                    <a class="btn btn-link" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="fa fa-ellipsis-h"></span>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                        <a class="dropdown-item" href="{{url('product-management/addrelated/'.$result->getObjectId())}}">
                                            <span class="fa fa-book icon text-primary-on-hover"></span> Add related
                                        </a>
                                        <a class="dropdown-item" href="{{url('product-management/image/'.$result->getObjectId())}}">
                                            <span class="fa fa-image icon text-primary-on-hover"></span> Add image
                                        </a>
                                        <a class="dropdown-item" href="{{url('product-management/edit/'.$result->getObjectId())}}">
                                            <span class="fa fa-pencil icon text-primary-on-hover"></span> Edit info
                                        </a>
                                        <a class="dropdown-item" href="{{url('product-management/delete/'.$result->getObjectId())}}" onclick="return confirm('Do you want to delete {{ $result->get('name') }}?')">
                                            <span class="fa fa-trash icon text-danger-on-hover"></span> Delete
                                        </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END DASHBOARD CONTENT -->
    <div class="ks-scrollable"></div>
@endsection

@section('footer')
    {{-- POPOVER SELECT --}}
    <script>
        $(document).ready(function(){
            $('[data-toggle="popover"]').popover({
                trigger: 'focus'
            });
        });

    </script>
@endsection