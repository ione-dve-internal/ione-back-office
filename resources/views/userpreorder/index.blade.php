@extends('layouts.app')
@section('title', 'Customers')

@section('header')
<link rel="stylesheet" type="text/css" href="{{url('libs/datatables-net/media/css/dataTables.bootstrap4.min.css')}}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{url('assets/styles/libs/datatables-net/datatables.min.css')}}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{url('libs/select2/css/select2.min.css')}}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{url('assets/styles/libs/select2/select2.min.css')}}"> <!-- Customization -->
@endsection

@section('content')
<!-- BEGIN DASHBOARD HEADER -->
<div class="ks-header">
   <section class="ks-title">
      <h3>Customers</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="index.html">
               <span class="fa fa-home ks-icon"></span>
            </a>
            <span class="breadcrumb-item active">Customers</span>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->

<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid">
         <table id="ks-datatable" class="table table-striped table-bordered" width="100%">
            <thead>
               <tr>
                  <th data-field="customer" data-sortable="true">Customer</th>
                  <th data-field="product" data-sortable="true">Product</th>
                  <th data-field="appearance" data-sortable="true">Appearance</th>
                  <th data-field="capacity" data-sortable="true">Capacity</th>
                  <th data-field="code" data-sortable="true">Code</th>
                  <th data-field="orderdate" data-sortable="true">Order Date</th>
                  <th data-field="getproduct" data-sortable="true" width="120">Get Product</th>
               </tr>
            </thead>
            <tbody>
               @foreach($results as $result)
                  <tr>
                     <td>{{ $result->get('user') ? $result->get('user')->get('lastName').' '.$result->get('user')->get('firstName') : "No Name" }}</td>
                     <td>{{ $result->get('detail') ? $result->get('detail')->get('productName') : "No product" }}</td>
                     <td>{{ $result->get('appearence') ? $result->get('appearence')->get('title') : "No Appearence" }}</td>
                     <td>{{ $result->get('price') ? $result->get('price')->get('capacity') : "No Capacity" }}</td>
                     <td>{{ $result->get('code') ? $result->get('code') : "No Code"}}</td>
                     <td>{{ $result->get('orderdate') ? $result->get('orderdate')->format('d-M-Y H:m') : "No Order Date" }}</td>
                     <td align="center">
                        @if($result->get('getProduct')==True)

                           <label class="ks-checkbox-switch ks-primary">
                              <input id="{{ $result->getObjectId() }}" type="checkbox" name="getProduct" checked>
                              <span class="ks-wrapper"></span>
                              <span class="ks-indicator"></span>
                              <span class="ks-on">Yes</span>
                              <span class="ks-off">No</span>
                           </label>

                        @else

                           <label  class="ks-checkbox-switch ks-primary">
                              <input id="{{ $result->getObjectId() }}" type="checkbox" name="getProduct">
                              <span class="ks-wrapper"></span>
                              <span class="ks-indicator"></span>
                              <span class="ks-on">Yes</span>
                              <span class="ks-off">No</span>
                           </label>

                        @endif
                     </td>
                  </tr>
               @endforeach
            </tbody>
         </table>
      </div>
   </div>
</div>

<div class="modal" style="display: none; background-color:#fff; opacity:0.8; margin: auto;" >
    <div style="margin: auto; position: absolute;top: 50%; left: 50%;">
        <img alt="" src="{{url('assets/img/loaders/page-loader-ring-lg-primary.gif')}}">
    </div>
</div>
<!-- END DASHBOARD CONTENT -->
<div class="ks-scrollable"></div>
@endsection

@section('footer')
   @foreach($results as $result)
   <script type="text/javascript">
      //update user preorder get product with ajax
      $(function(){
         $('#{{ $result->getObjectId() }}').on('change', function() {

            setTimeout(function() {
               $(".modal").show();
            }, 600);

            if ($(this).prop('checked')) {
               $.ajax({
                  type: 'get',
                  url: "{{ url('userpreorders/updategetproduct/'.$result->getObjectId().'/1') }}",
                  success: function (response) {
                     $(".modal").hide();
                     console.log(response);
                  },
                  error: function(jqXHR, textStatus, errorThrown) {
                     alert('Checked fail!');
                     console.log(textStatus, errorThrown);
                  }
               });
            }
            else {
               $.ajax({
                  type: 'get',
                  url: "{{ url('userpreorders/updategetproduct/'.$result->getObjectId().'/0') }}",
                  success: function (response) {
                     $(".modal").hide();
                     console.log(response);
                  },
                  error: function(jqXHR, textStatus, errorThrown) {
                     alert('Unchecked fail!');
                     console.log(textStatus, errorThrown);
                  }
               });
            }
         });
      });
   </script>
   @endforeach

   <script src="{{url('libs/datatables-net/media/js/jquery.dataTables.min.js')}}"></script>
   <script src="{{url('libs/datatables-net/media/js/dataTables.bootstrap4.min.js')}}"></script>
   <script src="{{url('libs/select2/js/select2.min.js')}}"></script>
   <script type="application/javascript">
   (function ($) {
       $(document).ready(function() {
           $('#ks-datatable').DataTable({
               "initComplete": function () {
                   $('.dataTables_wrapper select').select2({
                       minimumResultsForSearch: Infinity
                   });
               }
           });
       });
   })(jQuery);
   </script>
@endsection
