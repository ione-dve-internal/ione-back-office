@extends('layouts.app')
@section('title', 'New Order')

@section('header')
    <!-- BEGIN CHECK IMAGE DIMENSIONS -->
    <script type="text/javascript" src="{{url('assets/scripts/checkimagedimensions.js')}}"></script>
    <!-- END CHECK IMAGE DIMENSIONS -->

    <!-- MAP PICKER -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src='http://maps.google.com/maps/api/js?key=AIzaSyDvYzz9Qhl2NLOQrSfYqyJboUmGog1e1Ow&sensor=false&libraries=places'></script>
    <script src="{{url('assets/mappicker/locationpicker.jquery.min.js')}}"></script>
    <!-- Phone Formating -->
    <script type="text/javascript" src="{{url('assets/scripts/bootstrap-formhelpers-phone.js')}}"></script>
@endsection

@section('content')
    <!-- BEGIN DASHBOARD HEADER -->
    <div class="ks-header">
        <section class="ks-title">
            <h3>Create Order</h3>
            <div class="ks-controls">
                <nav class="breadcrumb ks-default">
                    <a class="breadcrumb-item ks-breadcrumb-icon" href="{{url('dashboard')}}">
                        <span class="fa fa-home ks-icon"></span>
                    </a>
                    <span class="breadcrumb-item active">New Item</span>
                    <a href="{{url('order-management')}}" class="breadcrumb-item">Back</a>
                </nav>
            </div>
        </section>
    </div>
    <!-- END DASHBOARD HEADER -->

    <!-- BEGIN DASHBOARD CONTENT -->
    <div class="ks-content">
        <div class="ks-body">
            <div class="container-fluid">
                <form role="form" method="post" enctype="multipart/form-data" action="{{url('order-management/insert')}}">
                    <label>Product</label>
                    <div class="form-group">
                        <select class="form-control" name="product">
                            @foreach($products as $product)
                                <option value="{{ $product->getObjectId() }}">
                                    {{ $product->get('name') }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" name="size" placeholder="Size (Required)" required>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" name="qty" placeholder="Qty (Required)" required>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" name="color" placeholder="Color (Required)" required>
                    </div>
                    <label>First name</label>
                    <div class="form-group">
                        <select class="form-control" name="firstName">
                            @foreach($users as $user)
                                <option value="{{ $user->getObjectId() }}">
                                    {{ $user->get('firstName') }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <label>Last name</label>
                    <div class="form-group">
                        <select class="form-control" name="lastName">
                            @foreach($users as $user)
                                <option value="{{ $user->getObjectId() }}">
                                    {{ $user->get('lastName') }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" name="price" placeholder="Price (Required)" required>
                    </div>

                    <div class="form-group">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button type="submit" name="save" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END DASHBOARD CONTENT -->
    <div class="ks-scrollable"></div>
@endsection

@section('footer')

@endsection
