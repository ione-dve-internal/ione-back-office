@extends('layouts.app')
@section('title', 'Update Status')

@section('header')
    <!-- MAP PICKER -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src='http://maps.google.com/maps/api/js?key=AIzaSyDvYzz9Qhl2NLOQrSfYqyJboUmGog1e1Ow&sensor=false&libraries=places'></script>
    <script src="{{url('assets/mappicker/locationpicker.jquery.min.js')}}"></script>
    <!-- Phone Formating -->
    <script type="text/javascript" src="{{url('assets/scripts/bootstrap-formhelpers-phone.js')}}"></script>
@endsection

@section('content')
    <!-- BEGIN DASHBOARD HEADER -->
    <div class="ks-header">
        <section class="ks-title">
            <h3>Update Order Status</h3>
            <div class="ks-controls">
                <nav class="breadcrumb ks-default">
                    <a class="breadcrumb-item ks-breadcrumb-icon" href="{{url('dashboard')}}">
                        <span class="fa fa-home ks-icon"></span>
                    </a>
                    <span class="breadcrumb-item active">Update Item</span>
                    <a href="{{url('order-management')}}" class="breadcrumb-item">Back</a>
                </nav>
            </div>
        </section>
    </div>
    <!-- END DASHBOARD HEADER -->

    <!-- BEGIN DASHBOARD CONTENT -->
    <div class="ks-content">
        <div class="ks-body">
            <div class="container-fluid">
                <h4>User Order</h4>
                <table>
                    <tr style="font-size: 15px;">
                        <td style="padding:0px 0px 15px 0px;">Order ID :</td>
                        <td style="padding:0px 0px 15px 50px;">{{$result[0]->get('userOrder')->getObjectId()}}</td>
                    </tr>

                    <tr style="font-size: 15px;">
                        <td style="padding:0px 0px 15px 0px;">First Name :</td>
                        <td style="padding:0px 0px 15px 50px;">{{$result[0]->get('userOrder')->get('firstName')}}</td>
                    </tr>
                    <tr style="font-size: 15px;">
                        <td style="padding:0px 0px 15px 0px;">Last Name :</td>
                        <td style="padding:0px 0px 15px 50px;">{{$result[0]->get('userOrder')->get('lastName')}}</td>
                    </tr>
                    <tr style="font-size: 15px;">
                        <td style="padding:0px 0px 15px 0px;">Payment Method :</td>
                        <td style="padding:0px 0px 15px 50px;">{{$result[0]->get('userOrder')->get('paymethod')}}</td>
                    </tr>
                    <tr style="font-size: 15px;">
                        <td style="padding:0px 0px 15px 0px;">Shipping :</td>
                        <td style="padding:0px 0px 15px 50px;">{{$result[0]->get('userOrder')->get('shipping')}}</td>
                    </tr>
                    <tr style="font-size: 15px;">
                        <td style="padding:0px 0px 15px 0px;">Order Date :</td>
                        <td style="padding:0px 0px 15px 50px;">{{date_format($result[0]->getCreatedAt(),"Y-M-d")}} &nbsp;{{date_format($result[0]->getCreatedAt()," H:i:s")}}</td>
                    </tr>
                </table>

                <hr>
                <h4>Product</h4>

                <table id="ks-datatable" class="table table-striped table-bordered" width="100%">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Size</th>
                        <th>Qty</th>
                        <th>Color</th>
                        <th>Price</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($result as $x)
                        @php
                            $price=(double)$x->get('price');
                            $qty=(int)$x->get('qty');
                            $total=$price*$qty;
                        @endphp
                        <tr>
                            @if(!empty($x->get('product')))
                                <td>{{$x->get('product')->get('name')}}</td>
                            @else
                                <td>None</td>
                            @endif
                                <td>{{$x->get('size')}}</td>
                                <td>{{$x->get('qty')}}</td>
                                <td>{{$x->get('color')}}</td>
                                <td>{{$x->get('price')}}</td>
                                <td>{{$total}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <form role="form" method="post" enctype="multipart/form-data" action="{{url('order-management/update/'.$x->get('userOrder')->getObjectId())}}">
                    <label>Status</label>
                    <div class="form-group">
                        <select class="form-control" name="status">
                            <option value="Ordered" @if($x->get('userOrder')->get("status") == "Ordered") selected @endif  >Ordered</option>
                            <option value="Delivery" @if($x->get('userOrder')->get("status") == "Delivery") selected @endif >Delivery</option>
                            <option value="Prepared" @if($x->get('userOrder')->get("status") == "Prepared") selected @endif >Prepared</option>
                            <option value="Done" @if($x->get('userOrder')->get("status") == "Done") selected @endif >Done</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button type="submit" name="save" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END DASHBOARD CONTENT -->
    <div class="ks-scrollable"></div>
@endsection

@section('footer')

@endsection