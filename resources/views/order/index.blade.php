@extends('layouts.app')
@section('title', 'Order')

@section('header')

    <script type="text/javascript" src="{{url('assets/scripts/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/scripts/datetimepicker/jquery.timepicker.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{url('assets/styles/datetimepicker/jquery.timepicker.css')}}" />
    <script type="text/javascript" src="{{url('assets/scripts/datetimepicker/bootstrap-datepicker.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{url('assets/styles/datetimepicker/bootstrap-datepicker.css')}}" />


@endsection

@section('content')
    <!-- BEGIN DASHBOARD HEADER -->
    <div class="ks-header">
        <section class="ks-title">
            <h3>User Orders List</h3>
        </section>
    </div>
    <!-- END DASHBOARD HEADER -->

    <!-- BEGIN DASHBOARD CONTENT -->
    <div class="ks-content">
        <div class="ks-body">
            <div class="container-fluid">
                <div style="position: absolute;top: 0;">
                    <button id="btnFilter"><i class="fa fa-sliders" style="font-size: 20px;"></i></button>
                    <form method="post" style="display: inline" id="frm">

                        <small>Date :</small>
                        <i id="datepairClaim">
                            <input id="date" type="text" name="date" class="date  datepicker-control" placeholder="Filter Date" autocomplete="off"
                                   style="height: 20px; width: 150px; display: inline;"/>
                        </i>
                        <small>Status :</small>
                        <select id="status" name="status">
                            <option selected value="">None</option>
                            <option value="Ordered">Ordered</option>
                            <option value="Delivery">Delivery</option>
                            <option value="Prepared">Prepared</option>
                            <option value="Done">Done</option>
                        </select>&nbsp;&nbsp;
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button type="button" id="btn-submit" style="border-radius: 5px; font-size: 10px;" class="btn-default" url="{{url('order-management')}}">Go</button>
                    </form>
                    <!--Date Time Picker-->

                    <script src="{{url('assets/scripts/datetimepicker/datepair.js')}}"></script>
                    <script src="{{url('assets/scripts/datetimepicker/jquery.datepair.js')}}"></script>

                    <script>
                        $(document).ready(function(){
                            $("#frm").hide();
                            $("#btnFilter").click(function () {
                                $("#frm").toggle();
                            });
                            $('#btn-submit').click(function () {
                                // console.log($(this).attr('url') + "?filters['date']='" + $("#date").val() + "'&filters['status']='" + $("#status").val()) + "'";
                                var url = $(this).attr('url');
                                const date = $('#date').val();
                                const status = $('#status').val();
                                if (date !== '' && status !== '') {
                                    url = url + "?filters[date]=" + date + "&filters[status]=" + status;
                                }
                                else if (date !== '' && status === '') {
                                    url = url + "?filters[date]=" + date;

                                }
                                else if (date === '' && status !== '') {
                                    url = url + "?filters[status]=" + status;
                                }

                                window.location.href = url;
                            })
                        });
                        $('#datepairClaim .time').timepicker({
                            'showDuration': true,
                            'timeFormat': 'g:ia'
                        });

                        $('#datepairClaim .date').datepicker({
                            'format': 'm/d/yyyy',
                            'autoclose': true
                        });

                        $('#datepairClaim').datepair();
                    </script>

                    <!--End Date Time Picker-->
                </div>

                <!--TABLE-->
                <table id="ks-datatable" class="table table-striped table-bordered" width="100%" >
                    <thead>
                    <tr>
                        <th>Order ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Payment Method</th>
                        <th>Shipping</th>
                        <th>Order Date</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($results as $result)
                        <tr>
                            <td>{{$result->getObjectId()}}</td>
                            <td>{{$result->get('firstName')}}</td>
                            <td>{{$result->get('lastName')}}</td>
                            <td>{{$result->get("paymethod")}}</td>
                            <td>{{$result->get("shipping")}}</td>
                            <td>{{date_format($result->getCreatedAt(),"Y-M-d ")}} &nbsp;&nbsp;{{date_format($result->getCreatedAt(),"H:i:s")}}</td>
                            <td>
                                @if($result->get('status')=="Ordered")
                                  <span class="badge ks-circle badge-primary">Ordered</span>
                                @endif
                                @if($result->get('status')=="Delivery")
                                    <span class="badge ks-circle badge-danger">Delivery</span>
                                @endif
                                @if($result->get('status')=="Prepared")
                                    <span class="badge ks-circle badge-info">Prepared</span>
                                @endif
                                @if($result->get('status')=="Done")
                                    <span class="badge ks-circle badge-success">Done</span>
                                @endif
                            </td>
                            <td class="table-actions">
                                <div class="dropdown padding-top-10">
                                    <a class="btn btn-link" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="fa fa-ellipsis-h"></span>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                        <a class="dropdown-item" href="{{url('order-management/edit/'.$result->getObjectId())}}">
                                            <span class="fa fa-pencil icon text-primary-on-hover"></span> Update Status
                                        </a>
                                        <a class="dropdown-item" href="{{url('order-management/show/'.$result->getObjectId())}}">
                                            <span class="fa fa-eye icon text-primary-on-hover"></span> Show Order
                                        </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <!-- being pagination -->
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                @if($totalPage<=10)
                                    <li class="page-item"><a class="page-link" href="{{url('order-management/page/'.$previous)}}{{ isset($filters) ? $filters : '' }} ">Previous</a></li>
                                    @for($i=1; $i <= $totalPage; $i++)
                                        <li class="page-item {{$i == $currentPage?'active':''}}">
                                            <a class="page-link" href="{{url('order-management/page/'.$i)}}{{ isset($filters) ? $filters : '' }} ">{{ $i }}</a>
                                        </li>
                                    @endfor
                                    <li class="page-item"><a class="page-link" href="{{url('order-management/page/'.$next)}}{{ isset($filters) ? $filters : '' }} ">Next</a></li>
                                @else
                                    @if($currentPage<$totalPage-10)
                                        @if($currentPage>3)
                                            <li class="page-item"><a class="page-link" href="{{url('order-management/page/'.$previous)}}{{ isset($filters) ? $filters : '' }}">Previous</a></li>
                                            @for($i=$currentPage-3; $i < $currentPage+5; $i++)
                                                <li class="page-item {{$i == $currentPage?'active':''}}">
                                                    <a class="page-link" href="{{url('order-management/page/'.$i)}}{{ isset($filters) ? $filters : '' }}">{{ $i }}</a>
                                                </li>
                                            @endfor
                                        @else
                                            <li class="page-item"><a class="page-link" href="{{url('order-management/page/'.$previous)}}{{ isset($filters) ? $filters : '' }}">Previous</a></li>
                                            @for($i=1; $i <= 9; $i++)
                                                <li class="page-item {{$i == $currentPage?'active':''}}">
                                                    <a class="page-link" href="{{url('order-management/page/'.$i)}}{{ isset($filters) ? $filters : '' }}">{{ $i }}</a>
                                                </li>
                                            @endfor
                                        @endif

                                        <li class="page-item"><span class="page-link">...</span></li>
                                        @for($i=$totalPage-2; $i < $totalPage; $i++)
                                            <li class="page-item {{$i == $currentPage?'active':''}}">
                                                <a class="page-link" href="{{url('order-management/page/'.$i)}}{{ isset($filters) ? $filters : '' }}">{{ $i }}</a>
                                            </li>
                                        @endfor
                                        <li class="page-item"><a class="page-link" href="{{url('order-management/page/'.$next)}}{{ isset($filters) ? $filters : '' }}">Next</a></li>
                                    @else
                                        <li class="page-item"><a class="page-link" href="{{url('order-management/page/'.$previous)}}{{ isset($filters) ? $filters : '' }}">Previous</a></li>
                                        @for($i=8; $i <= 10; $i++)
                                            <li class="page-item {{$i == $currentPage?'active':''}}">
                                                <a class="page-link" href="{{url('order-management/page/'.$i)}}{{ isset($filters) ? $filters : '' }}">{{ $i }}</a>
                                            </li>
                                        @endfor

                                        <li class="page-item"><span class="page-link">...</span></li>
                                        @for($i=$totalPage-10; $i < $totalPage; $i++)
                                            <li class="page-item {{$i == $currentPage?'active':''}}">
                                                <a class="page-link" href="{{url('order-management/page/'.$i)}}{{ isset($filters) ? $filters : '' }}">{{ $i }}</a>
                                            </li>
                                        @endfor
                                        <li class="page-item"><a class="page-link" href="{{url('order-management/page/'.$next)}}{{ isset($filters) ? $filters : '' }}">Next</a></li>
                                    @endif
                                @endif
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- end pagination -->
            </div>
        </div>
    </div>
    <!-- END DASHBOARD CONTENT -->
    <div class="ks-scrollable"></div>
@endsection

@section('footer')

@endsection