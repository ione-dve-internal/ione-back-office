@extends('layouts.app')
@section('title', 'Feedback')

@section('header')
<link rel="stylesheet" type="text/css" href="{{url('libs/datatables-net/media/css/dataTables.bootstrap4.min.css')}}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{url('assets/styles/libs/datatables-net/datatables.min.css')}}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{url('libs/select2/css/select2.min.css')}}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{url('assets/styles/libs/select2/select2.min.css')}}"> <!-- Customization -->
@endsection

@section('content')
<!-- BEGIN DASHBOARD HEADER -->
<div class="ks-header">
   <section class="ks-title">
      <h3>Feedback</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="{{url('dashboard')}}">
               <span class="fa fa-home ks-icon"></span>
            </a>
            <span class="breadcrumb-item active">Feedback</span>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->


<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid">
         <table id="ks-datatable" class="table table-striped table-bordered" width="100%">
            <thead>
               <tr>
                  <th>User Name</th>
                  <th>Subject</th>
                  <th>Message</th>
                  <th width='80'>View</th>
               </tr>
            </thead>
            <tbody>
               @foreach($results as $result)
                  <tr>
                     <td>{{ $result->get('user')?$result->get('user')->get('firstName').' '.$result->get('user')->get('lastName'):'' }}</td>
                     <td>{{ $result->get('title') }}</td>
                     <td>{{ $result->get('message') }}</td>
                     <td class="text-center">
                        <a href="" data-toggle="modal" data-target="#{{$result->getObjectId()}}">
                           <span class="fa fa-eye"></span>
                        </a>
                     </td>
                  </tr>
               @endforeach
            </tbody>
         </table>
      </div>
   </div>
</div>

<!-- Modal -->
@foreach($results as $result)
   <div id="{{$result->getObjectId()}}" class="modal fade" role="dialog">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <h5>{{ $result->get('title') }}</h5>
            </div>
            <div class="modal-body">
               <p>{{ $result->get('message') }}</p>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
   </div>
@endforeach

<!-- END DASHBOARD CONTENT -->
<div class="ks-scrollable"></div>
@endsection

@section('footer')
<script src="{{url('libs/datatables-net/media/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('libs/datatables-net/media/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{url('libs/select2/js/select2.min.js')}}"></script>
<script type="application/javascript">
(function ($) {
    $(document).ready(function() {
        $('#ks-datatable').DataTable({
            "initComplete": function () {
                $('.dataTables_wrapper select').select2({
                    minimumResultsForSearch: Infinity
                });
            }
        });
    });
})(jQuery);
</script>
@endsection
