@extends('layouts.app')
@section('title', 'Update Shop Category')

@section('header')
   <!-- BEGIN CHECK IMAGE DIMENSIONS -->
   <script type="text/javascript" src="{{url('assets/scripts/checkimagedimensions.js')}}"></script>
   <!-- END CHECK IMAGE DIMENSIONS -->
@endsection

@section('content')
<!-- BEGIN DASHBOARD HEADER -->
<div class="ks-header">
   <section class="ks-title">
      <h3>Update Shop Category</h3>
      <div class="ks-controls">
         <nav class="breadcrumb ks-default">
            <a class="breadcrumb-item ks-breadcrumb-icon" href="{{url('dashboard')}}">
               <span class="fa fa-home ks-icon"></span>
            </a>
            <span class="breadcrumb-item active">Update Item</span>
            <a href="{{url('shopcategory')}}" class="breadcrumb-item">Back</a>
         </nav>
      </div>
   </section>
</div>
<!-- END DASHBOARD HEADER -->

<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-content">
   <div class="ks-body">
      <div class="container-fluid">
         <form role="form" method="post" enctype="multipart/form-data" action="{{url('shopcategory/update/'.$shopCategory->getObjectId())}}">
            <div class="form-group">
					<input class="form-control" type="text" name="name" placeholder="Shop Category Name (Required)" value="{{ $shopCategory->get('name') }}" required>
				</div>

            <div class="row">

               <div class="col-lg-6">
                  <h4>Upload Photo</h4>
                  <div class="card panel panel-default ks-information ks-light">
                     <h5 class="card-header">

                           <input type="file" name="photo">



                     </h5>
                     <div class="card-block text-center">
                        <img src="{{ $shopCategory->get('photo')!=NULL?$shopCategory->get('photo')->getUrl():'' }}" style="width:50%">
                     </div>
                  </div>
               </div>

               <div class="col-lg-6">
                  <h4>Upload Pin Icon</h4>
                  <div class="card panel panel-default ks-information ks-light">
                     <h5 class="card-header">

                           <input type="file" name="pinIcon">


                     </h5>
                     <div class="card-block text-center">
                        <img src="{{ $shopCategory->get('pin')!=NULL?$shopCategory->get('pin')->getUrl():'' }}" style="width:50%">
                     </div>
                  </div>
               </div>
            </div>

				<div class="form-group margin-top-15 text-right">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
   				<button type="submit" name="save" class="btn btn-primary">Update</button>
				</div>

         </form>
      </div>
   </div>
</div>
<!-- END DASHBOARD CONTENT -->
<div class="ks-scrollable"></div>
@endsection

@section('footer')

@endsection
