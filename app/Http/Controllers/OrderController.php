<?php

namespace App\Http\Controllers;

use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseUser;

class OrderController extends Controller
{

    private $MenuController;

    public function __construct(){
        $this->MenuController = new MenuController();
    }

    public function index(Request $request, $page = 1)
    {


        if ($request->get('filters')) {
            return $this->filter($request->get('filters'), $page);

        }

        $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
        $childmenus = $this->MenuController->getAllChildMenu();

        $limit = 20;
        $query = new ParseQuery("UserOrder");
        $numberOfOrder = $query->count();
        $totalPage = ceil($numberOfOrder/$limit);

        $previous = 1;
        if($page>1){
            $previous = $page - 1;
        }
        if($page < ($totalPage-1)){
            $next = $page + 1;
        }else{
            $next = ($totalPage-1);
        }
        $query=new ParseQuery('UserOrder');
        $query->ascending("firstName");
        $query->includeKey("user");
        $query->limit($limit);
        $query->skip($limit * ($page-1));
        $results = $query->find();
        return view(
            'order.index',
            array(
                'results'=>$results,
                'totalPage'=>$totalPage,
                'currentPage'=>$page,
                'previous'=>$previous,
                'next'=>$next,
                'menus'=>$menus,
                'childmenus'=>$childmenus
            )
        );
    }

    public function edit($id)
    {
        $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
        $childmenus = $this->MenuController->getAllChildMenu();

        $userOrder = new ParseObject("UserOrder", $id);
        $query = new ParseQuery("UserOrderDetail");
        $query->includeKey(["product", "userOrder", "userOrder.user"]);
        $query->equalTo("userOrder", $userOrder);

        $result = $query->find();

        return view(
            'order.edit',
            array(
                'result'=>$result,
                'menus'=>$menus,
                'childmenus'=>$childmenus
            )
        );
    }

    public function update(Request $request, $id)
    {
            $order = new ParseObject("UserOrder",$id);
            $order->set("status", $request->get('status'));
            $order->save();
        return redirect()->back();
    }

    public function show($id){

        $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
        $childmenus = $this->MenuController->getAllChildMenu();


        $userOrder = new ParseObject("UserOrder", $id);

        $order = new ParseQuery('UserOrderDetail');
        $order->includeKey(["product", "userOrder", "userOrder.user"]);
        $order->equalTo("userOrder", $userOrder);

        $result = $order -> find();

        return view(
          'order.show',
            array(
                'result'=>$result,
                'menus'=>$menus,
                'childmenus'=>$childmenus
            )
        );
    }



    public function destroy($id)
    {
        try {
            $order = new ParseObject("UserOrderDetail", $id);
            $order->destroy(true);
            return redirect()->back();
        } catch (ParseException $ex) {
            echo 'Can not delete this Order: ' + $ex->getMessage();
        }
    }

    function saveOrder(Request $request, $id=null){
        if ($id) {
            $order = new ParseObject("UserOrder",$id);
            $status=$request->get('status');
            $order->set("status", $status);
            $order->save();
            return redirect()->back();
        }
    }

    /**
     * @param Request $request
     * @param int $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function filter($filters, $page=1){
        $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
        $childmenus = $this->MenuController->getAllChildMenu();

        $date= isset($filters['date']) ? $filters['date'] : null;
        $status= isset($filters['status']) ? $filters['status'] : null;
        $startDate=new \DateTime($date);
        $startDate->format('Y-m-d H:i:s');
        $startDate->setTime(0,0,0);
        $endDate=new \DateTime($date);
        $endDate->format('Y-m-d H:i:s');
        $endDate->setTime(23,59,59);
        $limit = 2;


        if($date==null){    //===FILTER  STATUS===
            $countQuery=new ParseQuery("UserOrder");
            $countQuery->includeKey("user");
            $countQuery->equalTo("status", $status);


            $numberOfOrder = $countQuery->count();
            $totalPage = ceil($numberOfOrder/$limit);

            $previous = 1;
            if($page>1){
                $previous = $page - 1;
            }
            if($page < ($totalPage-1)){
                $next = $page + 1;
            }else{
                $next = ($totalPage-1);
            }
            $query=new ParseQuery("UserOrder");
            $query->includeKey("user");
            $query->equalTo("status", $status);
            $query->limit($limit);
            $query->skip($limit * ($page-1));
            $results=$query->find();

            return view(
                'order.index',
                array(
                    'results'=>$results,
                    'totalPage'=>$totalPage,
                    'currentPage'=>$page,
                    'previous'=>$previous,
                    'filters' => '?filters[status]=' . $status,
                    'next'=>$next,
                    'menus'=>$menus,
                    'childmenus'=>$childmenus
                )
            );
        }
        else if($status==null){   //===FILTER  DATE===
            $filtersStr = "?filters[date]=" . $date;
            $countQuery=new ParseQuery("UserOrder");
            $countQuery->includeKey("user");
            $countQuery->greaterThanOrEqualTo("createdAt", $startDate);
            $countQuery->lessThanOrEqualTo("createdAt", $endDate);

            $numberOfOrder = $countQuery->count();
            $totalPage = ceil($numberOfOrder/$limit);
            $previous = 1;
            if($page>1){
                $previous = $page - 1;
            }
            if($page < ($totalPage-1)){
                $next = $page + 1;
            }else{
                $next = ($totalPage-1);
            }

            $query=new ParseQuery("UserOrder");
            $query->includeKey("user");
            $query->greaterThanOrEqualTo("createdAt", $startDate);
            $query->lessThanOrEqualTo("createdAt", $endDate);
            $query->limit($limit);
            $query->skip($limit * ($page-1));
            $results=$query->find();

            return view(
                'order.index',
                array(
                    'results'=>$results,
                    'totalPage'=>$totalPage,
                    'currentPage'=>$page,
                    'previous'=>$previous,
                    'next'=>$next,
                    'filters' => "?filters[date]=" . $date,
                    'menus'=>$menus,
                    'childmenus'=>$childmenus
                )
            );
        }
        else{         //===FILTER  STATUS AND DATE===
            $countQuery=new ParseQuery("UserOrder");
            $countQuery->includeKey("user");
            $countQuery->greaterThanOrEqualTo("createdAt",$startDate);
            $countQuery->lessThanOrEqualTo("createdAt", $endDate);
            $countQuery->equalTo("status", $status);

            $numberOfOrder = $countQuery->count();
            $totalPage = ceil($numberOfOrder/$limit);
            $previous = 1;
            if($page>1){
                $previous = $page - 1;
            }
            if($page < ($totalPage-1)){
                $next = $page + 1;
            }else{
                $next = ($totalPage-1);
            }


            $query=new ParseQuery("UserOrder");
            $query->includeKey("user");
            $query->greaterThanOrEqualTo("createdAt",$startDate);
            $query->lessThanOrEqualTo("createdAt", $endDate);
            $query->equalTo("status", $status);
            $query->limit($limit);
            $query->skip($page*($page-1));
            $results=$query->find();

            return view(
                'order.index',
                array(
                    'results'=>$results,
                    'totalPage'=>$totalPage,
                    'currentPage'=>$page,
                    'previous'=>$previous,
                    'next'=>$next,
                    'filters' => "?filters[date]=" . $date . "&filters[status]=" . $status,
                    'menus'=>$menus,
                    'childmenus'=>$childmenus
                )
            );
        }
    }
}
