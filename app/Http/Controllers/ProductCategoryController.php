<?php
namespace App\Http\Controllers;
use DateTime;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\LibHelper;
use App\UdLib\MenuObject;
use App\UdLib\MenuObjectList;
use App\Http\Requests;
use Session;

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Closure;

class ProductCategoryController extends Controller
{
   private $MenuController;

   public function __construct(){
      $this->MenuController = new MenuController();
   }

   public function index(){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      // $shopCategory = new ParseQuery("ProductCategory");
      // $shop = new ParseQuery("Shop");

      $query = new ParseQuery("ProductCategory");
      //$query->matchesQuery("Shop")
      //$query->include()
      $results = $query->find();


      return view(
         'productcategory.index',
         array(
            'results'=>$results,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function create(){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      return view(
         'productcategory.create',
         array(
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function store(Request $request){
      return $this->saveProductCategory($request);
   }

   public function edit($id){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $shopCategory = $this->getProductCategory($id);
      return view(
         'productcategory.edit',
         array(
            'productCategory'=>$shopCategory,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function update(Request $request, $id){
      return $this->saveProductCategory($request, $id);
   }

   public function destroy($id){
      $this->deleteProductByCategory($id);
      $this->deleteProductCategory($id);

      return redirect()->back();
   }

   ////////////////////////////////////////
   //function working on Data
   //////////////////////////////////////
   function saveProductCategory(Request $request, $id=null){
      if ($id) {
         // update on previos shopcategory record

         $productcategory = new ParseObject("ProductCategory", $id);
         $productcategory->set("title", $request->get('title'));

         if ($_FILES["icon"]["name"]) {
            $photoName = $_FILES["icon"]["name"];
            $photo = ParseFile::createFromData(file_get_contents( $_FILES['icon']['tmp_name'] ), $photoName);
            $productcategory->set("icon", $photo);
         }
         
         $productcategory->save();
         return redirect()->back();
      }else{
         // add new shopcategory record

         $productcategory = new ParseObject("ProductCategory");
         $productcategory->set("title", $request->get('title'));
         if ($_FILES["icon"]["name"]) {
            $photoName = $_FILES["icon"]["name"];
            $photo = ParseFile::createFromData(file_get_contents( $_FILES['icon']['tmp_name'] ), $photoName);
            $productcategory->set("icon", $photo);
         }
         
         $productcategory->save();

         return redirect('productcategory');
      }
   }

   // get shop category by objectid
   function getProductCategory($id){
      $query = new ParseQuery("ProductCategory");
      $query->equalTo("objectId", $id);
      return $query->first();
   }

   //delete shopcategory
   function deleteProductCategory($id){
      try {
         $query = new ParseQuery("ProductCategory");
			$productcategory=$query->get($id);
			$productcategory->destroy();
      } catch (ParseException $ex) {
			echo 'Can not delete this ShopCategory: ' + $ex->getMessage();
		}
   }

   //delete shops by categoryid
   function deleteProductByCategory($cid){
      $category = new ParseObject("ProductCategory", $cid);

      try {
         $shops = new ParseQuery("Product");
			$shops->equalTo("category",$category);
			$results = $shops->find();
         foreach($results as $result){
            $query = new ParseQuery("Shop");
            $shop=$query->get($result->getObjectId());
            $shop->destroy();
         }
      } catch (ParseException $ex) {
			echo 'Can not delete this Shop: ' + $ex->getMessage();
		}
   }

   //get shops by categoryid
   function hasProduct($cid){
      $category = new ParseObject("ProductCategory", $cid);

      $shops = new ParseQuery("Product");
      $shops->equalTo("category",$category);
      $results = $shops->find();
      return count($results);
   }
}
