<?php
namespace App\Http\Controllers;
use DateTime;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\LibHelper;
use App\UdLib\MenuObject;
use App\UdLib\MenuObjectList;
use App\Http\Requests;
use Session;

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Closure;

class PromotionController extends Controller
{
   private $MenuController;

   public function __construct(){
      $this->MenuController = new MenuController();
   }

   public function index($page = 1){
      $search = isset($_GET['search'])?$_GET['search']:"";

      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $limit = 20;
      $countQuery = new ParseQuery("Promotion");
      $numberOfPromotion = $countQuery->count();
      $totalPage = ceil($numberOfPromotion/$limit);

      $previous = 1;
      if($page>1){
         $previous = $page - 1;
      }
      if($page < ($totalPage-1)){
         $next = $page + 1;
      }else{
         $next = ($totalPage-1);
      }

      $query = new ParseQuery("Promotion");
      if($search){
         $query->containString("title" , $search);
      }

      $query->ascending("order");
      $query->includeKey("shop");
      $query->limit($limit);
      $query->skip($limit * ($page-1));
      $results = $query->find();

      return view(
         'promotion.index',
         array(
            'results'=>$results,
            'search'=>$search,
            'totalPage'=>$totalPage,
            'currentPage'=>$page,
            'previous'=>$previous,
            'next'=>$next,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function create(){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();
      $query = new ParseQuery("Shop");
      $query->equalTo("status", boolval(1));
      $query->limit(10000);
		$shops = $query->find();

      return view(
         'promotion.create',
         array(
            'shops'=>$shops,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function store(Request $request){
      $shopid = new ParseObject("Shop",$request->get('shop'));
      $available = new DateTime($request->get('available'));
      $expired = new DateTime($request->get('expired'));

      $promotion = new ParseObject("Promotion");
      $promotion->set("order", intval($request->get('order')));
      $promotion->set("title", $request->get('title'));
      $promotion->set("shop", $shopid);
      $promotion->set("shopId", $request->get('shop'));
      $promotion->set("available", $available);
      $promotion->set("expired", $expired);
      $promotion->set("video", $request->get('video'));
      $promotion->set("status", boolval($request->get('status')));

      if ($_FILES["thumb"]["name"]) {
         $thumbName = $_FILES["thumb"]["name"];
         $thumb = ParseFile::createFromData(file_get_contents( $_FILES['thumb']['tmp_name'] ), $thumbName);
         $promotion->set("thumb", $thumb);
      }

      if ($_FILES["fullBanner"]["name"]) {
         $fullBannerName = $_FILES["fullBanner"]["name"];
         $fullBanner = ParseFile::createFromData(file_get_contents( $_FILES['fullBanner']['tmp_name'] ), $fullBannerName);
         $promotion->set("fullBanner", $fullBanner);
      }

		$promotion->save();

      if($request->get('push')){
         $queryPush = new ParseQuery("PushNotification");
   		$pushes = $queryPush->find();
         foreach($pushes as $push){
            $push = ParseCloud::run("pushnotification", array("playerId" => $push->get('playerId'), "message"=>$request->get('title')));
         }
      }



      return redirect('promotion');
   }

   public function edit($id){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $query = new ParseQuery("Promotion");
   	$query->includeKey("shop");
   	$query->equalTo("objectId", $id);
   	$results = $query->first();

      $queryShops = new ParseQuery("Shop");
		$shops = $queryShops->find();

      return view(
         'promotion.edit',
         array(
            'result'=>$results,
            'shops'=>$shops,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function update(Request $request, $id){
      $shopid = new ParseObject("Shop",$request->get('shop'));
      $available = new DateTime($request->get('available'));
      $expired = new DateTime($request->get('expired'));

      $query = new ParseQuery("Promotion");
   	$query->equalTo("objectId", $id);
   	$edit = $query->first();

      $edit->set("order", intval($request->get('order')));
      $edit->set("title", $request->get('title'));
      $edit->set("shop", $shopid);
      $edit->set("shopId", $request->get('shop'));
      $edit->set("available", $available);
      $edit->set("expired", $expired);
      $edit->set("video", $request->get('video'));
      $edit->set("status", boolval($request->get('status')));
      $edit->save();

      return redirect()->back();
   }

   public function destroy($id){
      try {
         $query = new ParseQuery("Promotion");
			$promotion=$query->get($id);
			$promotion->destroy();

         return redirect('promotion');
      } catch (ParseException $ex) {
			echo 'Can not delete this promotion: ' + $ex->getMessage();
		}
   }

   public function image($id){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $query = new ParseQuery("Promotion");
   	$query->equalTo("objectId", $id);
   	$results = $query->first();

      return view(
         'promotion.image',
         array(
            'result'=>$results,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function uploadthumb(Request $request, $id){
      $thumb = new ParseObject("Promotion", $id);
      if ($_FILES["thumb"]["name"]) {
         $filename = $_FILES["thumb"]["name"];
         $file = ParseFile::createFromData(file_get_contents( $_FILES['thumb']['tmp_name'] ), $filename);
         $thumb->set("thumb", $file);
      }
      $thumb->save();

      return redirect()->back();
   }

   public function uploadbanner(Request $request, $id){
      $banner = new ParseObject("Promotion", $id);
      if ($_FILES["fullBanner"]["name"]) {
         $filename = $_FILES["fullBanner"]["name"];
         $file = ParseFile::createFromData(file_get_contents( $_FILES['fullBanner']['tmp_name'] ), $filename);
         $banner->set("fullBanner", $file);
      }
      $banner->save();

      return redirect()->back();
   }
}
