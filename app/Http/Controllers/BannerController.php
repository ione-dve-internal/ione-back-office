<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Parse\ParseFile;
use Parse\ParseObject;
use Parse\ParseQuery;

class BannerController extends Controller
{
    private $MenuController;

    public function __construct(){
        $this->MenuController = new MenuController();
    }

    public function index($page = 1)
    {
        $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
        $childmenus = $this->MenuController->getAllChildMenu();

        $limit = 20;
        $query = new ParseQuery("SlideShow");

        $numberOfBanner = $query->count();
        $totalPage = ceil($numberOfBanner/$limit);

        $previous = 1;
        if($page>1){
            $previous = $page - 1;
        }
        if($page < ($totalPage-1)){
            $next = $page + 1;
        }else{
            $next = ($totalPage-1);
        }

        $query = new ParseQuery("SlideShow");

        $query->descending("image");
        $query->includeKey("product");

        $query->limit($limit);
        $query->skip($limit * ($page-1));
        $results = $query->find();
        return view(
            'banner.index',
            array(
                'results'=>$results,
                'totalPage'=>$totalPage,
                'currentPage'=>$page,
                'previous'=>$previous,
                'next'=>$next,
                'menus'=>$menus,
                'childmenus'=>$childmenus
            )
        );
    }


    public function create()
    {
        $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
        $childmenus = $this->MenuController->getAllChildMenu();

        $query = new ParseQuery("Product");
        $products = $query->find();
        $productIds = array_map(function($product){
            return $product->getObjectId();
        }, $products);
        $productNames = array_map(function($product) {
            return $product->get('name');
        }, $products);

        return view(
            'banner.create',
            array(
                'productIds'=>implode(",", $productIds),
                'productNames'=>implode(",", $productNames),
                'pro'=>$products,
                'menus'=>$menus,
                'childmenus'=>$childmenus
            )
        );
    }


    public function store(Request $request)
    {
        $pro = $request->get('productId');
        $product = new ParseObject("Product",$pro);

        $banner = new ParseObject("SlideShow");

        $banner->set("product", $product);
        $banner->set("status", boolval($request->get('status')));
        if ($_FILES["image"]["name"]){
            $filename=$_FILES["image"]["name"];
            $file=ParseFile::createFromData(file_get_contents($_FILES["image"]["tmp_name"]), $filename);
            $banner->set("image", $file);
        }
        $banner->save();
        return redirect('banner');
    }

    public function update(Request $request, $id)
    {
        return $this->saveBanner($request, $id);
    }


    public function destroy($id)
    {
        try {
            $banner = new ParseQuery("SlideShow");
            $result=$banner->get($id);
            $result->destroy();
        } catch (ParseException $ex) {
            echo 'Can not delete this banner: ' + $ex->getMessage();
        }
        return redirect()->back();
    }

    public function uploadbanner(Request $request, $id){
        $image = new ParseObject("SlideShow", $id);
        if ($_FILES["image"]["name"]) {
            $filename = $_FILES["image"]["name"];
            $file = ParseFile::createFromData(file_get_contents( $_FILES['image']['tmp_name'] ), $filename);
            $image->set("image", $file);
        }
        $image->save();
        return redirect()->back();
    }


    public function image($id){
        $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
        $childmenus = $this->MenuController->getAllChildMenu();

        $query = new ParseQuery("SlideShow");
        $query->equalTo("objectId", $id);
        $result = $query->first();

        return view(
            'banner.image',
            array(
                'result'=>$result,
                'menus'=>$menus,
                'childmenus'=>$childmenus
            )
        );
    }

}

