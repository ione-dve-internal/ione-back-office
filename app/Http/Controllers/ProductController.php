<?php

namespace App\Http\Controllers;

use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Parse\ParseFile;
use Parse\ParseObject;
use Parse\ParseQuery;

class ProductController extends Controller
{
    private $MenuController;

    public function __construct()
    {
        $this->MenuController = new MenuController();
    }

    public function index($page = 1)
    {
        $search = isset($_GET['search']) ? $_GET['search'] : "";

        $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
        $childmenus = $this->MenuController->getAllChildMenu();

        $limit = 20;
        $countQuery = new ParseQuery("Product");
        $numberOfShop = $countQuery->count();
        $totalPage = ceil($numberOfShop / $limit);

        $previous = 1;
        if ($page > 1) {
            $previous = $page - 1;
        }
        if ($page < ($totalPage - 1)) {
            $next = $page + 1;
        } else {
            $next = ($totalPage - 1);
        }

        $query1 = new ParseQuery("Product");
        if ($search) {
            $query1->containString("code", $search);
        }

        $query2 = new ParseQuery("Product");
        if ($search) {
            $query2->containString("name", $search);
        }

        $query = ParseQuery::orQueries([$query1, $query2]);
        $query->descending("code");
        $query->includeKey("category");
        $query->limit($limit);
        $query->skip($limit * ($page - 1));
        $results = $query->find();
         return view(
                'product.index',
                array(
                    'results' => $results,
                    'search' => $search,
                    'totalPage' => $totalPage,
                    'currentPage' => $page,
                    'previous' => $previous,
                    'next' => $next,
                    'menus' => $menus,
                    'childmenus' => $childmenus
                )
            );
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
        $childmenus = $this->MenuController->getAllChildMenu();

        $query = new ParseQuery("ProductCategory");
        $q= new ParseQuery("Product");
        $productcategories = $query->find();
        $products = $q->find();
        return view(
            'product.create',
            array(
                'productcategories' => $productcategories,
                'products' => $products,
                'menus' => $menus,
                'childmenus' => $childmenus

            )
        );
    }

    public function store(Request $request)
    {

        return $this->saveProduct($request);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
        $childmenus = $this->MenuController->getAllChildMenu();

        $query = new ParseQuery("Product");
        $query->includeKey("category");
        $query->equalTo("objectId", $id);
        $result = $query->first();

        $queryCategory = new ParseQuery("ProductCategory");
        $productcategories = $queryCategory->find();
        $products = new ParseQuery("Product");
        $products = $products->find();

        // EDIT ADDRELATED PRO
        $productIds = array_map(function($product){
            return $product->getObjectId();
        }, $products);

        $productNames = array_map(function($product) {
            return $product->get('name');
        }, $products);

        return view(
            'product.edit',
            array(

                'productIds'=>implode(",", $productIds),
                'productNames'=>implode(",", $productNames),
                'result' => $result,
                'productcategories' => $productcategories,
                'products' => $products,
                'menus' => $menus,
                'childmenus' => $childmenus

            )
        );
    }

    public function update(Request $request, $id)
    {

        return $this->saveProduct($request, $id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        // update status only
        $offer = new ParseObject("Product", $id);
        $offer->set("status", boolval(0));
        $offer->save();

        return redirect()->back();

    }

    //image
    public function image($id)
    {

        $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
        $childmenus = $this->MenuController->getAllChildMenu();
        $pro = new ParseQuery('Product');
        $pro->equalTo('objectId',$id);
        $result = $pro->first();

        $queryProduct = new ParseObject("Product",$id);
        $queryImg= new ParseQuery('ProductImage');
        $queryImg->includeKey('product');

        $queryImg->equalTo('product',$queryProduct);
        $results = $queryImg->find();
        return view(
            'product.image',
            array(
                'result'=>$result,
                'imageId'=>$id,
                'results'=>$results,
                'menus'=>$menus,
                'childmenus'=>$childmenus

            )
        );
    }

    public function uploadphoto(Request $request, $id){
        $photo = new ParseObject("ProductImage");
        if ($_FILES["photo"]["name"]) {

            $filename = $_FILES["photo"]["name"];
            $file =ParseFile::createFromData(file_get_contents( $_FILES['photo']['tmp_name'] ), $filename);
            $photo->set("image", $file);

        }

        $post = new ParseObject("Product",$id);
        $photo->set("product", $post);
        $photo->save();
        return redirect()->back();

    }
    public function deleteimage($id){

        try {
            $order = new ParseObject("ProductImage", $id);
            $order->destroy(true);
            return redirect()->back();
        } catch (ParseException $ex) {
            echo 'Can not delete this Order: ' + $ex->getMessage();
        }
    }

////////////////////////////////////////
//function working on Data
////////////////////////////////////////
    function saveProduct(Request $request, $id = null)
    {
        if ($id)
        {

            // update on previos shop record

            $category = new ParseObject("ProductCategory", $request->get('category'));
            $product = new ParseObject("Product", $id);
            $related=$request->get("related");
            $color=$request->get('color');
            $size=$request->get('size');

            $product->set("category", $category);
            $product->set("code", $request->get('code'));
            $product->set("name", $request->get('name'));
            $p = $request->get('price');
            $price = (float)$p;
            $product->set("price", $price);
            $product->set("desc", $request->get('desc'));
            $b = $request->get('hot');
            $hot = (boolean)$b;
            $product->set("hot", $hot);
            $r = $request->get('recommend');
            $recommend = (boolean)$r;
            $product->set("recommend", $recommend);
            $product->set("status", boolval($request->get('status')));

//            EDIT WHEN VALUE UNDEFINE
            if (isset($related)) {
                $relatedArray=explode(",",$related);
                $product->setArray("related",$relatedArray);
            }
            if (isset($color)) {
                $arrColor=explode(",",$color);
                $product->setArray("color",$arrColor);
            }
            if (isset($size)) {
                $arrSize = explode(",", $size);
                $product->setArray("size", $arrSize);
            }
//            END EDIT WHEN VALUE UNDEFINE
                    $product->save();
                    return redirect()->back();

        } else
            {
                // add new shopcategory record
                $category = new ParseObject("ProductCategory", $request->get('category'));
                $product = new ParseObject("Product", $id);
                $related=$request->get("related");
                $color=$request->get('color');
                $size=$request->get('size');

            $product->set("category", $category);
            $product->set("code", $request->get('code'));
            $product->set("name", $request->get('name'));
            $p = $request->get('price');
            $price = (float)$p;
            $product->set("price", $price);
            $product->set("desc", $request->get('desc'));
            $b = $request->get('hot');
            $hot = (boolean)$b;
            $product->set("hot", $hot);
            $r = $request->get('recommend');
            $recommend = (boolean)$r;
            $product->set("recommend", $recommend);
            $product->set("status", boolval($request->get('status')));

//                CREATE WHEN VALUE UNDEFINE
            if (isset($related)) {
                $relatedArray=explode(",",$related);
                $product->setArray("related",$relatedArray);
            }
            if (isset($color)) {
                $arrColor=explode(",",$color);
                $product->setArray("color",$arrColor);
            }
            if (isset($size)) {
                $arrSize = explode(",", $size);
                $product->setArray("size", $arrSize);
            }
//            END CREATE WHEN VALUE UNDEFINE

        }
                 $product->save();
                 return redirect('product-management');

    }

//    SHOW STRING IN RELATED ARRAY
    function related($id)
    {

        $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
        $childmenus = $this->MenuController->getAllChildMenu();
        $productQuery = new ParseQuery('Product');
        $product = $productQuery->get($id);
        $relatedProductIds = (array)$product->get('related');

        if (count($relatedProductIds) > 0) {
            $query = new ParseQuery("Product");
            $query->containedIn("objectId", $relatedProductIds);
            $results = $query->find();
            return view('product.related',
                array(
                    'product'=>$product,
                    'results'=>$results,
                    'menus'=>$menus,
                    'childmenus'=>$childmenus
                ));

        }

        else {

            echo "No Related Include ";

        }

    }

    function addrelated($id)
    {

        $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
        $childmenus = $this->MenuController->getAllChildMenu();
        $queryProduct = new ParseQuery("Product");
        $queryProduct->equalTo("objectId", $id);
        $product=$queryProduct->first();

        $query = new ParseQuery("Product");
        $products = $query->find();

        $productIds = array_map(function($product){
            return $product->getObjectId();
        }, $products);

        $productNames = array_map(function($product) {
            return $product->get('name');
        }, $products);


        if(!empty($product->get('related'))){
            $relatedProductIds = $product->get('related');
            return view('product.addrelated',
                array(
                    'productIds'=>implode(",", $productIds),
                    'productNames'=>implode(",", $productNames),
                    'relatedProductIds'=>implode(",", $relatedProductIds),
                    'product'=> $product,
                    'productId' => $id,
                    'menus'=>$menus,
                    'childmenus'=>$childmenus
                ));
        }else{
            return view('product.addrelated',
                array(
                    'productIds'=>implode(",", $productIds),
                    'productNames'=>implode(",", $productNames),
                    'product'=> $product,
                    'productId' => $id,
                    'menus'=>$menus,
                    'childmenus'=>$childmenus
                ));
        }


    }

     //   SAVE ADDRELATED PRODUCT
    public function saveRelated(Request $request,$id)
    {

        $r=$request->get("related");
        $related=explode(",",$r);
        $product = new ParseObject("Product", $id);
        $product->setArray("related", $related);
        $product->save();

        return redirect()->back();

    }

    public function show($id)
    {

        $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
        $childmenus = $this->MenuController->getAllChildMenu();
        $query=new ParseQuery("Product");
        $query->includeKey("category");
        $query->equalTo("objectId", $id);
        $result=$query->first();

        return view('product.show',
            array(
                'result'=>$result,
                'menus'=>$menus,
                'childmenus'=>$childmenus
            ));

    }

}


