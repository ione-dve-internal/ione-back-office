<?php
namespace App\Http\Controllers;
use DateTime;
use Date;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\LibHelper;
use App\UdLib\MenuObject;
use App\UdLib\MenuObjectList;
use App\Http\Requests;
use Session;
use Maatwebsite\Excel\Facades\Excel;

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Closure;

class OfferController extends Controller
{
   private $MenuController;

   public function __construct(){
      $this->MenuController = new MenuController();
   }

   public function index($page = 1){
      $search = isset($_GET['search'])?$_GET['search']:"";

      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $limit = 20;
      $countQuery = new ParseQuery("Offer");
      $numberOfOffer = $countQuery->count();
      $totalPage = ceil($numberOfOffer/$limit);

      $previous = 1;
      if($page>1){
         $previous = $page - 1;
      }
      if($page < ($totalPage-1)){
         $next = $page + 1;
      }else{
         $next = ($totalPage);
      }

      $query = new ParseQuery("Offer");
      if($search){
         $query->containString("title" , $search);
      }
      $query->includeKey("shop");
      $query->limit($limit);
      $query->skip($limit * ($page-1));
      $query->ascending('title');
      $results = $query->find();

      return view(
         'offer.index',
         array(
            'results'=>$results,
            'search'=>$search,
            'totalPage'=>$totalPage,
            'currentPage'=>$page,
            'previous'=>$previous,
            'next'=>$next,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function create(){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $queryShop = new ParseQuery("Shop");
      $queryShop->limit(999);
		$shops = $queryShop->find();

      $queryCustomerType = new ParseQuery("CustomerType");
		$customerTypes = $queryCustomerType->find();

      return view(
         'offer.create',
         array(
            'shops'=>$shops,
            'customerTypes'=>$customerTypes,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function store(Request $request){
      return $this->saveOffer($request);
   }

   public function edit($id){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $query = new ParseQuery("Offer");
   	$query->includeKey("shop");
      $query->includeKey("customerType");
   	$query->equalTo("objectId", $id);
   	$results = $query->first();

      $queryShop = new ParseQuery("Shop");
      $queryShop->ascending('name');
    $queryShop->limit(10000);
		$shops = $queryShop->find();

      $queryCustomerType = new ParseQuery("CustomerType");
		$customerTypes = $queryCustomerType->find();

      return view(
         'offer.edit',
         array(
            'result'=>$results,
            'shops'=>$shops,
            'customerTypes'=>$customerTypes,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function update(Request $request, $id){
      return $this->saveOffer($request, $id);
   }

   public function destroy($id){
      // update status only
      $offer = new ParseObject("Offer", $id);
      $offer->set("status", boolval(0));
      $offer->save();

      return redirect()->back();
   }

   public function image($id){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $query = new ParseQuery("Offer");
   	$query->equalTo("objectId", $id);
   	$results = $query->first();

      return view(
         'offer.image',
         array(
            'result'=>$results,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function uploadthumb(Request $request, $id){
      $thumb = new ParseObject("Offer", $id);
      if ($_FILES["thumb"]["name"]) {
         $filename = $_FILES["thumb"]["name"];
         $file = ParseFile::createFromData(file_get_contents( $_FILES['thumb']['tmp_name'] ), $filename);
         $thumb->set("thumb", $file);
      }
      $thumb->save();

      return redirect()->back();
   }

   public function uploadbanner(Request $request, $id){
      $banner = new ParseObject("Offer", $id);
      if ($_FILES["fullBanner"]["name"]) {
         $filename = $_FILES["fullBanner"]["name"];
         $file = ParseFile::createFromData(file_get_contents( $_FILES['fullBanner']['tmp_name'] ), $filename);
         $banner->set("fullBanner", $file);
      }
      $banner->save();

      return redirect()->back();
   }

   //Coupon
   public function coupon($page = 1){
      $search = isset($_GET['search'])?$_GET['search']:"";

      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $limit = 20;
      $countQuery = new ParseQuery("UserCoupon");
      $numberOfCoupon = $countQuery->count();
      $totalPage = ceil($numberOfCoupon/$limit);

      $previous = 1;
      if($page>1){
         $previous = $page - 1;
      }
      if($page < ($totalPage-1)){
         $next = $page + 1;
      }else{
         $next = ($totalPage);
      }

      $query = new ParseQuery("UserCoupon");
      $query->includeKey("user");
      $query->includeKey("coupon");
      $query->limit($limit);
      $query->skip($limit * ($page-1));
      $results = $query->find();

      return view(
         'offer.coupon',
         array(
            'results'=>$results,
            'menus'=>$menus,
            'childmenus'=>$childmenus,
            'search'=>$search,
            'totalPage'=>$totalPage,
            'currentPage'=>$page,
            'previous'=>$previous,
            'next'=>$next,
         )
      );
   }

   public function couponExport(){
        ini_set('memory_limit', '1024M');
      $start = isset($_GET['start'])?new DateTime($_GET['start'].'T00:00:00.000Z'):"";
      $end = isset($_GET['end'])?new DateTime($_GET['end'].'T23:59:59.000Z'):"";

      $query = new ParseQuery("UserCoupon");
      $query->includeKey("user");
      $query->includeKey("coupon");
      if($start && $end){
         $query->greaterThan("createdAt",$start);
         $query->lessThan("createdAt",$end);
      }
      $query->limit(999);
      $results = $query->find();
      $coupons = array();
      foreach ($results as $value) {
         $coupons[] = array(
            "User Name"=>$value->get('user')?$value->get('user')->get('firstName').' '.$value->get('user')->get('lastName'):'--',
            "Coupon"=>$value->get('coupon')?$value->get('coupon')->get('title'):'--',
            "Valid Amount"=>$value->get('validAmount'),
            "Created Date"=>$value->getCreatedAt()->format('d-M-Y'),
         );
      }

      Excel::create('UserCoupon', function($excel)use($coupons){
         $excel->sheet('ExportFile', function($sheet)use($coupons){
            $sheet->fromArray($coupons);
         });
      })->export('xlsx');
   }

   //Redeem
   public function redeem($page = 1){
      $search = isset($_GET['search'])?$_GET['search']:"";

      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $limit = 20;
      $countQuery = new ParseQuery("UserRedeem");
      $numberOfRedeem = $countQuery->count();
      $totalPage = ceil($numberOfRedeem/$limit);

      $previous = 1;
      if($page>1){
         $previous = $page - 1;
      }
      if($page < ($totalPage-1)){
         $next = $page + 1;
      }else{
         $next = ($totalPage);
      }

      $query = new ParseQuery("UserRedeem");

      $query->includeKey("user");
      $query->includeKey("offer");
      $query->limit($limit);
      $query->skip($limit * ($page-1));
      $results = $query->find();

      return view(
         'offer.redeem',
         array(
            'results'=>$results,
            'menus'=>$menus,
            'childmenus'=>$childmenus,
            'search'=>$search,
            'totalPage'=>$totalPage,
            'currentPage'=>$page,
            'previous'=>$previous,
            'next'=>$next,
         )
      );
   }

   public function redeemExport(){
      $start = isset($_GET['start'])?new DateTime($_GET['start'].'T00:00:00.000Z'):"";
      $end = isset($_GET['end'])?new DateTime($_GET['end'].'T23:59:59.000Z'):"";

      $results = array();
      $limit = 1000;
      $index = 0;

      do {
         $tmparr = $this->getUserRedeemFrom($start, $end, $index, $limit);
         $results = array_merge($results, $tmparr);
         $index = $index + $limit;
      } while (count($tmparr) == $limit);

      $redeems = array();
      foreach ($results as $value) {
         if ($value) {
            $username = "";
            if ($value->has('user')) {
               if ($value->get('user')->get('firstName')) {
                  $username = $value->get('user')->get('firstName');
               }
               if ($value->get('user')->get('lastName')) {
                  $username = $username.' '.$value->get('user')->get('lastName');
               }
            }

            $offerName = "";
            if ($value->has('offer')) {
               $offerName = $value->get('offer')->get('title');
            }

            $redeems[] = array(
               "User Name"=>$username,
               "Offer Name"=>$offerName,
               "Created Date"=>$value->getCreatedAt()->format('d-M-Y'),
            );
         }
      }

      Excel::create('UserRedeem', function($excel)use($redeems){
         $excel->sheet('ExportFile', function($sheet)use($redeems){
            $sheet->fromArray($redeems);
         });
      })->export('xlsx');
   }


   ////////////////////////////////////////
   //function working on data flow
   ///////////////////////////////////////
   function saveOffer(Request $request, $id=null){
      if ($id) {
         // update record
         $shop = new ParseObject("Shop",$request->get('shop'));
         $customerType = new ParseObject("CustomerType",$request->get('customerType'));
         $available = new DateTime($request->get('available'));
         $expired = new DateTime($request->get('expired'));

         $offer = new ParseObject("Offer", $id);
         $offer->set("title", $request->get('title'));
         $offer->set("amount", intval($request->get('amount')));
         $offer->set("instock", intval($request->get('instock')));
         $offer->set("shop", $shop);
         $offer->set("customerType", $customerType);
         $offer->set("available", $available);
         $offer->set("expired", $expired);
         $offer->set("status", boolval($request->get('status')));
         $offer->save();

         return redirect()->back();
      }else{
         // add new record
         $shop = new ParseObject("Shop",$request->get('shop'));
         $customerType = new ParseObject("CustomerType",$request->get('customerType'));
         $available = new DateTime($request->get('available'));
         $expired = new DateTime($request->get('expired'));

         $offer = new ParseObject("Offer");
         $offer->set("title", $request->get('title'));
         $offer->set("amount", intval($request->get('amount')));
         $offer->set("instock", intval($request->get('instock')));
         $offer->set("shop", $shop);
         $offer->set("customerType", $customerType);
         $offer->set("available", $available);
         $offer->set("expired", $expired);
         $offer->set("status", boolval($request->get('status')));

         if ($_FILES["thumb"]["name"]) {
            $thumbName = $_FILES["thumb"]["name"];
            $thumb = ParseFile::createFromData(file_get_contents( $_FILES['thumb']['tmp_name'] ), $thumbName);
            $offer->set("thumb", $thumb);
         }

         if ($_FILES["fullBanner"]["name"]) {
            $fullBannerName = $_FILES["fullBanner"]["name"];
            $fullBanner = ParseFile::createFromData(file_get_contents( $_FILES['fullBanner']['tmp_name'] ), $fullBannerName);
            $offer->set("fullBanner", $fullBanner);
         }

   		$offer->save();

         if($request->get('push')){
            $queryPush = new ParseQuery("PushNotification");
      		$pushes = $queryPush->find();
            foreach($pushes as $push){
               $push = ParseCloud::run("pushnotification", array("playerId" => $push->get('playerId'), "message"=>$request->get('title')));
            }
         }

         return redirect('offer');
      }

   }

    // Get all record by pagination
    function getUserRedeemFrom($startDate, $endDate, $startIndex, $limit) {
        $query = new ParseQuery("UserRedeem");
        $query->select(["user.lastName", "user.firstName", "offer.title"]);

        if($startDate && $endDate){
            $query->greaterThan("createdAt",$startDate);
            $query->lessThan("createdAt",$endDate);
        }

        $query->skip($startIndex);
        $query->limit($limit);
        $results = $query->find();

        return $results;
    }
}
