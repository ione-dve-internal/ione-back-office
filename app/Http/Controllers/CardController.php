<?php
namespace App\Http\Controllers;
use DateTime;
use Date;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\LibHelper;
use App\UdLib\MenuObject;
use App\UdLib\MenuObjectList;
use App\Http\Requests;
use Parse\ParseSchema;
use Session;
use Maatwebsite\Excel\Facades\Excel;

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Closure;
use File;

class CardController extends Controller
{
   private $MenuController;

   public function __construct(){
      $this->MenuController = new MenuController();
   }

   public function index($page = 1){
      $filter = isset($_GET['filter'])?$_GET['filter']:"";
      $search = isset($_GET['search'])?$_GET['search']:"";
      $cols = isset($_GET['cols'])?$_GET['cols']:"";

      $startDate = "";
      $endDate = "";
      $start = "";
      $end = "";



      if(isset($_GET['start'])){
         $startDate = $_GET['start']!=""? $_GET['start']:"";
         $start = $_GET['start']!=""?new DateTime($_GET['start'].'T00:00:00.000Z'):"";
      }
      if(isset($_GET['start'])){
         $endDate = $_GET['end']!=""? $_GET['end']:"";
         $end = $_GET['end']!=""?new DateTime($_GET['end'].'T23:59:59.000Z'):"";
      }

      if($filter == 'activated'){
         $queryUser = new ParseQuery("_User");
         $queryUser->exists("opt");
      }elseif($filter == 'notactivate'){
         $queryUser = new ParseQuery("_User");
         $queryUser->doesNotExist("opt");
      }else{
         $queryUser = new ParseQuery("_User");
         $queryUser->exists("objectId");
      }
//      if($cols){
//          $cols = explode(', ', $cols);
//      }


      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $limit = 20;
      $countQuery1 = new ParseQuery("Card");
      if($search){
         $countQuery1->containString("cardNumber" , $search);
      }

      $countQuery2 = new ParseQuery("Card");
      if($search){
         $countQuery2->containString("holderName" , $search);
      }
      $countQuery = ParseQuery::orQueries([$countQuery1, $countQuery2]);
      if($start && $end){
         $countQuery->greaterThan("createdAt",$start);
         $countQuery->lessThan("createdAt",$end);
      }
      if($filter)
         $countQuery->matchesQuery("user", $queryUser);
      $numberOfCard = $countQuery->count();
      $totalPage = ceil($numberOfCard/$limit);

      $previous = 1;
      if($page>1){
         $previous = $page - 1;
      }
      if($page < ($totalPage)){
         $next = $page + 1;
      }else{
         $next = $totalPage;
      }

      $query1 = new ParseQuery("Card");
      if($search){
         $query1->containString("cardNumber" , $search);
      }

      $query2 = new ParseQuery("Card");
      if($search){
         $query2->containString("holderName" , $search);
      }

      $query = ParseQuery::orQueries([$query1, $query2]);
      $query->descending("cardNumber");
      $query->includeKey("user");
      $query->includeKey("cardType");
      if($start && $end){
         $query->greaterThan("createdAt",$start);
         $query->lessThan("createdAt",$end);
      }
      if($filter)
         $query->matchesQuery("user", $queryUser);
      $query->limit($limit);
      $query->skip($limit * ($page-1));
      $results = $query->find();


      $schema = new ParseSchema('_User');
      $fields = $schema->get();
      $field = array();
      foreach($fields as $f){
            $field[] = $f;
      }
       array_splice($field, array_search('ACL', $field ), 1);


       return view(
         'cards.index',
         array(
            'results'=>$results,
            'search'=>$search,
            'filter'=>$filter,
            'startDate'=>$startDate,
            'endDate'=>$endDate,
            'start'=>$start,
            'end'=>$end,
            'field'=>$field,
            'cols'=>$cols,
            'totalPage'=>$totalPage,
            'currentPage'=>$page,
            'previous'=>$previous,
            'next'=>$next,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }


   public function cardExport(){
      $start = "";
      $end = "";
      $cols = "";
      $filter = isset($_GET['filter'])?$_GET['filter']:"";

      if(isset($_GET['start'])){
         $start = $_GET['start']!=""?new DateTime($_GET['start'].'T00:00:00.000Z'):"";
      }
      if(isset($_GET['start'])){
         $end = $_GET['end']!=""?new DateTime($_GET['end'].'T23:59:59.000Z'):"";
      }
      if(isset($_GET['cols'])){
          $cols = $_GET['cols'];
      }

      $results = array();
      $limit = 1000;
      $index = 0;

      do {
          $tmparr = $this->getUserCard($start, $end, $filter, $cols, $index, $limit);
          $results = array_merge($results, $tmparr);
          $index = $index + $limit;
      } while (count($tmparr) == $limit);

      $cards = array();

      //my new update
       $column = explode(', ', $cols);
       $arr = array();

       foreach ($results as $value) {
            if ($value) {
               $customerName = "";
               $status = "";
               if ($value->has('user')) {
                  if ($value->get('user')->get('firstName')) {
                     $customerName = $value->get('user')->get('firstName');
                  }
                  if ($value->get('user')->get('lastName')) {
                        $customerName = $customerName.' '.$value->get('user')->get('lastName');
                  }

                  if ($value->get('user')->get('opt')=='') {
                        $status = "Not Activate";
                  }else{
                     $status = "Activated";
                  }
               }

               $cardType = "";
               if ($value->has('cardType')) {
                  $cardType = $value->get('cardType')->get('title');
               }


               // my new update
                foreach ($column as $col) {
                    if ($col == "Card Number") {
                        $arr["Card Number"] = $value->get("cardNumber");
                    }
                    if ($col == "Customer Name") {
                        $arr["Customer Name"] = $customerName;
                    }
                    if ($col == "Holder Name") {
                        $arr["Holder Name"] = $value->get("holderName");
                    }
                    if ($col == "Card Type"){
                        $arr["Card Type"] = $cardType;
                    }
                    if ($col == "Status") {
                        $arr["Status"] = $status;
                    }
                    if ($col == "Created Date") {
                        $arr["Created Date"] = $value->getCreatedAt()->format('d-M-Y');
                    }
                    if ($col == "First Name") {
                        $arr["First Name"] = $value->get("user")->get("firstName");
                    }
                    if ($col == "Last Name") {
                        $arr["Last Name"] = $value->get("user")->get("lastName");
                    }
                    if ($col == "Gender") {
                        $arr["Gender"] = $value->get("user")->get("gender");
                    }
                    if ($col == "Phone") {
                        $arr["Phone"] = $value->get("user")->get("phone");
                    }
                    if ($col == "Email") {
                        $arr["Email"] = $value->get("user")->get("email");
                    }
                    if ($col == "DOB") {
                        $arr["DOB"] = !empty($value->get('user')->get('dob'))?$value->get('user')->get('dob')->format('d-M-Y'):'';
                    }
                    if ($col == "Register") {
                        $arr["Register"] = !empty($value->get('user')->get('register'))?$value->get('user')->get('register')->format('d-M-Y'):'';
                    }
                    if ($col == "Married") {
                        $arr["Married"] = $value->get("user")->get("marrired");
                    }
                    if ($col == "Facebook") {
                        $arr["Facebook"] = $value->get("user")->get("facebook");
                    }
                    if ($col == "Wechat") {
                        $arr["Wechat"] = $value->get("user")->get("wechat");
                    }
                    if ($col == "Line") {
                        $arr["Line"] = $value->get("user")->get("line");
                    }
                    if ($col == "Address") {
                        $arr["Address"] = $value->get("user")->get("address");
                    }
                    if ($col == "Sangkat") {
                        $arr["Sangkat"] = $value->get("user")->get("sangkat");
                    }
                    if ($col == "Khan") {
                        $arr["Khan"] = $value->get("user")->get("khan");
                    }
                    if ($col == "City") {
                        $arr["City"] = $value->get("user")->get("city");
                    }
                    if ($col == "Country") {
                        $arr["Country"] = $value->get("user")->get("country");
                    }
                    if ($col == "Income") {
                        $arr["Income"] = $value->get("user")->get("income");
                    }
                    if ($col == "Source Retail") {
                        $arr["Source Retail"] = $value->get("user")->get("sourceretail");
                    }
                    if ($col == "Keyin") {
                        $arr["Keyin"] = $value->get("user")->get("keyin");
                    }

                }
                $cards[] = $arr;






//               $cards[] = array(
//                  "Card Number"=>$value->get('cardNumber'),
//                  "Customer Name"=>$customerName,
//                  "Holder Name"=>$value->get('holderName'),
//                  "Card Type"=>$cardType,
//                  "Status"=>$status,
//                  "Created Date"=>$value->getCreatedAt()->format('d-M-Y'),
//                   "First Name"=>$value->get('user')->get('firstName'),
//                   "Last Name"=>$value->get('user')->get('lastName'),
//                   "Gender"=>$value->get('user')?$value->get('user')->get('gender'):'',
//                   "Phone"=>$value->get('user')?$value->get('user')->get('phone'):'',
//                   "Email"=>$value->get('user')?$value->get('user')->get('email'):'',
//                   "Married"=>$value->get('user')?$value->get('user')->get('married'):'',
//                   "Income"=>$value->get('user')?$value->get('user')->get('income'):'',
//                   "Children"=>$value->get('user')?$value->get('user')->get('children'):'',
//                   "DOB"=>!empty($value->get('user')->get('dob'))?$value->get('user')->get('dob')->format('d-M-Y'):'',
//                   "Address"=>$value->get('user')?$value->get('user')->get('address'):'',
//                   "Sangkat"=>$value->get('user')?$value->get('user')->get('sangkat'):'',
//                   "Khan"=>$value->get('user')?$value->get('user')->get('kahn'):'',
//                   "City"=>$value->get('user')?$value->get('user')->get('city'):'',
//                   "Country"=>$value->get('user')?$value->get('user')->get('country'):'',
//                   "Facebook"=>$value->get('user')?$value->get('user')->get('facebook'):'',
//                   "Wechat"=>$value->get('user')?$value->get('user')->get('wechat'):'',
//                   "Line"=>$value->get('user')?$value->get('user')->get('line'):'',
//                   "Keyin"=>$value->get('user')?$value->get('user')->get('keyin'):'',
//                   "Source Retail"=>$value->get('user')?$value->get('user')->get('sourceretail'):'',
//
//               );
            }
      }

      Excel::create('UserCard', function($excel)use($cards){
         $excel->sheet('ExportFile', function($sheet)use($cards){
            $sheet->fromArray($cards);
         });
      })->export('xlsx');

      Excel::create('UserRedeem', function($excel)use($redeems){
         $excel->sheet('ExportFile', function($sheet)use($redeems){
            $sheet->fromArray($redeems);
         });
      })->export('xlsx');
   }

   public function create(){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $query = new ParseQuery("CardType");
      $cardtypes = $query->find();
      return view(
         'cards.create',
         array(
            'cardtypes'=>$cardtypes,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function creation(){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $query = new ParseQuery("CardType");
      $cardtypes = $query->find();
      return view(
         'cards.creation',
         array(
            'cardtypes'=>$cardtypes,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function store(Request $request){
      return $this->saveCard($request);
   }

   public function edit($id){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $query = new ParseQuery("Card");
   	$query->includeKey("user");
      $query->includeKey("cardType");
   	$query->equalTo("objectId", $id);
   	$result = $query->first();

      $query = new ParseQuery("CardType");
      $cardtypes = $query->find();

      return view(
         'cards.edit',
         array(
            'result'=>$result,
            'cardtypes'=>$cardtypes,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function update(Request $request, $id){
      return $this->saveCard($request, $id);
   }

   public function destroy($id){
      $this->deleteCardUser($id);
      $this->deleteCard($id);
      return redirect()->back();
   }

   // working on card type
   public function cardtype(){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $query = new ParseQuery("CardType");
      $results = $query->find();

      return view(
         'cards.cardtype',
         array(
            'results'=>$results,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function destroytype($id){
      try {
         $cardtype = new ParseObject("CardType", $id);
			$cardtype->destroy();
         return redirect()->back();
      } catch (ParseException $ex) {
			echo 'Can not delete this Card Type: ' + $ex->getMessage();
		}
   }

   public function storetype(Request $request){
      return $this->saveCardType($request);
   }

   /////////////////////////////////////
   //function working on card type
   //////////////////////////////////////
   function saveCardType(Request $request, $id=null){
      if ($id) {
         // update on previos shop record
         $card = new ParseObject("CardType", $id);
         $cardtype->set("title", $request->get('title'));
         $cardtype->save();

         return redirect()->back();
      }else{
         // add new card type card record
         $cardtype = new ParseObject("CardType");
         $cardtype->set("title", $request->get('title'));
         $cardtype->save();

         return redirect()->back();
      }
   }

   ////////////////////////////////////////
   //function working on Data
   //////////////////////////////////////
   function saveCard(Request $request, $id=null){
      if ($id) {
         // update on previos shop record
         $dob = new DateTime($request->get('dob'));
         $customerType = new ParseObject("CustomerType","wm1YAC9NFK");

         if($request->get('userId')){
            $user = new ParseObject("_User", $request->get('userId'));
            $user->set("firstName", $request->get('firstName'));
            $user->set("lastName", $request->get('lastName'));
            $user->set("phone", str_replace(' ','',$request->get('phone')));
            $user->set("gender", $request->get('gender'));
            $user->set("username", str_replace(' ','',$request->get('phone')).time());
            $user->set("dob", $dob);
            $user->set("married", boolval($request->get('married')));
            $user->set("children", intval($request->get('children')));
            $user->set("address", $request->get('address'));
            $user->set("sangkat", $request->get('sangkat'));
            $user->set("khan", $request->get('khan'));
            $user->set("city", $request->get('city'));
            $user->set("country", $request->get('country'));
            $user->set("keyin", $request->get('keyin'));
            $user->set("facebook", $request->get('facebook'));
            $user->set("line", $request->get('line'));
            $user->set("wechat", $request->get('wechat'));
            $user->save(true);
         }else{
            $user = new ParseUser();
            $user->set("firstName", $request->get('firstName'));
            $user->set("lastName", $request->get('lastName'));
            $user->set("phone", $request->get('phone'));
            $user->set("gender", $request->get('gender'));
            $user->set("username", str_replace(' ','',$request->get('phone')).time());
            $user->set("password", str_replace(' ','',$request->get('phone')).time());
            $user->set("customerType", $customerType);
            $user->set("dob", $dob);
            $user->set("gender", $request->get('gender'));
            $user->set("married", boolval($request->get('married')));
            $user->set("children", intval($request->get('children')));
            $user->set("address", $request->get('address'));
            $user->set("sangkat", $request->get('sangkat'));
            $user->set("khan", $request->get('khan'));
            $user->set("city", $request->get('city'));
            $user->set("country", $request->get('country'));
            $user->set("keyin", $request->get('keyin'));
            $user->set("facebook", $request->get('facebook'));
            $user->set("line", $request->get('line'));
            $user->set("wechat", $request->get('wechat'));
            try {
               $user->signUp();
            } catch (ParseException $ex) {
               $message = $ex->getMessage();
            }
         }

         $card = new ParseObject("Card", $id);
         $card->set("holderName", $request->get('holderName'));
         $card->set("cardNumber", str_replace(' ','',$request->get('cardNumber')));
         if($request->get('cardType')){
            $cardType = new ParseObject("CardType",$request->get('cardType'));
            $card->set("cardType", $cardType);
         }
         if($user->getObjectId()){
            $user = new ParseObject("_User",$user->getObjectId());
            $card->set("user", $user);
         }
         $card->save();

         return redirect()->back();
      }else{

         // add new user card record
         $dob = new DateTime($request->get('dob'));
         $customerType = new ParseObject("CustomerType","wm1YAC9NFK");

         $user = new ParseUser();
         $user->set("firstName", $request->get('firstName'));
         $user->set("lastName", $request->get('lastName'));
         $user->set("phone", $request->get('phone'));
         $user->set("gender", $request->get('gender'));
         $user->set("username", str_replace(' ','',$request->get('phone')).time());
         $user->set("password", str_replace(' ','',$request->get('phone')).time());
         $user->set("customerType", $customerType);
         $user->set("dob", $dob);
         $user->set("gender", $request->get('gender'));
         $user->set("married", boolval($request->get('married')));
         $user->set("children", intval($request->get('children')));
         $user->set("address", $request->get('address'));
         $user->set("sangkat", $request->get('sangkat'));
         $user->set("khan", $request->get('khan'));
         $user->set("city", $request->get('city'));
         $user->set("country", $request->get('country'));
         $user->set("keyin", $request->get('keyin'));
         $user->set("facebook", $request->get('facebook'));
         $user->set("line", $request->get('line'));
         $user->set("wechat", $request->get('wechat'));
         try {
            $user->signUp();
         } catch (ParseException $ex) {
            $message = $ex->getMessage();
         }

         $card = new ParseObject("Card");
         $card->set("holderName", $request->get('holderName'));
         $card->set("cardNumber", str_replace(' ','',$request->get('cardNumber')));
         if($request->get('expiredDate')){
            $expiredDate = new DateTime($request->get('expiredDate'));
            $card->set("expiredDate", $expiredDate);
         }
         if($request->get('cardType')){
            $cardType = new ParseObject("CardType",$request->get('cardType'));
            $card->set("cardType", $cardType);
         }
         if($user->getObjectId()){
            $user = new ParseObject("_User",$user->getObjectId());
            $card->set("user", $user);
         }
         $card->save();
         return redirect()->back()->with('message', 'New user card had been created successfully!');
      }
   }
   // delete card by objectid
   function deleteCard($id){
      try {
         $card = new ParseObject("Card", $id);
			$card->destroy();
      } catch (ParseException $ex) {
			echo 'Can not delete this Card: ' + $ex->getMessage();
		}
   }
   // delete card user by card id
   function deleteCardUser($cid){
      $query = new ParseQuery("Card");
   	$query->includeKey("user");
   	$query->equalTo("objectId", $cid);
   	$result = $query->first();
      if($result->get('user')){
         try {
            $user = new ParseObject("_User", $result->get('user')->getObjectId());
   			$user->destroy(true);
         } catch (ParseException $ex) {
   			echo 'Can not delete this User: ' + $ex->getMessage();
   		}
      }
   }

   // Get all record by pagination
   function getUserCard($startDate, $endDate, $filter, $cols, $startIndex, $limit) {

      if($filter == 'activated'){
         $queryUser = new ParseQuery("_User");
         $queryUser->exists("opt");
      }elseif($filter == 'notactivate'){
         $queryUser = new ParseQuery("_User");
         $queryUser->doesNotExist("opt");
      }else{
         $queryUser = new ParseQuery("_User");
         $queryUser->exists("objectId");
      }

      $query = new ParseQuery("Card");

      //============ New update select multiple column to export UserCard ==============
      $column = explode(', ', $cols);
      $mycol = array();
      foreach($column as $col){
          $mycol[]=$col;
      }

      $lastCol = array();
      foreach($mycol as $cc){
          if($cc == "Card Number"){
              $cc = "cardNumber";
              $lastCol[] = $cc;
          }
          if($cc == "Holder Name"){
              $cc= "holderName";
              $lastCol[] = $cc;
          }
          if($cc == "Card Type"){
              $cc= "cardType.title";
              $lastCol[] = $cc;
          }
          if($cc == "Last Name"){
              $cc= "user.lastName";
              $lastCol[] = $cc;
          }
          if($cc == "First Name"){
              $cc= "user.firstName";
              $lastCol[] = $cc;
          }
          if($cc == "Gender"){
              $cc= "user.gender";
              $lastCol[] = $cc;
          }
          if($cc == "Phone"){
              $cc= "user.phone";
              $lastCol[] = $cc;
          }
          if($cc == "Email"){
              $cc= "user.email";
              $lastCol[] = $cc;
          }
          if($cc == "Facebook"){
              $cc= "user.facebook";
              $lastCol[] = $cc;
          }
          if($cc == "Wechat"){
              $cc= "user.wechat";
              $lastCol[] = $cc;
          }
          if($cc == "Line"){
              $cc= "user.line";
              $lastCol[] = $cc;
          }
          if($cc == "Khan"){
              $cc= "user.khan";
              $lastCol[] = $cc;
          }
          if($cc == "Sangkat"){
              $cc= "user.sangkat";
              $lastCol[] = $cc;
          }
          if($cc == "City"){
              $cc= "user.city";
              $lastCol[] = $cc;
          }
          if($cc == "Country"){
              $cc= "user.country";
              $lastCol[] = $cc;
          }
          if($cc == "Address"){
              $cc= "user.address";
              $lastCol[] = $cc;
          }
          if($cc == "Keyin"){
              $cc= "user.keyin";
              $lastCol[] = $cc;
          }
          if($cc == "DOB"){
              $cc= "user.dob";
              $lastCol[] = $cc;
          }
          if($cc == "Username"){
              $cc= "user.username";
              $lastCol[] = $cc;
          }
          if($cc == "Source Retail"){
              $cc= "user.sourceretail";
              $lastCol[] = $cc;
          }
          if($cc == "Children"){
              $cc= "user.children";
              $lastCol[] = $cc;
          }
          if($cc == "Customer Type"){
              $cc= "user.customerType.type";
              $lastCol[] = $cc;
          }
          if($cc == "Register"){
              $cc= "user.register";
              $lastCol[] = $cc;
          }
      }

      $lastColStr = implode(',', $lastCol);
      $query->select([$lastColStr]);
    //===================== End my new update ========


//      $query->select(["cardNumber","cardNumber","holderName",
//          "user.lastName", "user.firstName","user.opt","user.gender","user.register","user.keyin","user.facebook","user.wechat","user.income",
//          "user.khan","user.city","user.phone","user.address","user.married","user.line","user.username","user.country","user.dob","user.sangkat",
//          "user.email","user.children","cardType.title"]);


      if($startDate && $endDate){
           $query->greaterThan("createdAt",$startDate);
           $query->lessThan("createdAt",$endDate);
      }

      if($filter)
         $query->matchesQuery("user", $queryUser);

      $query->skip($startIndex);
      $query->limit($limit);
      $results = $query->find(true);

      return $results;
   }




    /// upload with csv
    function csv() {
        $csvArray = $this->ImportCSV2Array("newfile.csv");
        $line = 0;
        foreach ($csvArray as $line){
            print_r($line);
            $line++;
            echo "<br/> ------------------------------------------------------------------------------ <br/>";


            if ($line["phone"]) {
                // add new user card record
                $customerType = new ParseObject("CustomerType","wm1YAC9NFK");

                $user = new ParseUser();
                $user->set("firstName", $line["fname"]);
                $user->set("lastName", $line["lname"]);
                $user->set("phone", str_replace(' ','',$line["phone"]));
                $user->set("gender", $line["gender"]);
                $user->set("username", str_replace(' ','',$line["phone"]));
                $user->set("password", $line["phone"]);
                $user->set("customerType", $customerType);
                try {
                    $user->signUp();
                } catch (ParseException $ex) {
                    $message = $ex->getMessage();
                }

                $card = new ParseObject("Card");
                $card->set("holderName", $line["holder"]);
                $card->set("cardNumber", str_replace(' ','',$line["card"]));

                $expiredDate = new DateTime("2021-01-01");
                $card->set("expiredDate", $expiredDate);


                $cardType = new ParseObject("CardType", "cWn1xarDod");
                $card->set("cardType", $cardType);

                if($user->getObjectId()){
                    $user = new ParseObject("_User",$user->getObjectId());
                    $card->set("user", $user);
                }
                $card->save();
            }

            if ($line == 3) {

                exit();
            }
        }
        exit();
    }


    function ImportCSV2Array($filename)
    {
    $row = 0;
    $col = 0;

    $handle = @fopen($filename, "r");
    if ($handle)
    {
        while (($row = fgetcsv($handle, 4096)) !== false)
        {
            if (empty($fields))
            {
                $fields = $row;
                continue;
            }

            foreach ($row as $k=>$value)
            {
                $results[$col][$fields[$k]] = $value;
            }
            $col++;
            unset($row);
        }
        if (!feof($handle))
        {
            echo "Error: unexpected fgets() failn";
        }
        fclose($handle);
    }

    return $results;
}







   // MY NEW UPDATE IMPORT CARD
   public function browseFile(){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      return view(
         'cards.cardimport',
         array(
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

    public function importCard(Request $request){
         $file = $request->file('file');
         $destination = 'csv';
         $file->move($destination, $file->getClientOriginalName());
         $csvArray = $this->csvToArray("csv/".$file->getClientOriginalName());
         dump($csvArray); 
         exit();

        $line = 0;
        foreach ($csvArray as $line){
         print_r($line);
         $line++;
         echo "<br/> ------------------------------------------------------------------------------ <br/>";
            $card1 = new ParseQuery('Card');
            $card1->equalTo('cardNumber', $line["card"]);
            $result = $card1->find(); 
            if ($line["phone"]) { 
               if(count($result)==0){
                  // add new user
                  $customerType = new ParseObject("CustomerType","wm1YAC9NFK");
                  $user = new ParseUser();
                  $user->set("firstName", $line["fname"]);
                  $user->set("lastName", $line["lname"]);
                  $user->set("phone", str_replace(' ','',$line["phone"]));
                  $user->set("gender", $line["gender"]);
                  $user->set("username", str_replace(' ','',$line["phone"]));
                  $user->set("password", $line["phone"]);
                  $user->set("customerType", $customerType);
                  try {
                      $user->signUp();
                  } catch (ParseException $ex) {
                      $message = $ex->getMessage();
                  }

                  //add new card
                  $card = new ParseObject("Card");
                  $card->set("holderName", $line["holder"]);
                  $card->set("cardNumber", str_replace(' ','',$line["card"]));
                  $expiredDate = new DateTime("2021-01-01");
                  $card->set("expiredDate", $expiredDate);

                  $cardType = new ParseObject("CardType", "cWn1xarDod");
                  $card->set("cardType", $cardType);
                  if($user->getObjectId()){
                     $user = new ParseObject("_User",$user->getObjectId());
                     $card->set("user", $user);
                  }
                  $card->save();

                 
               }
            }
            if ($line == 3) {
               exit();
            }
        }// end foreach csvArray
        exit();
    }

    public function csvToArray($filename){
        $row = 0;
        $col = 0;

        $handle = @fopen($filename, "r");
        if ($handle)
        {
            while (($row = fgetcsv($handle, 4096)) !== false)
            {
                if (empty($fields))
                {
                    $fields = $row;
                    continue;
                }

                foreach ($row as $k=>$value)
                {
                    $results[$col][$fields[$k]] = $value;
                }
                $col++;
                unset($row);
            }
            if (!feof($handle))
            {
                echo "Error: unexpected fgets() failn";
            }
            fclose($handle);
        }

        return $results;
    }
}
