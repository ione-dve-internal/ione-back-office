<?php
namespace App\Http\Controllers;
use DateTime;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\LibHelper;
use App\UdLib\MenuObject;
use App\UdLib\MenuObjectList;
use App\Http\Requests;
use Session;

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Closure;

class MenuController extends Controller
{
    public function getAllParentMenu(){
      $query = new ParseQuery("UserMenu");
      $query->equalTo("parent", null);
      $query->ascending("order");
      return $query->find();
   }

   public function getAllChildMenu(){
      $query = new ParseQuery("UserMenu");
      $query->includeKey("parent");
      $query->ascending("order");
      $query->notEqualTo("parent", null);
      return $query->find();
   }

   public function getMenusByUserId($uid){
      $user = new ParseObject("_User", $uid);

      $query = new ParseQuery("UserRole");
      $query->equalTo("user", $user);
      $query->includeKey("menu");
      return $query->find();
   }
}
