<?php
namespace App\Http\Controllers;
use DateTime;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\LibHelper;
use App\UdLib\MenuObject;
use App\UdLib\MenuObjectList;
use App\Http\Requests;
use Session;

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Closure;

class DashboardController extends Controller
{
   private $MenuController;

   public function __construct(){
      $this->MenuController = new MenuController();
   }

   public function index(){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $numberOfPromotion = $this->getAllPromotions();
      $numberOfShops = $this->getAllShops();
      $numberOfCards = $this->getAllCards();
      $numberOfFeedback = $this->getAllFeedback();
      return view(
         'dashboard',
         array(
            'menus'=>$menus,
            'childmenus'=>$childmenus,
            'numberOfPromotion'=>$numberOfPromotion,
            'numberOfShops'=>$numberOfShops,
            'numberOfCards'=>$numberOfCards,
            'numberOfFeedback'=>$numberOfFeedback,
         )
      );
	}

   function getAllPromotions(){
      $query = new ParseQuery("Promotion");
      $result = $query->count();

      return $result;
   }

   function getAllShops(){
      $query = new ParseQuery("Shop");
      $result = $query->count();

      return $result;
   }

   function getAllCards(){
      $query = new ParseQuery("Card");
      $result = $query->count();

      return $result;
   }

   function getAllFeedback(){
      $query = new ParseQuery("Feedback");
      $result = $query->count();

      return $result;
   }
}
