<?php
namespace App\Http\Controllers;
use DateTime;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\LibHelper;
use App\UdLib\MenuObject;
use App\UdLib\MenuObjectList;
use App\Http\Requests;
use Session;

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Closure;

class PreOrderController extends Controller
{
   /////////////////////
   //working on preorder
   /////////////////////
   private $MenuController;

   public function __construct(){
      $this->MenuController = new MenuController();
   }

   public function index(){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $results = $this->getAllPreOrder();
      return view(
         'preorder.index',
         array(
            'results'=>$results,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function create(){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      return view(
         'preorder.create',
         array(
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function store(Request $request){
      return $this->savePreOrder($request);
   }

   public function edit($id){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $preOrder = $this->getPreOrder($id);
      $preOrderDetails = $this->getPreOrderDetailByPreOrderId($id);
      $productPrices = $this->getProductPriceByPreOrderId($id);
      $productAppearences = $this->getProductAppearenceByPreOrderId($id);

      return view(
         'preorder.edit',
         array(
            'preOrder'=>$preOrder,
            'preOrderDetails'=>$preOrderDetails,
            'productPrices'=>$productPrices,
            'productAppearences'=>$productAppearences,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function update(Request $request, $id){
      return $this->savePreOrder($request, $id);
   }

   public function destroy($id){
      $this->deletePreOrder($id);
      $this->deletePreOrderDetailByPreOrderId($id);
      $this->deleteProductPriceByPreOrderId($id);
      $this->deleteProductAppearenceByPreOrderId($id);
      return redirect()->back();
   }

   //////////////////////////////
   // working on preorder detail
   /////////////////////////////
   public function storedetail(Request $request){
      return $this->savePreOrderDetail($request);
   }

   public function updatedetail(Request $request, $id){
      return $this->savePreOrderDetail($request, $id);
   }

   public function destroydetail($id){
      $this->deletePreOrderDetail($id);
      $this->deleteProductPriceByPreOrderDetailId($id);
      $this->deleteProductAppearenceByPreOrderDetailId($id);
      return redirect()->back();
   }

   //////////////////////////////
   // working on product price
   /////////////////////////////
   public function storeprice(Request $request){
      return $this->saveProductPrice($request);
   }

   public function updateprice(Request $request, $id){
      return $this->saveProductPrice($request, $id);
   }

   public function destroyprice($id,$detailId){
      $this->deleteProductPrice($id,$detailId);
      return redirect()->back();
   }

   //////////////////////////////
   // working on product price
   /////////////////////////////
   public function storeappearence(Request $request){
      return $this->saveProductAppearence($request);
   }

   public function updateappearence(Request $request, $id){
      return $this->saveProductAppearence($request, $id);
   }

   public function destroyappearence($id){
      $this->deleteProductAppearence($id);
      return redirect()->back();
   }


   /////////////////////////////////
   // function working for preorder
   ////////////////////////////////
   function savePreOrder(Request $request, $id=null) {
      if ($id) {
         // update on previos preorder record
         $available = new DateTime($request->get('available'));
         $preorder = new ParseObject("PreOrder", $id);

         $preorder->set("redeemCode", $request->get('redeemCode'));
         $preorder->set("name", $request->get('name'));
         $preorder->set("available", $available);
         $preorder->set("status", boolval($request->get('status')));

         if ($_FILES["thumb"]["name"]) {
            $thumbName = $_FILES["thumb"]["name"];
            $thumb = ParseFile::createFromData(file_get_contents( $_FILES['thumb']['tmp_name'] ), $thumbName);
            $preorder->set("thumb", $thumb);
         }
         $preorder->save();

         return redirect()->back();
      }
      else {
         // add new preorder record
         $available = new DateTime($request->get('available'));
         $preorder = new ParseObject("PreOrder");

         $preorder->set("redeemCode", $request->get('redeemCode'));
         $preorder->set("name", $request->get('name'));
         $preorder->set("available", $available);
         $preorder->set("status", boolval($request->get('status')));
         if ($_FILES["thumb"]["name"]) {
            $thumbName = $_FILES["thumb"]["name"];
            $thumb = ParseFile::createFromData(file_get_contents( $_FILES['thumb']['tmp_name'] ), $thumbName);
            $preorder->set("thumb", $thumb);
         }
         $preorder->save();

         return redirect('preorder/edit/'.$preorder->getObjectId());
      }
   }

   // get preorder by objectid
   function getPreOrder($id){
      $query = new ParseQuery("PreOrder");
      $query->equalTo("objectId", $id);
      return $query->first();
   }

   // get all preorder record
   function getAllPreOrder(){
      $query = new ParseQuery("PreOrder");
      return $query->find();
   }

   // delete preorder by objectid
   function deletePreOrder($id){
      try {
         $query = new ParseQuery("PreOrder");
         $preorder=$query->get($id);
         $preorder->destroy();
         return redirect()->back();
      } catch (ParseException $ex) {
         echo 'Can not delete this preorder: ' + $ex->getMessage();
      }
   }

   ///////////////////////////////////////
   // function working for preorder detail
   ///////////////////////////////////////
   function savePreOrderDetail(Request $request, $id=null) {
      if ($id) {
         // update on previos preorder detail record
         $detail = new ParseObject("PreOrderDetail", $id);
         if ($request->get('productName')) {
            $detail->set("productName", $request->get('productName'));
         }
         if ($request->get('spec')) {
            $detail->set("spec", $request->get('spec'));
         }

         if ($request->get('preorderId')) {
            $preorder = new ParseObject("PreOrder", $request->get('preorderId'));
            $detail->set("preorder", $preorder);
         }
         $detail->save();

         return redirect()->back();
      }else {
         // add new preorder detail record
         $detail = new ParseObject("PreOrderDetail");
         if ($request->get('productName')) {
            $detail->set("productName", $request->get('productName'));
         }
         if ($request->get('spec')) {
            $detail->set("spec", $request->get('spec'));
         }

         if ($request->get('preorderId')) {
            $preorder = new ParseObject("PreOrder", $request->get('preorderId'));
            $detail->set("preorder", $preorder);
         }
         $detail->save();

         return redirect()->back();
      }
   }

   // get all preorder detail by preorderid
   function getPreOrderDetailByPreOrderId($preOrderId){
      $preorder = new ParseObject("PreOrder", $preOrderId);

      $query = new ParseQuery("PreOrderDetail");
      $query->equalTo("preorder", $preorder);
      return $query->find();
   }

   // delete preorder detail by objectid
   function deletePreOrderDetail($id){
      try {
         $query = new ParseQuery("PreOrderDetail");
         $preorderdetail=$query->get($id);
         $preorderdetail->destroy();
      } catch (ParseException $ex) {
         echo 'Can not delete this preorder: ' + $ex->getMessage();
      }
   }

   // delete all preorder detail by preorderid
   function deletePreOrderDetailByPreOrderId($preOrderId){
      try {
         $preorder = new ParseObject("PreOrder", $preOrderId);

         $query = new ParseQuery("PreOrderDetail");
         $query->equalTo("preorder", $preorder);
         $results = $query->find();
         for ($i = 0; $i < count($results); $i++) {
            $row = $results[$i];
            $deletedetail=$query->get($row->getObjectId());
            $deletedetail->destroy();
         }
      } catch (ParseException $ex) {
         echo 'Can not delete this preorder: ' + $ex->getMessage();
      }
   }


   ///////////////////////////////////////
   // function working for product price
   ///////////////////////////////////////
   function saveProductPrice(Request $request, $id=null) {
      if ($id) {
         // update on previos product price record
         $price = new ParseObject("ProductPrice", $id);

         if ($request->get('capacity')) {
            $price->set("capacity", $request->get('capacity'));
         }

         if ($request->get('price')) {
            $price->set("price", intval($request->get('price')));
         }

         $price->save();
         return redirect()->back();
      }else{
         // add new product price record
         $price = new ParseObject("ProductPrice");

         if ($request->get('capacity')) {
            $price->set("capacity", $request->get('capacity'));
         }

         if ($request->get('price')) {
            $price->set("price", intval($request->get('price')));
         }

         if ($request->get('detailId')) {
            $detail = new ParseObject("PreOrderDetail", $request->get('detailId'));
            $price->set("detail", $detail);
         }

         if ($request->get('preorderId')) {
            $preorder = new ParseObject("PreOrder", $request->get('preorderId'));
            $price->set("preorder", $preorder);
         }
         $price->save();

         if ($request->get('detailId')) {
            // update prices on product appearance record by detailId
            $this->updateProductAppearencePriceByDetailId($request->get('detailId'));
         }
         return redirect()->back();
      }
   }

   // update prices on product appearance record by detailId
   function updateProductAppearencePriceByDetailId($detailId){
      $productAppearences = $this->getProductAppearenceByPreOrderDetailId($detailId);
      foreach($productAppearences as $productAppearence){
         if($productAppearence){
            $appearance = new ParseObject("ProductAppearence", $productAppearence->getObjectId());

            $productPrices = $this->getProductPriceByPreOrderDetailId($detailId);
            $prices = array();
            foreach($productPrices as $productPrice){
               $prices[] = $productPrice->getObjectId();
            }
            $appearance->setArray("prices", $prices);
            $appearance->save();
         }
      }
   }

   // get all product price by preorderid
   function getProductPriceByPreOrderId($preOrderId){
      $preorder = new ParseObject("PreOrder", $preOrderId);

      $query = new ParseQuery("ProductPrice");
      $query->equalTo("preorder", $preorder);
      return $query->find();
   }

   // get all product price by detailid
   function getProductPriceByPreOrderDetailId($preOrderDetailId){
      $detail = new ParseObject("PreOrderDetail", $preOrderDetailId);

      $query = new ParseQuery("ProductPrice");
      $query->equalTo("detail", $detail);
      return $query->find();
   }

   // delete product price by objectid
   // update prices on product appearance record by detailId
   function deleteProductPrice($id,$detailId=null){
      try {
         $query = new ParseQuery("ProductPrice");
         $productprice=$query->get($id);
         $productprice->destroy();
      } catch (ParseException $ex) {
         echo 'Can not delete this product price: ' + $ex->getMessage();
      }

      if ($detailId) {
         // update prices on product appearance record by detailId
         $this->updateProductAppearencePriceByDetailId($detailId);
      }
   }

   // delete all product price by preorderdetailid
   function deleteProductPriceByPreOrderDetailId($preOrderDetailId){
      try {
         $detail = new ParseObject("PreOrderDetail", $preOrderDetailId);

         $query = new ParseQuery("ProductPrice");
         $query->equalTo("detail", $detail);
         $results = $query->find();
         for ($i = 0; $i < count($results); $i++) {
            $row = $results[$i];
            $deleteproductprice=$query->get($row->getObjectId());
            $deleteproductprice->destroy();
         }
      } catch (ParseException $ex) {
         echo 'Can not delete this preorder: ' + $ex->getMessage();
      }
   }

   // delete all product price by preorderid
   function deleteProductPriceByPreOrderId($preOrderId){
      try {
         $preorder = new ParseObject("PreOrder", $preOrderId);

         $query = new ParseQuery("ProductPrice");
         $query->equalTo("preorder", $preorder);
         $results = $query->find();
         for ($i = 0; $i < count($results); $i++) {
            $row = $results[$i];
            $deleteproductprice=$query->get($row->getObjectId());
            $deleteproductprice->destroy();
         }
      } catch (ParseException $ex) {
         echo 'Can not delete this preorder: ' + $ex->getMessage();
      }
   }


   ///////////////////////////////////////////
   // function working for product appearance
   //////////////////////////////////////////
   function saveProductAppearence(Request $request, $id=null) {
      if ($id) {
         // update on previos product appearance record
         $appearance = new ParseObject("ProductAppearence", $id);
         if ($request->get('title')) {
            $appearance->set("title", $request->get('title'));
         }
         if ($_FILES["photo"]["name"]) {
            $photoName = $_FILES["photo"]["name"];
            $photo = ParseFile::createFromData(file_get_contents( $_FILES['photo']['tmp_name'] ), $photoName);
            $appearance->set("photo", $photo);
         }
         if ($request->get('detailId')) {
            $detail = new ParseObject("PreOrderDetail", $request->get('detailId'));
            $appearance->set("detail", $detail);
         }

         // TODO: save prices objectId as array of string. To it base on ur html form. below it's sample to save
         // *****************
         //$appearance->set("prices", ["xxx", "yyyy", "zzzzz"]);

         $appearance->save();
         return redirect()->back();
      }else {
         // add new product appearance record
         $appearance = new ParseObject("ProductAppearence");
         if ($request->get('title')) {
            $appearance->set("title", $request->get('title'));
         }
         if ($_FILES["photo"]["name"]) {
            $photoName = $_FILES["photo"]["name"];
            $photo = ParseFile::createFromData(file_get_contents( $_FILES['photo']['tmp_name'] ), $photoName);
            $appearance->set("photo", $photo);
         }
         if ($request->get('detailId')) {
            $detail = new ParseObject("PreOrderDetail", $request->get('detailId'));
            $appearance->set("detail", $detail);
         }
         if ($request->get('preOrderId')) {
            $preorder = new ParseObject("PreOrder", $request->get('preOrderId'));
            $appearance->set("preorder", $preorder);
         }

         // TODO: save prices objectId as array of string. To it base on ur html form. below it's sample to save
         // *****************
         if ($request->get('detailId')) {
            $productPrices = $this->getProductPriceByPreOrderDetailId($request->get('detailId'));
            $prices = array();
            foreach($productPrices as $productPrice){
               $prices[] = $productPrice->getObjectId();
            }
            $appearance->setArray("prices", $prices);
         }

         $appearance->save();
         return redirect()->back();
      }
   }

   // get all product appearance by preorderid
   function getProductAppearenceByPreOrderId($preOrderId){
      $preorder = new ParseObject("PreOrder", $preOrderId);

      $query = new ParseQuery("ProductAppearence");
      $query->equalTo("preorder", $preorder);
      return $query->find();
   }

   // get all product appearance by detailId
   function getProductAppearenceByPreOrderDetailId($preOrderDetailId){
      $detail = new ParseObject("PreOrderDetail", $preOrderDetailId);

      $query = new ParseQuery("ProductAppearence");
      $query->equalTo("detail", $detail);
      return $query->find();
   }

   // delete product price by objectid
   function deleteProductAppearence($id){
      try {
         $query = new ParseQuery("ProductAppearence");
         $productappearence=$query->get($id);
         $productappearence->destroy();
      } catch (ParseException $ex) {
         echo 'Can not delete this product appearance: ' + $ex->getMessage();
      }
   }

   // delete all product appearance by preorderdetailid
   function deleteProductAppearenceByPreOrderDetailId($preOrderDetailId){
      try {
         $detail = new ParseObject("PreOrderDetail", $preOrderDetailId);

         $query = new ParseQuery("ProductAppearence");
         $query->equalTo("detail", $detail);
         $results = $query->find();
         for ($i = 0; $i < count($results); $i++) {
            $row = $results[$i];
            $deleteproductappearence=$query->get($row->getObjectId());
            $deleteproductappearence->destroy();
         }
      } catch (ParseException $ex) {
         echo 'Can not delete this preorder: ' + $ex->getMessage();
      }
   }

   // delete all product appearance by preorderid
   function deleteProductAppearenceByPreOrderId($preOrderId){
      try {
         $preorder = new ParseObject("PreOrder", $preOrderId);

         $query = new ParseQuery("ProductAppearence");
         $query->equalTo("preorder", $preorder);
         $results = $query->find();
         for ($i = 0; $i < count($results); $i++) {
            $row = $results[$i];
            $deleteproductappearence=$query->get($row->getObjectId());
            $deleteproductappearence->destroy();
         }
      } catch (ParseException $ex) {
         echo 'Can not delete this preorder: ' + $ex->getMessage();
      }
   }

}
