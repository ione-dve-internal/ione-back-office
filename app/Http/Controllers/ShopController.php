<?php
namespace App\Http\Controllers;
use DateTime;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\LibHelper;
use App\UdLib\MenuObject;
use App\UdLib\MenuObjectList;
use App\Http\Requests;
use Session;

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Parse\ParseGeoPoint;
use Closure;

class ShopController extends Controller
{
   private $MenuController;

   public function __construct(){
      $this->MenuController = new MenuController();
   }

   public function index($page = 1){
      $search = isset($_GET['search'])?$_GET['search']:"";

      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $limit = 20;
      $countQuery1 = new ParseQuery("Shop");
      if($search){
         $countQuery1->containString("code" , $search);
      }

      $countQuery2 = new ParseQuery("Shop");
      if($search){
         $countQuery2->containString("name" , $search);
      }
      $countQuery = ParseQuery::orQueries([$countQuery1, $countQuery2]);
      $numberOfShop = $countQuery->count();
      $totalPage = ceil($numberOfShop/$limit);

      $previous = 1;
      if($page>1){
         $previous = $page - 1;
      }
      if($page < ($totalPage-1)){
         $next = $page + 1;
      }else{
         $next = ($totalPage-1);
      }

      $query1 = new ParseQuery("Shop");
      if($search){
         $query1->containString("code" , $search);
      }

      $query2 = new ParseQuery("Shop");
      if($search){
         $query2->containString("name" , $search);
      }

      $query3 = new ParseQuery("Shop");
      if($search){
         $query3->containString("phone" , $search);
      }

      $query = ParseQuery::orQueries([$query1, $query2, $query3]);
      $query->descending("code");
      $query->includeKey("category");
      $query->limit($limit);
      $query->skip($limit * ($page-1));
      $results = $query->find();

      return view(
         'shop.index',
         array(
            'results'=>$results,
            'search'=>$search,
            'totalPage'=>$totalPage,
            'currentPage'=>$page,
            'previous'=>$previous,
            'next'=>$next,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function create(){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $query = new ParseQuery("ShopCategory");
      $shopcategories = $query->find();
      return view(
         'shop.create',
         array(
            'shopcategories'=>$shopcategories,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function store(Request $request){
      return $this->saveShop($request);
   }

   public function edit($id){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $query = new ParseQuery("Shop");
   	$query->includeKey("category");
   	$query->equalTo("objectId", $id);
   	$result = $query->first();

      $queryCategory = new ParseQuery("ShopCategory");
		$shopcategories = $queryCategory->find();

      return view(
         'shop.edit',
         array(
            'result'=>$result,
            'shopcategories'=>$shopcategories,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function update(Request $request, $id){
      return $this->saveShop($request, $id);
   }

   public function destroy($id){
      // update status only
      $offer = new ParseObject("Shop", $id);
      $offer->set("status", boolval(0));
      $offer->save();

      return redirect()->back();
   }

   public function image($id){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $query = new ParseQuery("Shop");
   	$query->equalTo("objectId", $id);
   	$results = $query->first();

      return view(
         'shop.image',
         array(
            'result'=>$results,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function uploadlogo(Request $request, $id){
      $logo = new ParseObject("Shop", $id);
      if ($_FILES["logo"]["name"]) {
         $filename = $_FILES["logo"]["name"];
         $file = ParseFile::createFromData(file_get_contents( $_FILES['logo']['tmp_name'] ), $filename);
         $logo->set("logo", $file);
      }
      $logo->save();
      return redirect()->back();
   }

   public function uploadphoto(Request $request, $id){
      $photo = new ParseObject("Shop", $id);
      if ($_FILES["photo"]["name"]) {
         $filename = $_FILES["photo"]["name"];
         $file = ParseFile::createFromData(file_get_contents( $_FILES['photo']['tmp_name'] ), $filename);
         $photo->set("photo", $file);
      }
      $photo->save();
      return redirect()->back();
   }

   public function review($id){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $shop = new ParseObject("Shop",$id);
      $query = new ParseQuery("ShopReview");
   	$query->includeKey("user");
   	$query->equalTo("shop", $shop);
   	$results = $query->find();

      $queryShop = new ParseQuery("Shop");
   	$queryShop->equalTo("objectId", $id);
   	$shop = $queryShop->first();

      return view(
         'shop.review',
         array(
            'results'=>$results,
            'shop'=>$shop,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }
   ////////////////////////////////////////
   //function working on Data
   //////////////////////////////////////
   function saveShop(Request $request, $id=null){
      if ($id) {
         // update on previos shop record
         $category = new ParseObject("ShopCategory",$request->get('category'));

         $shop = new ParseObject("Shop", $id);
         $shop->set("code", $request->get('code'));
         $shop->set("name", $request->get('name'));
         $shop->set("phone", str_replace(' ','',$request->get('phone')));
         $shop->set("email", $request->get('email'));
         $shop->set("website", $request->get('website'));
         $shop->set("about", $request->get('about'));
         if($request->get('latitude') && $request->get('longitude')){
            $point = new ParseGeoPoint($request->get('latitude'),$request->get('longitude'));
            $shop->set("location", $point);
         }
         $shop->set("category", $category);
         $shop->set("status", boolval($request->get('status')));
         $shop->set("popular", boolval($request->get('popular')));

         $shop->save();
         return redirect()->back();
      }else{
         // add new shopcategory record


         $category = new ParseObject("ShopCategory",$request->get('category'));

         $shop = new ParseObject("Shop");
         $shop->set("code", $request->get('code'));
         $shop->set("name", $request->get('name'));
         $shop->set("phone", str_replace(' ','',$request->get('phone')));
         $shop->set("email", $request->get('email'));
         $shop->set("website", $request->get('website'));
         $shop->set("about", $request->get('about'));
         if($request->get('latitude') && $request->get('longitude')){
            $point = new ParseGeoPoint($request->get('latitude'),$request->get('longitude'));
            $shop->set("location", $point);
         }
         $shop->set("category", $category);

         if ($_FILES["logo"]["name"]) {
            $logoName = $_FILES["logo"]["name"];
            $logo = ParseFile::createFromData(file_get_contents( $_FILES['logo']['tmp_name'] ), $logoName);
            $shop->set("logo", $logo);
         }

         if ($_FILES["photo"]["name"]) {
            $photoName = $_FILES["photo"]["name"];
            $photo = ParseFile::createFromData(file_get_contents( $_FILES['photo']['tmp_name'] ), $photoName);
            $shop->set("photo", $photo);
         }
         $shop->set("status", boolval($request->get('status')));
         $shop->save();
         return redirect('shop');
      }
   }

   // get shop category by objectid
   function getShopCategory($id){
      $query = new ParseQuery("ShopCategory");
      $query->equalTo("objectId", $id);
      return $query->first();
   }
}
