<?php
namespace App\Http\Controllers;
use DateTime;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\LibHelper;
use App\UdLib\MenuObject;
use App\UdLib\MenuObjectList;
use App\Http\Requests;
use Session;

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Closure;

class PageController extends Controller
{
   private $MenuController;

   public function __construct(){
      $this->MenuController = new MenuController();
   }

   public function aboutus(){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $query = new ParseQuery("Page");
      $query->equalTo("code", 'about_us');
   	$result = $query->first();

      return view(
         'page.aboutus',
         array(
            'result'=>$result,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }


   public function update(Request $request, $id){
      $offer = new ParseObject("Page", $id);
      $offer->set("content", $request->get('content'));
      $offer->save();

      return redirect()->back();
   }
}
