<?php
namespace App\Http\Controllers;
use DateTime;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\LibHelper;
use App\UdLib\MenuObject;
use App\UdLib\MenuObjectList;
use App\Http\Requests;
use Session;

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Closure;

class UserController extends Controller
{
   private $MenuController;

   public function __construct(){
      $this->MenuController = new MenuController();
   }

   public function index(){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $customerType = new ParseObject("CustomerType","CQii6LjxRQ");

      $query = new ParseQuery("_User");
      $query->equalTo("customerType", $customerType);
		$results = $query->find();

      return view(
         'user.index',
         array(
            'results'=>$results,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function create(){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      return view(
         'user.create',
         array(
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function store(Request $request){
      return $this->saveUser($request);
   }

   public function edit($id){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();


      $userroles = $this->getUserRoleByUserId($id);
      $menusNotAssiged = $this->getMenusThatNotAssigned($id);

      $query = new ParseQuery("_User");
   	$query->equalTo("objectId", $id);
   	$result = $query->first();

      return view(
         'user.edit',
         array(
            'result'=>$result,
            'userroles'=>$userroles,
            'menusNotAssiged'=>$menusNotAssiged,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function update(Request $request, $id){
      $this->saveUser($request, $id);
      $this->deleteUserRoleByUserId($id);
      $this->saveUserRole($request, $id);
      return redirect()->back();
   }

   public function destroy($id){
      try {
         $user = new ParseObject("_User", $id);
			$user->destroy(true);
         return redirect()->back();
      } catch (ParseException $ex) {
			echo 'Can not delete this User: ' + $ex->getMessage();
		}
   }

   public function password($id){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $query = new ParseQuery("_User");
   	$query->equalTo("objectId", $id);
   	$result = $query->first();

      return view(
         'user.password',
         array(
            'result'=>$result,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function updatepassword(Request $request, $id){
      $user = new ParseObject("_User", $id);
      $user->set("password", $request->get('password'));
      $user->save(true);
      Session::flash('message', "Password has been saved!");
      return redirect()->back();
   }

   ////////////////////////////////////////
   //function working on Data
   //////////////////////////////////////
   function saveUser(Request $request, $id=null){
      if ($id) {
         // update on previos shop record
         $user = new ParseObject("_User", $id);
         $user->set("firstName", $request->get('firstName'));
         $user->set("lastName", $request->get('lastName'));
         $user->set("phone", $request->get('phone'));
         $user->set("gender", $request->get('gender'));
         $user->set("username", $request->get('username'));
         $user->save(true);
      }else{
         // add new shopcategory record
         $customerType = new ParseObject("CustomerType","CQii6LjxRQ");

         $user = new ParseUser();
         $user->set("firstName", $request->get('firstName'));
         $user->set("lastName", $request->get('lastName'));
         $user->set("phone", $request->get('phone'));
         $user->set("gender", $request->get('gender'));
         $user->set("username", $request->get('username'));
         $user->set("password", $request->get('password'));
         $user->set("customerType", $customerType);
         try {
            $user->signUp();
            return redirect('user/edit/'.$user->getObjectId());
         } catch (ParseException $ex) {
            $message = $ex->getMessage();
         }
      }
   }

   //////////////////////////////////////////
   // working on user role
   /////////////////////////////////////////
   function saveUserRole(Request $request, $uid){
      $user = new ParseObject("_User",$uid);

      if($request->get('menus')){
         $countMenu = count($request->get('menus'));
         for($mn=0;$mn<$countMenu;$mn++){

            $readOnly = $request->get('readOnly')[$mn];
            $menuid = $request->get('menus')[$mn];
            $menu = new ParseObject("UserMenu",$menuid);

            $userrole = new ParseObject("UserRole");
            $userrole->set("user", $user);
            $userrole->set("menu", $menu);
            // if($readOnly == $menuid){
            //    exit ($readOnly.'='.$menuid);
            // }
            $userrole->save();
         }
      }
   }

   function getUserRoleByUserId($uid){
      $user = new ParseObject("_User", $uid);

      $query = new ParseQuery("UserRole");
      $query->equalTo("user", $user);
      $query->includeKey("menu");
      return $query->find();
   }

   function getMenusThatNotAssigned($uid){
      $user = new ParseObject("_User", $uid);

      $userRole = new ParseQuery("UserRole");
      $userRole->equalTo("user", $user);
      $results = $userRole->find();
      $menuIds = array();
      foreach($results as $result){
         $menuIds[] = $result->get('menu')->getObjectId();
      }

      $query = new ParseQuery("UserMenu");
      $query->equalTo("parent", null);
      $query->ascending("order");
      $query->notContainedIn("objectId",$menuIds);
      return $query->find();
   }

   function deleteUserRoleByUserId($uid){
      // try {
      //    $user = new ParseObject("_User", $uid);
      //
      //    $userRole = new ParseQuery("UserRole");
      //    $userRole->equalTo("user", $user);
		// 	$userRole->destroy();
      // } catch (ParseException $ex) {
		// 	echo 'Can not delete this User: ' + $ex->getMessage();
		// }

      $user = new ParseObject("_User", $uid);

      try {
         $userRoles = new ParseQuery("UserRole");
			$userRoles->equalTo("user", $user);
			$results = $userRoles->find();
         foreach($results as $result){
            $query = new ParseQuery("UserRole");
            $userRole=$query->get($result->getObjectId());
            $userRole->destroy();
         }
      } catch (ParseException $ex) {
			echo 'Can not delete this userRole: ' + $ex->getMessage();
		}
   }
   //end function working on Data
}
