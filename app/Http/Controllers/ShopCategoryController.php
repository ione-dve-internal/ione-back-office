<?php
namespace App\Http\Controllers;
use DateTime;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\LibHelper;
use App\UdLib\MenuObject;
use App\UdLib\MenuObjectList;
use App\Http\Requests;
use Session;

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Closure;

class ShopCategoryController extends Controller
{
   private $MenuController;

   public function __construct(){
      $this->MenuController = new MenuController();
   }

   public function index(){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      // $shopCategory = new ParseQuery("ShopCategory");
      // $shop = new ParseQuery("Shop");

      $query = new ParseQuery("ShopCategory");
      //$query->matchesQuery("Shop")
      //$query->include()
      $results = $query->find();


      return view(
         'shopcategory.index',
         array(
            'results'=>$results,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function create(){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      return view(
         'shopcategory.create',
         array(
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function store(Request $request){
      return $this->saveShopCategory($request);
   }

   public function edit($id){
      $menus = $this->MenuController->getMenusByUserId(session()->get('currentUser')->getObjectId());
      $childmenus = $this->MenuController->getAllChildMenu();

      $shopCategory = $this->getShopCategory($id);
      return view(
         'shopcategory.edit',
         array(
            'shopCategory'=>$shopCategory,
            'menus'=>$menus,
            'childmenus'=>$childmenus
         )
      );
   }

   public function update(Request $request, $id){
      return $this->saveShopCategory($request, $id);
   }

   public function destroy($id){
      $this->deleteShopsByCategory($id);
      $this->deleteShopsCategory($id);

      return redirect()->back();
   }

   ////////////////////////////////////////
   //function working on Data
   //////////////////////////////////////
   function saveShopCategory(Request $request, $id=null){
      if ($id) {
         // update on previos shopcategory record

         $shopcategory = new ParseObject("ShopCategory", $id);
         $shopcategory->set("name", $request->get('name'));

         if ($_FILES["photo"]["name"]) {
            $photoName = $_FILES["photo"]["name"];
            $photo = ParseFile::createFromData(file_get_contents( $_FILES['photo']['tmp_name'] ), $photoName);
            $shopcategory->set("photo", $photo);
         }
         if ($_FILES["pinIcon"]["name"]) {
            $photoName = $_FILES["photo"]["name"];
            $pinIcon = ParseFile::createFromData(file_get_contents( $_FILES['pinIcon']['tmp_name'] ), $photoName);
            $shopcategory->set("pinIcon", $pinIcon);
         }
         $shopcategory->save();
         return redirect()->back();
      }else{
         // add new shopcategory record

         $shopcategory = new ParseObject("ShopCategory");
         $shopcategory->set("name", $request->get('name'));
         if ($_FILES["photo"]["name"]) {
            $photoName = $_FILES["photo"]["name"];
            $photo = ParseFile::createFromData(file_get_contents( $_FILES['photo']['tmp_name'] ), $photoName);
            $shopcategory->set("photo", $photo);
         }
         if ($_FILES["pinIcon"]["name"]) {
            $photoName = $_FILES["photo"]["name"];
            $pinIcon = ParseFile::createFromData(file_get_contents( $_FILES['pinIcon']['tmp_name'] ), $photoName);
            $shopcategory->set("pinIcon", $pinIcon);
         }
         $shopcategory->save();

         return redirect('shopcategory');
      }
   }

   // get shop category by objectid
   function getShopCategory($id){
      $query = new ParseQuery("ShopCategory");
      $query->equalTo("objectId", $id);
      return $query->first();
   }

   //delete shopcategory
   function deleteShopsCategory($id){
      try {
         $query = new ParseQuery("ShopCategory");
			$shopcategory=$query->get($id);
			$shopcategory->destroy();
      } catch (ParseException $ex) {
			echo 'Can not delete this ShopCategory: ' + $ex->getMessage();
		}
   }

   //delete shops by categoryid
   function deleteShopsByCategory($cid){
      $category = new ParseObject("ShopCategory", $cid);

      try {
         $shops = new ParseQuery("Shop");
			$shops->equalTo("category",$category);
			$results = $shops->find();
         foreach($results as $result){
            $query = new ParseQuery("Shop");
            $shop=$query->get($result->getObjectId());
            $shop->destroy();
         }
      } catch (ParseException $ex) {
			echo 'Can not delete this Shop: ' + $ex->getMessage();
		}
   }

   //get shops by categoryid
   function hasShop($cid){
      $category = new ParseObject("ShopCategory", $cid);

      $shops = new ParseQuery("Shop");
      $shops->equalTo("category",$category);
      $results = $shops->find();
      return count($results);
   }
}
