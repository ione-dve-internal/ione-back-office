<?php
namespace App\Http\Controllers;
use DateTime;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\LibHelper;
use App\UdLib\MenuObject;
use App\UdLib\MenuObjectList;
use App\Http\Requests;
use Session;

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Parse\ParseSessionStorage;
use Closure;

class UserLoginController extends Controller
{
   public function index()
	{
      $currentUser = session()->get('currentUser');
      if($currentUser) return redirect()->action('DashboardController@index');
		return view('welcome');
	}
    public  function login(Request $request){
      try {
         $user = ParseUser::logIn($request->get('username'), $request->get('password'));

         session()->put('currentUser', $user);
         // echo '<pre>'; print_r(session()->get('userRoles'));
         // exit();
			return redirect('dashboard');
      }catch (ParseException $ex) {
         //exit('error');
			Session::flash('message', "Invalid Username or Password");
         return redirect()->back();
		}
    }

    public function logout(Request $request){
      $request->session()->flush();
      return redirect('./');
   }
}
