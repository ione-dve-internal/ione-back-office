<?php

namespace App\UdLib;

class MenuObject {
	public $id;
	public $group;
	public $name;
	public $route;
	public $url;
	public $selected=false;

	public function __construct($_id, $_group, $_name, $_route, $_url, $_selected= false){
		$this->id = $_id;
		$this->group = $_group;
		$this->name = $_name;
		$this->route = $_route;
		$this->url = $_url;
		$this->selected = $_selected;
	}
}
