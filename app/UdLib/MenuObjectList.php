<?php

namespace App\UdLib;

class MenuObjectList{
	public function getAllList(){
		$list[] = new MenuObject('1', '1', 'Dashboard', 'admin/dashboard', 'Admin\UserRoleController@index');
		$list[] = new MenuObject('2', '2', 'Pages', 'admin/pages', 'Admin\UserRoleController@index');
		$list[] = new MenuObject('3', '3', 'News Category', 'admin/NewsCategories', 'Admin\UserRoleController@index');
		$list[] = new MenuObject('4', '4', 'News', 'admin/News', 'Admin\NewsController@index');
		$list[] = new MenuObject('5', '5', 'Office', 'admin/office', 'Admin\OfficeController@index');

		$list[] = new MenuObject('6', '6', 'Product', 'admin/product', 'Admin\ProductController@index');

		$list[] = new MenuObject('100', '1', 'User Role', 'admin/UserRole', 'Admin\UserRoleController@index');
		$list[] = new MenuObject('101', '2', 'User', 'admin/User', 'Admin\UsersController@index');

		return $list;
	}

	public function setSelectItem($selectedArrays){
		$allData = $this->getAllList();
		$setData = $allData;
		if(count($selectedArrays)){
			for($i=0; $i< count($allData); $i++){
				foreach($selectedArrays as $arr){

					if($allData[$i]->id == $arr){
						$lrFound = $arr;
						$setData[$i]->selected = true;
						break;
					}
				}


			}
		}
		return $setData;
	}


	public function getSelectItem($selectedArrays){
		$allData = $this->getAllList();
		$setData = [];
		if(count($selectedArrays) >0){
			for($i=0; $i< count($allData); $i++){
				foreach($selectedArrays as $arr){
					if($allData[$i]->id == $arr){
						$lrFound = $arr;
						$setData[] = $allData[$i];
						break;
					}
				}
			}
		}
		return $setData;
	}

	public function isExistSelectItem($route){
		$allData = $this->getSelectItem(Session()->get('LOGIN_ROLE'));
		$isExist = false;
		foreach($allData as $data){
			if($data==$route){
				$isExist =true;
				break;
			}
		}
		return $isExist;
	}


}
